<?php

class UserEventHandler 
{
    const ON_LOGIN = 'auth.login';
    const ON_REGISTRATION = 'user.registration';
    const ON_PROJECT_SUBSCRIPTION = 'user.project.subscription';
    const ON_PROJECT_SUBSCRIPTION_CANCEL = 'user.project.subscription.cancel';
    
    public function onUserLogin($user) {}
    public function onUserRegistration($user) {}
    
    public function onProjectSubscription($subscription, $reward, $payment, $user, $type) 
    {
        if ('boleto' == $type) {
            Mail::send('emails.subscriptions.boleto', ['subscription'=>$subscription,'reward'=>$reward, 'boleto_url'=>$payment['url']], function($m) use($user){
                $m->to($user->email, $user->name)->subject('Bepartus - Confirmação de patrocínio');
            });
        }
        
        if ('cartao' == $type) {
            Mail::send('emails.subscriptions.confirm', ['subscription'=>$subscription,'reward'=>$reward], function($m) use($user){
                $m->to($user->email, $user->name)->subject('Bepartus - Confirmação de patrocínio');
            });
        }
        
        $project = $subscription->project;
        $user_producer = $project->user;
        
        Mail::send('emails.subscriptions.producer', ['subscription'=>$subscription,'user'=>$user, 'reward'=>$reward], function($m) use($user_producer, $project){
            $m->to($user_producer->email, $user_producer->name)->subject('Bepartus - Novo patrocínio para o projeto '.$project->name);
        });
    }
    
    public function onProjectSubscriptionCancelByUser($subscription, $user) 
    {
        $project = $subscription->project;
        $user_producer = $project->user;
        
        Mail::send('emails.subscriptions.cancel.producer', ['subscription'=>$subscription,'user'=>$user], function($m) use($user_producer, $project){
            $m->to($user_producer->email, $user_producer->name)->subject('Bepartus - Cancelamento de patrocínio para o projeto '.$project->name);
        });
        
        Mail::send('emails.subscriptions.cancel.user', ['subscription'=>$subscription,'user'=>$user], function($m) use($user){
            $m->to($user->email, $user->name)->subject('Bepartus - Confirmação de Cancelamento de Patrocínio');
        });
    }
    
    public function subscribe($events)
    {
        $events->listen(self::ON_LOGIN, 'UserEventHandler@onUserLogin');
        $events->listen(self::ON_REGISTRATION, 'UserEventHandler@onUserRegistration');
        $events->listen(self::ON_PROJECT_SUBSCRIPTION, 'UserEventHandler@onProjectSubscription');
        $events->listen(self::ON_PROJECT_SUBSCRIPTION_CANCEL, 'UserEventHandler@onProjectSubscriptionCancelByUser');
    }
}
