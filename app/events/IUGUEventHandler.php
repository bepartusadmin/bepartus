<?php

class IUGUEventHandler 
{
    public function onReferralsVerification($data) 
    {
        $producer = Producer::where('iugu_account_id',$data['account_id'])->first();
        if($producer) 
        {
            $user_producer = $producer->user;
            Mail::send('emails.producer.verification', ['data'=> $data, 'producer' => $user_producer], function($m) use($user_producer){
                $m->to($user_producer->email, $user_producer->name)->subject('Bepartus - Resultado de Verificação');
            });

            die('OK');
        } else {
            die('Producer not found');
        }
    }
    
    public function onInvoiceStatusChange($data) 
    {
        $subscription = Subscription::where('iugu_subscription_id',$data['subscription_id'])->first();
        if($subscription) {
            if($data['status'] == 'paid') {
                $subscription->status = '1';
                $subscription->save();
            }
            die('OK');
        } else {
            die('Subscription not found');
        }
    }
    
    public function subscribe($events)
    {
        $events->listen('referrals.verification', 'IUGUEventHandler@onReferralsVerification');
        $events->listen('invoice.status_changed', 'IUGUEventHandler@onInvoiceStatusChange');
    }
}

