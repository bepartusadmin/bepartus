<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public static $rules = [
		'name' => 'required',
		'email' => 'required|email|unique:users',
		'password' => 'required|confirmed',
		'cpf' => 'required|unique:userdatas,cpf'
	];
	public static $update_rules = [
		'name' => 'required',
		'email' => 'required|email',
		'cpf' => 'required'
	];

	public function userdata()
	{
		return $this->hasOne('Userdata');
	}

	public function producer()
	{
		return $this->hasOne('Producer');
	}

	public function customer()
	{
		return $this->hasOne('Customer');
	}

	public function projects()
	{
		return $this->hasMany('Project');
	}



	public function rewards()
	{
		return $this->hasManyThrough('Reward', 'Subscription');
	}

	public function subscriptions()
	{
		return $this->hasMany('Subscription');
	}

	public function getImageSrc($w=null,$h=null,$size = 'thumbs')
	{
		return $this->userdata->image != '' ? '<img height="'.$h.'" width="'.$w.'" src="'.asset('uploads/'.$size.'/'.$this->userdata->image).'" alt="'.$this->name.'">' : '<span class="user_avatar"><i class="fa fa-user"></i></span>';
	}

	public function getFirstNameAttribute()
	{
		$arr = explode(' ', $this->name);
		return $arr[0];
	}
}
