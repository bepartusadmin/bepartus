<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Reward extends \Eloquent{
	
        use SoftDeletingTrait;
        
        protected $dates = ['deleted_at'];
	protected $fillable = ['title','value','description','limit','iugu_plan_id','iugu_plan_identifier'];

	public function project()
	{
		return $this->belongsTo('Project');
	}

	public function subscriptions()
	{
		return $this->hasMany('Subscription');
	}

}