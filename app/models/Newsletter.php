<?php
class Newsletter extends \Eloquent{
	
	protected $fillable = ['name','email'];

	public static $rules = ['email'=>'required|email|unique:newsletters'];
}