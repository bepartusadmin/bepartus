<?php
class Userdata extends \Eloquent{
	public $timestamps = false;
	protected $fillable = ['cpf','user_id','address','city_id'];

	public static $rules = [
		'address' => 'required',
		'city_id' => 'required',
		'state_id' => 'required'
	];


	public function user(){
		return $this->belongs_to('User');
	}

	public function city()
	{
		return $this->belongsTo('City');
	}
}