<?php
class Producer extends \Eloquent{
	
	protected $fillable = [	'iugu_account_id','iugu_name','iugu_user_token','iugu_test_api_token','iugu_live_api_token',
		'physical_products',
		'business_type',
		'telephone',
		'cep',
		'bank',
		'bank_ag',
		'bank_cc',
		'account_type'
	];

	public static $rules = [
		'data[physical_products]' => 'required',
		'business_type' => 'required',
		'telephone' => 'required',
		'cep' => 'required',
		'bank' => 'required',
		'bank_ag' => 'required',
		'bank_cc' => 'required',
		'account_type' => 'required'
	];

	public function user()
	{
		return $this->belongsTo('User');
	}


}