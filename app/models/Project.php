<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Project extends \Eloquent{
	
        use SoftDeletingTrait;
        
        protected $dates = ['deleted_at'];
	protected $fillable = ['name','site','excerpt','description','approval_comment'];

	public static $rules = [
		'name'=>'required', 
		'site'=>'required',
		'excerpt'=>'required|max:67',
		'description'=>'required',
		'category_id' => 'required'
	];

	public function categories()
	{
		return $this->belongsToMany('Category','projects_categories');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function subscriptions()
	{
		return $this->hasMany('Subscription');
	}

	public function getAllCash()
	{
		
		$sum = Subscription::whereIn('id',$this->subscriptions->lists('id'))->sum('amount');
		
		return $sum/100;
	}

	public function objectives()
	{
		return $this->hasMany('Objective');
	}

	public function rewards()
	{
		return $this->hasMany('Reward');
	}
	public function scopeFeatured($query)
	{
		return $query->whereFeatured('1');
	}

	public function scopeActive($query)
	{
		return $query->where('status','1');
	}

	public function scopeConfirmed($query)
	{
		return $query->where('status_docs','1');
	}

	public function getVideo()
	{
		if($this->video_type == '1'){
			// youtube
			return '<iframe style="width:100%;" height="350" src="https://www.youtube.com/embed/'.$this->video_src.'" frameborder="0" allowfullscreen></iframe>';
		}elseif($this->video_type == '2'){
			return '<iframe src="https://player.vimeo.com/video/'.$this->video_src.'" style="width:100%;" height="350" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		}

		return null;
	}
	
	public function getImageSrc($size_path = 'uploads/thumbs/')
	{
		if($size_path == 'uploads/banner/'){
			return $this->image != '' ? asset($size_path.$this->image) : asset('img/project_default_banner.jpg'); 
		}
		return $this->image != '' ? asset($size_path.$this->image) : asset('img/project_default_image.jpg'); 
	}
}