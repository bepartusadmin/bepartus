<?php

class City extends \Eloquent {
	protected $fillable = [];

	public function userdatas()
	{
		return $this->hasMany('Userdata');
	}

	public function state()
	{
		return $this->belongsTo('State');
	}

}