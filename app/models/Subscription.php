<?php
class Subscription extends \Eloquent{

	protected $fillable = ['project_id','user_id','reward_id','statusText','iugu_subscription_id','paid_for_reward','amount','type','status'];

	public static $rules = [
		'address' => 'required',
		'city_id' => 'required',
		'state_id' => 'required'
	];

	public function project(){
		return $this->belongsTo('Project');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function reward()
	{
		return $this->belongsTo('Reward');
	}
	public function getReward()
	{
		return isset($this->reward->id) ? $this->reward->value : $this->amount;
	}

	public function direct()
	{
		return $this->paid_for_reward == '1' ? true : false;
	}

	public function getValue()
	{
		return $this->amount/100;
	}
}