<?php
class Objective extends \Eloquent{
	
	protected $fillable = ['name','value','result'];

	public function project()
	{
		return $this->belongsTo('Project');
	}
}