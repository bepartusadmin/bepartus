<?php
class Customer extends \Eloquent{
	
	protected $fillable = ['iugu_customer_id','iugu_item_type','iugu_data_token'];

	public function user()
	{
		return $this->belongsTo('User');
	}

}