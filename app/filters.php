<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

Route::filter('force.ssl', function()
{
    if( ! Request::secure() && !App::environment('staging'))
    {
        return Redirect::secure(Request::getRequestUri());
    }

});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/
Route::filter('already.producer', function(){
	$p = Producer::whereUser_id(Sentry::getUser()->id)->first();

	if(isset($p->id)){
		return Redirect::route('user.account.producer');
	}
});
Route::filter('already.logged', function(){
	if(Sentry::check()){
		return Redirect::route('user.account.sponsor');
	}
});
Route::filter('already.logged.admin', function(){
	if(Sentry::check()){
		return Redirect::route('admin.projetos.index');
	}
});
Route::filter('admin', function(){
	if(!Sentry::getUser()->hasAccess('Admin')){
		Sentry::logout();
		return Redirect::route('admin.login')->withError('Você não possui esta permissao de acesso');
	}
});
Route::filter('auth', function($route)
{
	if(!Sentry::check()){
		if($route->getName() == 'project.subscribe'){
			Session::set('buy_intent', $route->getParameter('slug'));
		}
		return Redirect::route('auth.login');
	}
});
Route::filter('my.project', function($route){
	$project = Project::whereUser_id(Sentry::getUser()->id)->whereSlug($route->getParameter('slug'))->get();
	if($project->count() < 1){
		return Redirect::route('projects.create')->withError('Você não possui esta permissão');
	}
});

Route::filter('has.producer', function(){
	$p = Producer::whereUser_id(Sentry::getUser()->id)->first();

	if(!isset($p->id)){
		return Redirect::route('producer.create')->withWarning('Você deverá criar sua conta como produtor');
	}
});



Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});


Validator::extend('greaterthan', function($attribute, $value, $parameters)
{
    $compare_value = is_array($parameters) && count($parameters) > 0 ? intval($parameters[0]) : 0;
    return intval(Helper::toCents($value)) > $compare_value;
});