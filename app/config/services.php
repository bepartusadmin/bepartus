<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => array(
		'domain' => 'bepartus.com',
		'secret' => 'key-15bcd397a37e058a71661c78169a8a11',
	),

	'mandrill' => array(
		'secret' => '',
	),

	'stripe' => array(
		'model'  => 'User',
		'secret' => '',
	),
    
        'iugu' => array(
            'token' => '0136b8bc1291e6543c7d501ae32e19da',
            'endpoint' => 'https://api.iugu.com/v1/marketplace'
        )



);
