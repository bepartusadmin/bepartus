<?php
# Site /estaticas

//Route::get('sub', 'SubscriptionsController@showSubCount');
Route::get('/',['as'=>'home','uses'=>'SiteController@index']);
Route::get('como-functiona', ['as'=>'about', 'uses'=>'SiteController@about']);
Route::get('perguntas-frequentes', ['as'=>'faq', 'uses'=>'SiteController@faq']);
Route::get('termos-de-uso', ['as'=>'terms', 'uses'=>'SiteController@terms']);
Route::get('contato', ['as'=>'get.contact', 'uses'=>'SiteController@getContact']);
Route::post('contato', ['as'=>'post.contact', 'uses'=>'SiteController@postContact']);
Route::post('webhooks',['as'=>'webhooks','uses'=>'SiteController@webhooks']);

# Cadastro
Route::get('cadastro',['before'=>'already.logged','as'=>'auth.register', 'uses'=>'AuthController@register']);
Route::post('cadastro',['as'=>'auth.post.register', 'uses'=>'AuthController@postRegister']);
# Testedecriação de conta
Route::get('criar-conta/{user_id}/teste/{project_id}', 'SubscriptionsController@createAccount');

# Newslleters
Route::post('newsletters', ['as'=>'newsletter.store', 'uses'=>'NewslettersController@store']);
# Login
Route::get('login',['before'=>'already.logged','as'=>'auth.login', 'uses'=>'AuthController@login']);
Route::post('login',['as'=>'auth.post.login', 'uses'=>'AuthController@postLogin']);
# BUscas
Route::get('buscar',['as'=>'main.search', 'uses'=>'ProjectsController@search']);
# Passwords
Route::get('password/reset', ['uses' => 'PasswordController@remind',  'as' => 'password.remind']);
Route::post('password/reset', ['uses' => 'PasswordController@request', 'as' => 'password.request']);
Route::get('password/reset/{token}', ['uses' => 'PasswordController@reset', 'as' => 'password.reset']);
Route::post('password/reset/{token}', ['uses' => 'PasswordController@update', 'as' => 'password.update']);

# Projetos
Route::get('projetos',['as'=>'projects.index', 'uses'=>'ProjectsController@indexSite']);
Route::get('projeto/{slug}',['as'=>'project.show', 'uses'=>'ProjectsController@showSite']);

# Categorias
Route::get('projetos/{slug_cat}',['as'=>'category.show', 'uses'=>'ProjectsController@indexSite']);
# Buscas
Route::post('city/search', ['as'=>'city.search', 'uses'=>'CitiesController@searchByState']);
Route::post('city/search/name', ['as'=>'city.search.name', 'uses'=>'CitiesController@searchByStateName']);

	# Pratocinar / Kart
Route::group(['before'=>'auth|force.ssl'],function(){
	Route::get('projeto/{slug}/patrocinar',['as'=>'project.subscribe', 'uses'=>'SubscriptionsController@getSubscribe']);
	Route::post('projeto/{slug}/patrocinar',['as'=>'project.payment.subscribe', 'uses'=>'SubscriptionsController@paymentSubscribe']);

	Route::get('projeto/{slug}/patrocinar/checkout',['as'=>'project.subscribe.checkout', 'uses'=>'SubscriptionsController@getSubscribeCheckout']);
	Route::post('projeto/{slug}/patrocinar/checkout',['as'=>'project.post.subscribe', 'uses'=>'SubscriptionsController@postSubscribe']);
});

# Accounts
Route::group(['prefix'=>'minha-conta','before'=>'auth'],function(){
	# Tipagem
	Route::get('patrocinador', ['before'=>'already.producer','as'=>'user.account.sponsor', 'uses'=>'UsersController@profileSponsor']);
	Route::get('produtor', ['before'=>'has.producer', 'as'=>'user.account.producer', 'uses'=>'SubscriptionsController@profileProducer']);

	#Produtor
	Route::get('produtor/verificacao', ['as'=>'producer.create', 'uses'=>'UsersController@createProducer']);
	Route::post('produtor/verificacao', ['as'=>'producer.store', 'uses'=>'SubscriptionsController@createAccount']);
	// Enviando a verificação ao IUGU
	Route::get('produtor/{pid}/verificacao', ['as'=>'sending.verification', 'uses'=>'SubscriptionsController@sendindVerification']);

	# Update de senha
	Route::post('salvar/nova/senha',['as'=>'user.update.password', 'uses'=>'PasswordController@updatePass']);

	#Updae userdata
	Route::post('salvar/dados/usuario',['as'=>'user.update.userdata','uses'=>'UsersController@updateUserdata']);
	# Upload de imagem
	Route::post('image/upload', ['as'=>'user.upload.photo','uses'=>'UsersController@uploadImage']);

	# Projetos
	Route::get('projeto',['before'=>'has.producer','as'=>'projects.create', 'uses'=>'ProjectsController@createSite']);
	Route::post('projeto', ['before'=>'has.producer','as'=>'projects.store', 'uses'=>'ProjectsController@storeSite']);

	Route::group(['before'=>'my.project|has.producer', 'prefix'=>'projeto/{slug}'], function(){
		Route::get('editar',['as'=>'projects.edit', 'uses'=>'ProjectsController@editSite']);
		Route::post('editar', ['as'=>'projects.update', 'uses'=>'ProjectsController@updateSite']);

		# Projeto single(área admin)
		Route::get('/',['as'=>'projects.account.show', 'uses'=>'ProjectsController@accountShow']);

		# Criação de metas e recompensas
		Route::get('metas',['as'=>'project.objective.create', 'uses'=>'ProjectsController@objectiveCreate']);
		Route::post('metas',['as'=>'project.objective.store', 'uses'=>'ProjectsController@objectiveStore']);
		Route::delete('metas/{objective_id}',['as'=>'project.objective.delete', 'uses'=>'ProjectsController@objectiveDelete']);
		Route::get('recompensas',['as'=>'project.reward.create', 'uses'=>'ProjectsController@rewardCreate']);
		Route::post('recompensas',['as'=>'project.reward.store', 'uses'=>'SubscriptionsController@rewardStore']);
                Route::delete('recompensas/{reward_id}',['as'=>'project.reward.delete', 'uses'=>'ProjectsController@rewardDelete']);
	});

	# Cancelar subscriptiom
	Route::post('cancelar/{subscription_id}',['as'=>'subscription.cancel','uses'=>'SubscriptionsController@cancel']);
	#Logout
	Route::get('logout',['as'=>'user.logout', 'uses'=>'AuthController@logout']);

});
# Admin
Route::get('admin',['before'=>'already.logged.admin','as'=>'admin.login', 'uses'=>'AdminController@login']);
Route::post('admin/login',['as'=>'admin.post.login', 'uses'=>'AdminController@postLogin']);
Route::get('admin/logout',['as'=>'admin.logout', 'uses'=>'AdminController@logout']);

Route::group(['prefix'=>'admin', 'before'=>'auth|admin', 'namespace' => 'Admin'], function(){
    
    Route::resource('projetos', 'ProjectsController');
    Route::post('projeto/{id}/aprovar', ['as'=>'admin.projetos.aprovar', 'uses'=>'ProjectsController@aprovar']);
    Route::post('projeto/{id}/justifica', ['as'=>'admin.projetos.justifica', 'uses'=>'ProjectsController@justifica']);
    
    Route::resource('usuarios', 'UsersController');
    Route::post('usuario/{id}/ativacao', ['as'=>'admin.user.deactivate', 'uses'=>'UsersController@deactivate']);
        
    Route::resource('newsletters', 'NewslettersController');
    Route::post('news/download', ['as'=>'admin.newsletters.download', 'uses'=>'NewslettersController@download']);
    
    Route::resource('patrocinios', 'SubscriptionsController');
    Route::get('dashboard', ['as'=>'admin.dashboard.index', 'uses'=>'DashboardController@index']);
}); 

