<?php 
View::composer('partials.site.header', function($view){
	$user = null;
	if(Sentry::check()){
		$user = User::find(Sentry::getUser()->id);	
	}
	$view->with('user', $user);
});