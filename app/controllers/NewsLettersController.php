<?php

class NewslettersController extends BaseController {

	public function store()
	{
		$data = Input::all();

		$validator = Validator::make($data, Newsletter::$rules, ['unique' => 'Email cadastrado anteriormente.']);

		if($validator->fails()){
                    App::abort(403, $validator->errors()->first());
		}
		$news = Newsletter::create($data);
                return Response::json(array('success' => true, 'message' => 'Cadastro efetuado com sucesso, aguarde em breve mais novidades no seu e-mail!'));
	}

	public function index()
	{
		$news = Newsletter::orderBy('id','desc')->paginate(25);

		return View::make('admin.newsletter.index', compact('news'));
	}

	public function download()
	{
		$news = Newsletter::orderBy('id','desc')->get();

		Excel::create('Bepartus_base_newsletters_'.date('d_m_Y'), function($excel) use($news){

			$excel->sheet('Emails', function($sheet) use($news) {

       			 $sheet->loadView('admin.excel.news',['news'=>$news]);
       		});
		})->export('xls');
	}
}
