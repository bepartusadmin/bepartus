<?php

class CitiesController extends \BaseController {

	public function searchByState(){
		if(Request::ajax()){
			$cities = City::where('state_id',Input::get('state'))->get();


			$output = '<option value="">Selecione sua cidade</option>';

			foreach ($cities as $c) {
				$output .= '<option value="'.$c->id.'">'.$c->name.'</option>';
			}

			return Response::json(['response' => $output]);
		}
	}

	public function searchByStateName(){
		if(Request::ajax()){
			$cities = City::whereHas('state', function($e){
				$e->whereName(Input::get('state'));
			})->get();


			$output = '<option value="">Selecione sua cidade</option>';

			foreach ($cities as $c) {
				$output .= '<option value="'.$c->name.'">'.$c->name.'</option>';
			}

			return Response::json(['response' => $output]);
		}
	}


}