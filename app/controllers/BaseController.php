<?php

class BaseController extends Controller 
{

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout() 
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    public function upload_file($file, $banner = false, $w = 307, $h = 307, $filename = null) 
    {
        $destinationPath = public_path() . '/uploads/';
        
        $ext = $file->getClientOriginalExtension();
        $name = uniqid(rand());
        $filename = $name . '.' . $ext;

        $upload_success = $file->move($destinationPath, $filename);

        if ($upload_success) 
        {
            // resizing an uploaded file
            $destinationThumbPath = $destinationPath . 'thumbs/';
            $destinationNormalPath = $destinationPath . 'normal/';
            $destinationBannerPath = $destinationPath . 'banner/';

            Image::make($destinationPath . $filename)
                ->fit($w, $h, function($constraint) {
                    $constraint->upsize();
                })
                ->save($destinationNormalPath . $filename)
            ;

            if ($banner == true) 
            {
                Image::make($destinationPath . $filename)
                    ->fit(1280, 400)
                    ->save($destinationBannerPath . $filename)
                ;
            }

            Image::make($destinationPath . $filename)
                ->fit(263, 191)
                ->save($destinationThumbPath . $filename)
            ;

            File::delete($destinationPath . $filename);
            return $filename;
        }

        return false;
    }

    public function getUser() {
        return Sentry::check() ? User::find(Sentry::getUser()->id) : null;
    }

    public function getIuguApiToken($force_live = false) {
        return Config::get('services.iugu.token');
    }

    public function _defaultHeaders($headers = []) 
    {
        $headers[] = "Authorization: Basic " . base64_encode(Iugu::getApiKey() . ":");
        $headers[] = "Accept: application/json";
        $headers[] = "Accept-Charset: utf-8";
        $headers[] = "User-Agent: Iugu PHPLibrary";
        $headers[] = "Accept-Language: pt-br;q=0.9,pt-BR";
        return $headers;
    }

}
