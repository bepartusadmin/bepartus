<?php

namespace Admin;

use \Project;
use \Category;
use \File;
use \Input;
use \View;
use \Validator;
use \Redirect;

class ProjectsController extends \BaseController {
    
    
    public function index() 
    {
        $p = Project::orderBy('id','desc');

        if(Input::has('n')){
            $p->where('name','like','%'.Input::get('n').'%');
        }

        $projects = $p->paginate(10);

        return View::make('admin2.projects.index', compact('projects'));
    }
    
    public function create(){
        return Redirect::route('admin.projetos.index')->withWarning('Ação não implementada');
    }
    
    public function store(){
        return Redirect::route('admin.projetos.index')->withWarning('Ação não implementada');
    }
    
    public function show($id) {
        return Redirect::route('admin.projetos.edit', $id);
    }
    
    public function edit($id)
    {
        $project = Project::find($id);
        $project->load('objectives');
        $project->load('rewards');
        $project_categories = $project->categories()->lists('category_id');
        $categories = [''=>'Escolher categoria'] + Category::orderBy('name')->lists('name','id');
        
        $networks = \Network::all();
	$n = $project->networks != '' ? unserialize(base64_decode($project->networks)) : array();
        
        return View::make('admin2.projects.edit', compact('categories','project','project_categories','networks','n'));
    }
    
    public function update($id)
    {
        $validator = Validator::make(Input::except('objectives','rewards'), \Project::$rules);
        $validatorObjectives = $this->setupObjectiveValidator();
        $validatorRewards = $this->setupRewardValidator();

        if ($validator->fails() || $validatorObjectives->fails() || $validatorRewards->fails()) 
        {
            $errors = $validator->messages()
                ->merge($validatorObjectives->messages())
                ->merge($validatorRewards->messages())
            ;
             
            return Redirect::route('admin.projetos.edit', $id)
                ->withErrors($errors)
                ->withInput(\Input::except('password'))
            ;
            
        } else {

            $p = Project::find($id);
            $p->name = Input::get('name');
            $p->excerpt = Input::get('excerpt');
            $p->description = Input::get('description');
            $p->featured = Input::get('featured');
            $p->status = Input::get('status');
            $p->site = Input::get('site');
            $p->networks = !empty(Input::get('networks')) ? base64_encode(serialize(Input::get('networks'))) : null;
            $p->categories()->sync(array_values(Input::get('category_id')));
            
            $video = $this->processVideo();
            
            $p->video_src = $video['video_src'];
            $p->video_link = $video['video_link'];
            $p->video_type = $video['video_type'];
                        
            if(Input::hasFile('image'))
            {
                if($p->image != ''){
                    File::delete('uploads/normal/'.$p->image);
                    File::delete('uploads/thumbs/'.$p->image);
                    File::delete('uploads/banner/'.$p->image);
                }
                
                $p->image = $this->upload_file(Input::file('image'), true);
            }
            
            if(Input::has('objectives'))
            {
                foreach(Input::get('objectives') as $i => $params) 
                {
                    $objective = \Objective::findOrNew($params['id']);
                    if(isset($params['delete']) && !is_null($objective->id)) {
                        $objective->delete();
                    } else {
                        $objective->value = \Helper::toCents($params['value']);
                        $objective->name = $params['name'];
                        $objective->result = $params['result'];
                        $objective->project()->associate($p);
                        $objective->save();
                    }
                }
            }
            
            if(Input::has('rewards'))
            {
                foreach(Input::get('rewards') as $i => $params) 
                {
                    $reward = \Reward::findOrNew($params['id']);
                    if(isset($params['delete']) && !is_null($reward->id)) {
                        $reward->delete();
                    } else {
                        $reward->value = \Helper::toCents($params['value']);
                        $reward->title = $params['title'];
                        $reward->description = $params['description'];
                        $reward->project()->associate($p);
                        $reward->save();
                    }
                }
            }
            
            $p->push();
            
            return Redirect::route('admin.projetos.index')->withSuccess('Projeto atualizado com sucesso');
        }  
        
    }
    
    public function destroy($id){
        $project = Project::find($id);
        $project->delete();
        
        //Cancelar inscrições
        
        return Redirect::route('admin.projetos.index')->withSuccess('Projeto removido com sucesso');
    }
    
    public function aprovar($id)
    {
        $project = Project::find($id);
	$project->status = Input::get('state') == 'true' ? '1' : '0';
	$project->save();
	$res = Input::get('state') == 'true' ? 'Projeto ativado com sucesso' : 'Projeto reprovado';
	return Response::json(['response'=>$res,'status'=>Input::get('state')]);
    }

    public function justifica($id)
    {
        $project = Project::find($id);
	$project->approval_comment = Input::get('approval_comment');
	$project->save();

	Mail::send('emails.projects.justify', ['project'=>$project], function($m) use($project){
            $m->to($project->user->email, $project->user->name)->subject('Bepartus - Saiba porque seu projeto não foi aprovado');
	});
	return Redirect::route('admin.projetos.index', Input::get('page', null))->withSuccess('Justificativa enviada com sucesso!');
    }
    
    private function setupObjectiveValidator() 
    {
        $objectives = Input::get('objectives');
        $objectivesRules = [];
        $objectivesAttrNames = [];
        
        if(Input::has('objectives'))
        {
            foreach( $objectives as $i => $obj ) {
                $objectivesRules['objectives.'.$i.'.value'] = 'required|greaterthan:0';
                $objectivesRules['objectives.'.$i.'.name'] = 'required|max:255';
                $objectivesRules['objectives.'.$i.'.result'] = 'required';

                $objectivesAttrNames['objectives.'.$i.'.value'] = 'Valor';
                $objectivesAttrNames['objectives.'.$i.'.name'] = 'Nome';
                $objectivesAttrNames['objectives.'.$i.'.result'] = 'Resultado';
            }
        }

        $validatorObjectives = Validator::make(Input::only('objectives'), $objectivesRules);
        $validatorObjectives->setAttributeNames($objectivesAttrNames); 
        
        return $validatorObjectives;
    }
    
    private function setupRewardValidator() 
    {
        $rewards = Input::get('rewards');
        $rewardsRules = [];
        $rewardsAttrNames = [];
        
        if(Input::has('rewards'))
        {
            foreach( $rewards as $i => $obj ) {
                $rewardsRules['rewards.'.$i.'.value'] = 'required|greaterthan:0';
                $rewardsRules['rewards.'.$i.'.title'] = 'required|max:255';
                $rewardsRules['rewards.'.$i.'.description'] = 'required';

                $rewardsAttrNames['rewards.'.$i.'.value'] = 'Valor';
                $rewardsAttrNames['rewards.'.$i.'.title'] = 'Titulo';
                $rewardsAttrNames['rewards.'.$i.'.description'] = 'Descrição';
            }
        }

        $validatorRewards = Validator::make(Input::only('rewards'), $rewardsRules);
        $validatorRewards->setAttributeNames($rewardsAttrNames); 
        
        return $validatorRewards;
    }
    
    private function processVideo() 
    {
        $data = Input::all();
        $video_src = null;
        $video_type = '0';

        if(Input::has('video_src'))
        {
            if(strpos($data['video_src'], 'yout')){
                $video_src = \Helper::parse_youtube($data['video_src']);
		$video_type = '1';
            } elseif(strpos($data['video_src'], 'vimeo')) {
                $video_src = \Helper::parse_vimeo($data['video_src']);
		$video_type = '2';
            } else {
                $video_src = null;
		$video_type = '0';
            }
        }
        
        return [
            'video_src' => $video_src,
            'video_link' => $data['video_src'] != '' ? $data['video_src'] : '',
            'video_type' => $video_type
        ];  
    }
}
