<?php

namespace Admin;

use \User;
use \Userdata;
use \City;
use \State;
use \Input;
use \File;
use \View;
use \Validator;
use \Redirect;

class UsersController extends \BaseController 
{
    public function index()
    {
        $u = User::orderBy('id','desc');

	if(Input::has('n')){
            $u->where('name', 'like', '%'.Input::get('n').'%');
        }

	$users = $u->paginate(20);
        $admin_group = \Sentry::findGroupByName('Admin');
        
	return View::make('admin2.users.index', compact('users','admin_group'));
    }
    
    public function create(){
        return Redirect::route('admin.users.index')->withWarning('Ação não implementada');
    }
    
    public function store(){
        return Redirect::route('admin.users.index')->withWarning('Ação não implementada');
    }
    
    public function show($id) {
        return Redirect::route('admin.users.edit', $id);
    }
    
    public function edit($id)
    {
        $user = User::find($id); 
        $user->load('userdata');
        
        if(isset($user->userdata->city_id)){
            $cities = $user->userdata->city_id != '0' ? City::whereState_id($user->userdata->city->state->id)->orderBy('name')->lists('name','id') : array();
        }elseif(Input::has('state_id')){
            $cities = City::whereState_id(Input::get('state_id'))->orderBy('name')->lists('name','id');
        }else{
            $cities = array();
        }

        $cities =  [''=>'selecione cidade'] + $cities;
        $states = [''=>'selecione estado']+ State::orderBy('name')->lists('name','id');

        return View::make('admin2.users.edit', compact('user','cities','states'));
    }
    
    public function update($id)
    {   
        $data = Input::all();
        
        $user_rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'min:5|confirmed',
            'userdata.cpf' => 'required|unique:userdatas,cpf,'.$id.',user_id',
            'userdata.address' => 'required',
            'userdata.city_id' => 'required'
	];
        
        $validator = Validator::make(Input::all(), $user_rules);
        if($validator->fails()) {
            return Redirect::route('admin.usuarios.edit', $id)
                ->withErrors($validator)
                ->withInput(Input::except('userdata.image','password'))
            ;
        }
        
        $user = User::find($id);
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->userdata->cpf = $data['userdata']['cpf'];
        $user->userdata->address = $data['userdata']['address'];
        $user->userdata->city_id = $data['userdata']['city_id'];
        
        
        if(Input::hasFile('userdata.image'))
        {
            if( $user->userdata->image != '' ) {
                File::delete('uploads/thumbs/'.$user->userdata->image);
                File::delete('uploads/normal/'.$user->userdata->image);
            }
            
            $user->userdata->image = $this->upload_file(Input::file('userdata.image'), true);
        }
        
        $user->push();
        
        if($data['password'] != '') {
            $user = \Sentry::findUserById($id);
            $user->password = $data['password']; 
            $user->save();
        }
        
        return Redirect::back()->withSuccess(\Lang::get('user.edit.success'));
    }
    
    public function destroy($id){
        return Redirect::route('admin2.users.index')->withWarning('Ação não implementada');
    }
    
    public function deactivate($id)
    {
        $user = User::find($id);

        $user->activated = $user->activated == '1' ? '0' : '1';
        $user->save();
        $act = $user->activated == '1' ? 'ativado' : 'desativado';

        return Redirect::back()->withSuccess('Usuário '.$act);
    }
}
