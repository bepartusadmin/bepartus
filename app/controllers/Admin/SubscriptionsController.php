<?php
namespace Admin;

use \View;
use \Subscription;
use \Redirect;
use \Iugu_Subscription;
use \IuguObjectNotFound;
use \IuguException;
use \App;
use \Iugu;
use \Input;

class SubscriptionsController extends \BaseController
{
    public function index() 
    {
        $q = Subscription::with('project','user')
            ->orderBy('id','desc')
        ;
        
        if(Input::has('project'))
        {
            $q->whereHas('project',function($q){
                $q->where('name','like','%'.Input::get('project').'%');
            });
        }
        
        $subscriptions = $q->paginate(25);
        
        return View::make('admin2.subscriptions.index', compact('subscriptions'));
    }
    
    public function create(){
        return Redirect::route('admin.patrocinios.index')->withWarning('Ação não implementada');
    }
    
    public function store(){
        return Redirect::route('admin.patrocinios.index')->withWarning('Ação não implementada');
    }
    
    public function show($id) {
        return Redirect::route('admin.patrocinios.index')->withWarning('Ação não implementada');
    }
    
    public function edit($id) {
        return Redirect::route('admin.patrocinios.index')->withWarning('Ação não implementada');
    }
    
    
    public function destroy($id)
    {
        $subscription = Subscription::find($id);
        
        if (App::environment('local', 'staging')) {
            Iugu::setApiKey($subscription->project->user->producer->iugu_test_api_token);
        } else {
            Iugu::setApiKey($subscription->project->user->producer->iugu_live_api_token);
        }

        try 
        {
            if(empty($subscription->iugu_subscription_id)){
                throw new IuguObjectNotFound();
            }
            $sub = Iugu_Subscription::fetch($subscription->iugu_subscription_id);
            $sub->delete();
            $subscription->delete();
            return Redirect::route('admin.patrocinios.index')->withSuccess('Patrocínio cancelado com sucesso');
        } 
        catch (IuguObjectNotFound $e) 
        {
            $subscription->delete();
            return Redirect::route('admin.patrocinios.index')->withSuccess('Patrocínio cancelado com sucesso');
        } 
        catch (IuguException $e) 
        {
            return Redirect::route('admin.patrocinios.index')
                ->withError('Erro ao suspender patrocinio, tente novamente mais tarde ou entre em contato conosco.'.$e->getMessage())
            ;
        }
    }
}

