<?php

namespace Admin;

use \View;
use \Input;
use \DB;

class DashboardController extends \BaseController 
{
    public function index()
    {
        $to = Input::get('to',date('Y-m-d'));
        $from = Input::get('from',date('Y-m-d', strtotime($to.' -1 months')));
        
        $subscriptions = [
            'per_project' => $this->getSubsQuery($from,$to)
                ->select(DB::raw('count(*) as count, (SELECT name FROM projects as p WHERE p.id = project_id) as project_name, project_id'))
                ->groupBy('project_id')
                ->orderBy('project_id', 'asc')
                ->get()
            ,
            'type' => $this->getSubsQuery($from,$to)
                ->select(DB::raw("count(1) as value, case type when '1' then 'Cartão' when '2' then 'Boleto' else 'Cartão' end as label, '#01a8fe' as color, '#01a8fe' as highlight "))
                ->groupBy('type')
                ->get()
            ,
            'count' => $this->getSubsQuery($from,$to)->count(),
            'pending' => $this->getSubsQuery($from,$to)->where('status','0')->count(),
            'sum' => $this->getSubsQuery($from,$to)->sum('amount'),
        ];
        
        $users = [
            'count' => $this->getUsersQuery($from,$to)->count(),
            'progression' => $this->getUsersQuery($from,$to)->select(DB::raw('count(*) as user_count, date(created_at)'))->groupBy('date(created_at)')->orderBy('created_at', 'asc')->get()
        ];
                
	return View::make('admin2.dashboard.index', compact('from','to','subscriptions','users'));
    }
    
    private function getSubsQuery($from,$to) {
        return DB::table('subscriptions')
            ->whereRaw('date(created_at) >= ? AND date(created_at) <= ?',[$from,$to])
        ;
    }
    
    private function getUsersQuery($from,$to) {
        return DB::table('users')
            ->whereRaw('date(created_at) >= ? AND date(created_at) <= ?',[$from,$to])
        ;
    }
    
}

