<?php

namespace Admin;

use \Newsletter;
use \Excel;
use \View;

class NewslettersController extends \BaseController 
{
    public function index()
    {
        $news = Newsletter::orderBy('id','desc')->paginate(25);
	return View::make('admin2.newsletter.index', compact('news'));
    }

    public function download()
    {
        $news = Newsletter::orderBy('id','desc')->get();
	Excel::create('Bepartus_base_newsletters_'.date('d_m_Y'), function($excel) use($news){

        $excel->sheet('Emails', function($sheet) use($news) {
            $sheet->loadView('admin2.excel.news',['news'=>$news]);
       	});
        })->export('xls');
    }
}
