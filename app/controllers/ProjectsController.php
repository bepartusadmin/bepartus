<?php

class ProjectsController extends BaseController {

	protected $layout = 'layouts.application';

	public function indexSite($slug_cat = null)
	{
            $categories = Category::orderBy('name','DESC')->get();
            
            $p =  Project::active()
                ->confirmed()
            ;
                        
            $category = Category::query();
            if($slug_cat != null){
                $p->whereHas('categories', function($c) use($slug_cat){
                    $c->whereSlug($slug_cat);
		});
		$category = Category::whereSlug($slug_cat)->first();
            }
            
            switch (Input::get('order')) {
                case '1':
                    $p->orderByRaw('(SELECT COUNT(1) FROM subscriptions as s WHERE s.project_id = projects.id  ) desc');
                    break;
                case '2':
                    $p->orderByRaw('(SELECT SUM(amount) FROM subscriptions as s WHERE s.project_id = projects.id  ) desc');
                    break;
                case '4':
                    $p->orderBy('id', 'asc');
                    break;
                case '3':
                default:
                    $p->orderBy('id', 'desc');
                    break;
            }
            
            //die($p->getQuery()->toSql());
            $projects = $p->paginate(15);
            $this->layout->content = View::make('projects.index', compact('projects','categories', 'category'));
	}

	public function search()
	{
		
		$projects = Project::orderBy('id','desc')->active()->confirmed()
		->where(function($q){
			$q->where('name','like','%'.Input::get('s').'%')
			->orWhere('excerpt','like','%'.Input::get('s').'%')
			->orWhere('description','like','%'.Input::get('s').'%');
		})
		->paginate(12);

		$this->layout->content = View::make('projects.search', compact('projects'));
	}

	public function createSite()
	{
		$categories = Category::orderBy('name')->lists('name','id');
		$networks = Network::all();
		$this->layout->content = View::make('projects.create', compact('categories','networks'));
	}

	public function storeSite()
	{
		$data =Input::all();

		$validator = Validator::make($data, Project::$rules);

		if($validator->fails()){
			return Redirect::route('projects.create')->withErrors($validator)->withInput();
		}

		$user = $this->getUser();
		
		$project = Project::create($data);

		//Programando o envio
		if(Input::has('video_src')){
			if(strpos($data['video_src'], 'yout')){
				$video_src = $this->parse_youtube($data['video_src']);
				$video_type = '1';
			}elseif(strpos($data['video_src'], 'vimeo')){
				$video_src = $this->parse_vimeo($data['video_src']);
				$video_type = '2';
			}else{
				$video_src = '';
				$video_type = '0';
			}

			$project->video_src = $video_src;
			$project->video_type = $video_type;
			$project->video_link = $data['video_src'] != '' ? $data['video_src'] : '';
		}
		if(Input::hasFile('image')){
			//criando imagem com banner
			$main_image = $this->upload_file(Input::file('image'), true);
			$project->image = $main_image;
		}

		if(Input::has('slug')){
			$sluggeds = Project::whereSlug($data['slug'])->first();
			$project->slug = isset($sluggeds->id) ? Slugify::slugify($data['slug']).'-'.uniqid() : Slugify::slugify($data['slug']);
		}else{
			$sluggeds = Project::whereSlug(Slugify::slugify($data['name']))->first();
			$project->slug = isset($sluggeds->id) ? Slugify::slugify($data['name']).'-'.uniqid() : Slugify::slugify($data['name']);
		}
		if(!empty($data['category_id'])){
			$project->categories()->attach($data['category_id']);
		}
		
		
		$project->networks = !empty($data['networks']) ? base64_encode(serialize($data['networks'])) : null;
		
		$project->user()->associate($user);
		$project->save();

		// Então retorna ele para criação de objetivo
		return Redirect::route('project.objective.create', $project->slug);
	}

	public function showSite($slug)
	{
		$project = Project::whereSlug($slug)->active()->confirmed()->first();
		$networks = Network::all();
                $categories = $project->categories->sortBy('name')->lists('name');
		$n = $project->networks != '' ? unserialize(base64_decode($project->networks)) : array();

		$this->layout->content = View::make('projects.show', compact('project',	'networks','n','categories'));
	}

	public function editSite($slug)
	{
		$project = Project::whereSlug($slug)->first();
		$categories = Category::orderBy('name')->lists('name','id');
		$networks = Network::all();

		$n = $project->networks != '' ? unserialize(base64_decode($project->networks)) : array();

		$this->layout->content = View::make('projects.edit', compact('categories','networks','project','n'));
	}

	public function updateSite($slug)
	{
		$data = Input::all();
		$project = Project::whereSlug($slug)->first();

		$project->update($data);

		if(Input::hasFile('image')){
			//criando imagem com banner
			if($project->image != ''){
				File::delete('uploads/normal/'.$project->image);
				File::delete('uploads/thumbs/'.$project->image);
				File::delete('uploads/banner/'.$project->image);
			}
			$main_image = $this->upload_file(Input::file('image'), true);
			$project->image = $main_image;
		}

		if(Input::has('video_src')){
			if(strpos($data['video_src'], 'yout')){
				$video_src = $this->parse_youtube($data['video_src']);
				$video_type = '1';
			}elseif(strpos($data['video_src'], 'vimeo')){
				$video_src = $this->parse_vimeo($data['video_src']);
				$video_type = '2';
			}else{
				$video_src = null;
				$video_type = '0';
			}

			$project->video_src = $video_src;
			$project->video_link = $data['video_src'] != '' ? $data['video_src'] : '';
			$project->video_type = $video_type;
		}else{
			$project->video_src = null;
			$project->video_link = $data['video_src'] != '' ? $data['video_src'] : '';
			$project->video_type = '0';
		}

		if(Input::get('slug') != $project->slug or Input::get('name') != $project['name']){
			if(Input::has('slug')){
				$sluggeds = Project::whereSlug($data['slug'])->first();
				$project->slug = isset($sluggeds->id) ? Slugify::slugify($data['slug']).'-'.uniqid() : Slugify::slugify($data['slug']);
			}else{

				$sluggeds = Project::whereSlug(Slugify::slugify($data['name']))->first();
				$project->slug = isset($sluggeds->id) ? Slugify::slugify($data['name']).'-'.uniqid() : Slugify::slugify($data['name']);
			}
		}

		if(!empty($project->categories()->lists('category_id'))){
			$project->categories()->detach($project->categories()->lists('category_id'));
		}
		if(!empty($data['category_id'])){
			$project->categories()->attach($data['category_id']);
		}
		
	
		
		$project->networks = !empty($data['networks']) ? base64_encode(serialize($data['networks'])) : null;

		$project->save();
		
		return Redirect::route('projects.edit', $project->slug)->withSuccess(Lang::get('project.updated'));
	}

	public function objectiveCreate($slug)
	{
		$project = Project::whereSlug($slug)->first();

		$this->layout->content = View::make('projects.objective', compact('project'));
	}
	public function objectiveStore($slug)
	{
                $data = Input::all();
		$project = Project::whereSlug($slug)->first();
                
                $validator = Validator::make($data, []);
                $validator->each('value',['required|greaterthan:0']);
                $validator->each('name',['required|max:37']);
                $validator->each('result',['required']);
                
                $attrNames = [];
                foreach(range(0,count($data['name'])) as $i) {
                    $attrNames['value.'.$i] = 'Meta';
                    $attrNames['name.'.$i] = 'Nome da Meta';
                    $attrNames['result.'.$i] = 'Resultado';
                }
                
                $validator->setAttributeNames($attrNames); 
                
                if($validator->fails()) {
                    return Redirect::route('project.objective.create',$project->slug)
                        ->withErrors($validator)
                        ->withInput()
                    ;
                }
                
		$edit = false;
		for ($i=0; $i<sizeof($data['name']); $i++) {

			if(isset($data['ids'][$i])){
				$objective = Objective::find($data['ids'][$i]);
				$objective->update(['name'=>$data['name'][$i], 'value'=>Helper::toCents($data['value'][$i]), 'result'=>$data['result'][$i]]);
				$edit = true;
			}else{
				if($data['name'] != '' and $data['value'] != '' and $data['result'] != ''){
					$objective = Objective::create(['name'=>$data['name'][$i], 'value'=>Helper::toCents($data['value'][$i]), 'result'=>$data['result'][$i]]);
					$objective->project()->associate($project);
					$objective->save();
				}
			}
		}
		return Redirect::route('project.reward.create', $project->slug)->withSuccess(Lang::get('project.objective.created'));
		
	}
        
        public function objectiveDelete($slug, $objective_id) 
        {
            $objective = Objective::find($objective_id);
            if($objective->project->user->id != $this->getUser()->id) {
                App::abort(403, 'Unauthorized action.');
            }
            
            $objective->delete();
            return Response::json(array('id' => $objective_id, 'success' => true));
        }

	public function rewardCreate($slug)
	{
		$project = Project::whereSlug($slug)->first();

		$this->layout->content = View::make('projects.reward', compact('project'));
	}
        
        public function rewardDelete($slug, $reward_id) 
        {
            $reward = Reward::find($reward_id);
            if($reward->project->user->id != $this->getUser()->id) {
                App::abort(403, 'Unauthorized action.'.$reward->project->user->id.' - '.$this->getUser()->id);
            }
            
            $reward->delete();
            return Response::json(array('id' => $reward_id, 'success' => true));
        }


	public function accountShow($slug)
	{
		$project = Project::whereSlug($slug)->first();
		
		$this->layout->content = View::make('accounts.project.show', compact('project'));
	}

	/**
	* Administração
	**/

	public function index()
	{
		$p = Project::orderBy('id','desc');

		if(Input::has('n')){
			$p->where('name','like','%'.Input::get('n').'%');
		}

		$projects = $p->paginate(15);

		return View::make('admin.projects.index', compact('projects'));
	}
	public function show($id)
	{
		$project = Project::find($id);

		return View::make('admin.projects.show', compact('project'));
	}

	private function parse_youtube($url)
    {
        if(strlen($url) > 11)
        {
            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match))
            {
                return $match[1];
            }
            else
                return false;
        }

        return $url;
    }

    private function parse_vimeo($link){
		
		$regexstr = '~
			# Match Vimeo link and embed code
			(?:&lt;iframe [^&gt;]*src=")?		# If iframe match up to first quote of src
			(?:							# Group vimeo url
				https?:\/\/				# Either http or https
				(?:[\w]+\.)*			# Optional subdomains
				vimeo\.com				# Match vimeo.com
				(?:[\/\w]*\/videos?)?	# Optional video sub directory this handles groups links also
				\/						# Slash before Id
				([0-9]+)				# $1: VIDEO_ID is numeric
				[^\s]*					# Not a space
			)							# End group
			"?							# Match end quote if part of src
			(?:[^&gt;]*&gt;&lt;/iframe&gt;)?		# Match the end of the iframe
			(?:&lt;p&gt;.*&lt;/p&gt;)?		        # Match any title information stuff
			~ix';
		
		preg_match($regexstr, $link, $matches);
		
		return $matches[1];
		
	}
}
