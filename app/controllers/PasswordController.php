<?php
class PasswordController extends BaseController {
 	
 	protected $layout  = 'layouts.application';

	public function remind()
	{
	  $this->layout->content = View::make('password.remind');
	}

  	public function request()
	{
		$Validator = Validator::make(Input::all(), ['email'=>'required|email']);

		if($Validator->fails()){
			return Redirect::route('password.remind')->withErrors($Validator)->withInput();
		}

	 	try
		{
		    $user = Sentry::findUserByLogin(Input::get('email'));

		    $resetCode = $user->getResetPasswordCode();

		    $data = array('user'=>$user, 'resetCode'=>$resetCode);

		    Mail::send('emails.auth.reminder', $data, function($message)
			{
			  $message->to(Input::get('email'), 'Bepartus nova senha')
			          ->subject('Bepartus nova senha');
			});

			return Redirect::route('password.remind')->with('success','Foi enviado para o seu email instruções para criação de uma nova senha!');
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		   return Redirect::route('password.remind')->with('error','Usuário não encontrado!');
		}
	}

	public function updatePass()
	{
		$user = $this->getUser();
		$data = Input::all();
		$validator = Validator::make($data, ['password'=>'required|confirmed']);
		if($validator->fails()){
			return Redirect::back()->withErrors($validator);
		}

		$user->password = Hash::make($data['password']);
		$user->save();

		return Redirect::back()->withSuccess('Nova senha salva com sucesso. Não se esqueça ao próximo login entrar com esta nova senha.');
	}

	public function reset($token)
	{
		try{
			$user = Sentry::findUserByResetPasswordCode($token);
			 $this->layout->content = View::make('password.reset', compact('user'))->with('token', $token);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e){
			return Redirect::to('/')->with('error','Token já utilizado');
		}
	 
	}
	public function update($token)
	{
		$data =Input::all();
		
		$validate = Validator::make($data, ['password'=>'required|min:4','password_confirmation'=>'required|same:password'], ['password.required'=>'Favor preencher senha','password.min'=>'Senha deve conter mais de 4 caracteres','password_confirmation.required'=>'Favor preencher confirmação de senha','password_confirmation.same'=>'Sua confirmação deve ser igual ao campo senha']);

		if($validate->fails()){
			return Redirect::back()->withInput()->withErrors($validate);
		}else{
			
			 try
			{
			    // Find the user using the user id
			   $user = Sentry::findUserByResetPasswordCode($token);

			    // Check if the reset password code is valid
			    if ($user->checkResetPasswordCode($token))
			    {
			        // Attempt to reset the user password
			        if ($user->attemptResetPassword($token, Input::get('password')))
			        {
			            // Password reset passed
			            return Redirect::route('auth.login')->with('success','Senha alterada com sucesso, favor logar com nova senha!');
			        }
			        else
			        {
			            // Password reset failed
			            return Redirect::back()->with('error','Erro ao enviar nova senha');
			        }
			    }
			    else
			    {
			        // The provided password reset code is Invalid
			        return Redirect::back()->with('error','Código ou url incorreto, favor verificar seu E-mail com o link correto, para fazer nova senha.');
			    }
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    return Redirect::to('auth.login')->with('error','Usuário não encontrado');
			}
		}
	}

	public function userUpdate(){
		$data =Input::all();
		
		$validate = Validator::make($data, ['password'=>'required|min:4','password_confirmation'=>'required|same:password'], ['password.required'=>'Favor preencher senha','password.min'=>'Senha deve conter mais de 4 caracteres','password_confirmation.required'=>'Favor preencher confirmação de senha','password_confirmation.same'=>'Sua confirmação deve ser igual ao campo senha']);

		if($validate->fails()){
			return Redirect::back()->withInput()->withErrors($validate);
		}

		$user = $this->getUser();

		$user->password = Hash::make($data['password']);
		$user->save();

		return Redirect::route('user.edit')->withSuccess('Senha alterada com sucesso.');
	}
}