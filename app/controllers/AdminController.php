<?php
class AdminController extends \BaseController {

	public function login()
	{
		return View::make('admin2.index');
	}
	public function postLogin()
	{
		$data = Input::all();

		$validator = Validator::make($data, ['email'=>'required|email', 'password'=>'required']);

		if($validator->fails()){
			return Redirect::route('admin.login')->withErrors($validator)->withInput();
		}
		try {
			$user = Sentry::authenticate(Input::only('email', 'password'), Input::get('remember-me', 0));

			return Redirect::route('admin.dashboard.index');

		}catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		
			return Redirect::route('admin.login')->with('error','Usuário não encontrado')->withInput(Input::except('password'));
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
			return Redirect::route('admin.login')->with('error','Usuário não ativado!')->withInput(Input::except('password'));
		}
	}
	public function logout()
	{
		Sentry::logout();
		return Redirect::route('admin.login');
	}

}