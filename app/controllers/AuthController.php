<?php
class AuthController extends BaseController {

	protected $layout = 'layouts.application';

	public function register()
	{
		$this->layout->content = View::make('auth.register');
	}
	public function postRegister()
	{
		$data = Input::all();

		$validator = Validator::make($data, User::$rules);

		if($validator->fails()){
			return Redirect::route('auth.register')->withErrors($validator)->withInput();
		}

		$user = Sentry::register([
			'name' => $data['name'],
			'email' =>$data['email'],
			'password' => $data['password'],
			'activated' => true
		]);

		$userdata = Userdata::create($data);
		$userdata->user_id = $user->id;
		$userdata->save();
                
                Event::fire(UserEventHandler::ON_REGISTRATION, array($user));

		// Enviando email de boas vindas
		Mail::send('emails.auth.register', ['user'=>$user], function($m){
			$m->to(Input::get('email'), Input::get('name'))->subject('Bem vindo ao Bepartus!');
		});

		Sentry::login($user);
                Event::fire(UserEventHandler::ON_LOGIN, array($user));

		// Se o usuário tiver comprado patrocínio
		if(Session::has('buy_intent')){
			$intent = Session::get('buy_intent');
			Session::forget('buy_intent');
			return Redirect::route('project.subscribe', $intent);
		}

		return Redirect::route('user.account.sponsor');

	}

	public function login()
	{
		$this->layout->content = View::make('auth.login');
	}

	public function postLogin()
	{
		$data = Input::all();

		$validator = Validator::make($data, ['email'=>'required|email', 'password'=>'required']);

		if($validator->fails()){
			return Redirect::route('auth.login')->withErrors($validator)->withInput();
		}
		try {
			$user = Sentry::authenticate(Input::only('email', 'password'), Input::get('remember-me', 0));
                        Event::fire(UserEventHandler::ON_LOGIN, array($user));
                        
			$view = $user->hasAccess('producer') ? 'user.account.producer' :'user.account.sponsor';

			// Se o usuário tiver comprado patrocínio
			if(Session::has('buy_intent')){
				$intent = Session::get('buy_intent');
				Session::forget('buy_intent');
				return Redirect::route('project.subscribe', $intent);
			}

			return Redirect::route($view);

		}catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		
			return Redirect::route('auth.login')->with('error','Usuário não encontrado')->withInput(Input::except('password'));
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
			return Redirect::route('auth.login')->with('error','Usuário não ativado!')->withInput(Input::except('password'));
		}
		
	}

	public function logout()
	{
		Sentry::logout();

		return Redirect::route('auth.login')->withSuccess('Deslogado com sucesso!');
	}

}
