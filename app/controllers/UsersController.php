<?php

class UsersController extends BaseController {

	protected $layout = 'layouts.application';

	public function index()
	{
		$u = User::orderBy('id','desc');

		if(Input::has('n')){
			$u->where('name', 'like', '%'.Input::get('n').'%');
		}

		$users = $u->paginate(20);

		return View::make('admin.users.index', compact('users'));
	}

	public function profileSponsor()
	{
		$user = $this->getUser();
		
		if(isset($user->userdata->city_id)){
			$cities = $user->userdata->city_id != '0' ? City::whereState_id($user->userdata->city->state->id)->orderBy('name')->lists('name','id') : array();
		}elseif(Input::has('state_id')){
			$cities = City::whereState_id(Input::get('state_id'))->orderBy('name')->lists('name','id');
		}else{
			$cities = array();
		}

		$states = State::orderBy('name')->lists('name','id');

		$this->layout->content = View::make('accounts.sponsor.show', compact('user','states','cities'));
	}

	public function uploadImage()
	{
		$user = $this->getUser();

		
		if($_FILES['user_image']['error'] == '0'){
			echo $image = $this->upload_file(Input::file('user_image'));
			$userdata = Userdata::find($user->userdata->id);
			if($userdata->image != ''){
				if(File::exists('uploads/thumbs/'.$userdata->image)){
					File::delete('uploads/thumbs/'.$userdata->image);
				}
				if(File::exists('uploads/normal/'.$userdata->image)){
					File::delete('uploads/normal/'.$userdata->image);
				}
			}
			$userdata->image = $image;
			$userdata->save();
		}elseif($_FILES['user_image']['error'] != '0'){
			return Redirect::route('user.account.sponsor')->withError($this->uploadErrors($_FILES['user_image']['error']));
		}

		return Redirect::route('user.account.sponsor')->withSuccess(Lang::get('user.upload.success'));
	}

	public function updateUserdata()
	{
		$data = Input::all();

		$validator = Validator::make($data, Userdata::$rules);
		$validator_user = Validator::make($data, User::$update_rules);
		if($validator_user->fails()){
			return Redirect::back()->withErrors($validator_user)->withInput();
		}
		if($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput();
		}

                $user = $this->getUser();
                $user->name = $data['name'];
                $user->email = $data['email'];
                $user->userdata->cpf = $data['cpf'];
                $user->userdata->address = $data['address'];
                $user->userdata->city_id = $data['city_id'];
                $user->push();

		return Redirect::back()->withSuccess(Lang::get('user.edit.success'));
	}

	public function createProducer()
	{
		$user = $this->getUser();
		$cities = array();
		if(Input::old('state') || isset($user->userdata->city->id)){
			
			$c = City::query();
			
			if(isset($user->userdata->city->id)){
				$c->whereState_id($user->userdata->city->state->id);
			}else{
				$c->whereHas('state', function($e){
					$e->whereName(Input::old('state'));
				});
			}
			$cities = $c->orderBy('name')->lists('name','id');
		}
		$states = State::orderBy('name')->lists('name','name');

		$doc_rg = $doc_cpf = $doc_activity = null;

		if(isset($user->producer->id)){
			$doc_rg = $user->producer->doc_rg;
			$doc_cpf = $user->producer->doc_cpf;
			$doc_activity = $user->producer->doc_activity;
		}

		$this->layout->content = View::make('accounts.producer.subscription', compact('user','cities','states','doc_rg','doc_cpf','doc_activity'));
	}

	public function deactivate($id)
	{
		$user = User::find($id);

		$user->activated = $user->activated == '1' ? '0' : '1';
		$user->save();
		$act = $user->activated == '1' ? 'ativado' : 'desativado';
		
		return Redirect::back()->withSuccess('Usuário '.$act);
	}
	private function uploadErrors($error){
		switch ($error) {
			case '1':
				$res = 'O arquivo enviado excede o limite definido de tamanho 2MB';
			break;
			case '1':
				$res = 'O arquivo excede o limite definido em 2MB';
			break;
			case '2':
				$res = 'O upload do arquivo foi feito parcialmente.';
			break;
			case '3':
				$res = 'Nenhum arquivo foi enviado.';
			break;
			case '4':
				$res = 'Pasta temporária ausênte';
			break;
			case '6':
				$res = 'Falha em escrever o arquivo em disco';
			break;
			case '7':
				$res = '';
			break;
			default:
				$res = '';
				break;
		}; 
		return $res;
	}

}
