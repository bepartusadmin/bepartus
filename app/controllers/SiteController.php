<?php

class SiteController extends BaseController {

	protected $layout = 'layouts.application';

	public function index()
	{
		$projects = Project::orderBy('id','desc')->active()->confirmed()->take(4)->get();
		$projects_featured = Project::orderBy('id','desc')->active()->confirmed()->take(4)->get();
		$projects_highlights = Project::active()->confirmed()->orderByRaw('(SELECT SUM(amount) FROM subscriptions as s WHERE s.project_id = projects.id  ) desc')->take(4)->get();
		$this->layout->content = View::make('site.index', compact('projects','projects_featured', 'projects_highlights'));
	}

	public function about()
	{
		$this->layout->content = View::make('site.about');
	}

	public function faq()
	{
		$this->layout->content = View::make('site.faq');
	}

	public function terms()
	{
		$this->layout->content = View::make('site.terms');
	}

	public function getContact()
	{
		$this->layout->content = View::make('site.contact');
	}

	public function postContact()
	{
		$data = Input::all();

		$validator = Validator::make($data, ['name'=>'required','email'=>'required|email', 'msg'=>'required']);

		if($validator->fails()){
			return Redirect::route('get.contact')->withErrors($validator)->withInput();
		}

		Mail::send('emails.site.contact', ['data'=>$data], function($m){
			$m->to('contato@bepartus.com','Contato Bepartus')->subject('Novo contato site Bepartus');
		});

		return Redirect::route('get.contact')->withSuccess('Mensagem enviada, obrigado. Em breve entraremos em contato.');
	}
        
        public function webhooks() {
            Event::fire(Input::get('event'), ['data' => Input::get('data')]);
            return 'OK';
        }

}
