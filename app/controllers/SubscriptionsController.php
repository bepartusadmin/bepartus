<?php

class SubscriptionsController extends BaseController {

    protected $layout = 'layouts.application';
    protected $user;

    public function createAccount() 
    {
        $data = Input::all();
        $this->user = $this->getUser();

        $validator = Validator::make(Input::all(), $this->getProducerRoles());
        $validator->setAttributeNames($this->getProducerAttributeNames());

        if ($validator->fails()) {
            return Redirect::route('producer.create')->withErrors($validator)->withInput();
        }
        
        # Aqui eu gero as restrições para se criar produtor
        $this->handleProducerFiles();

        if (isset($this->user->producer->iugu_user_token)) 
        {
            //Se usuário já existe, atualizamos os dados:
            $this->updateUser();

            //Quando a conta já foi verificada, não é possivel enviar
            //outra solicitação de verificação. Apenas atualização de dados bancários
            $verify = $this->verifyAccount($this->user->producer->iugu_account_id, $this->user->producer->iugu_user_token);
            if ($verify['is_verified?'] == '1' and $verify['can_receive?'] == '1') 
            {
                $response = $this->sendBankVerification($this->user->producer, [
                    'agency' => Input::get('data.bank_ag'),
                    'account' => Input::get('data.bank_cc'),
                    'account_type' => Helper::getAccountType(Input::get('data.account_type')),
                    'bank' => Helper::getBankCode(Input::get('data.bank')),
                ]);

                if (isset($response['errors'])) {
                    return Redirect::route('producer.create')->withError('Ocorreu algum erro ao solicitar esta verificação, favor tente mais tarde.')->withErrors($response['errors']);
                }

                $this->user->producer->bank_ag = Input::get('data.bank_ag');
                $this->user->producer->bank = Input::get('data.bank');
                $this->user->producer->account_type = Input::get('data.account_type');
                $this->user->producer->bank_cc = Input::get('data.bank_cc');
                $this->user->push();
            } 
            else 
            {
                $res = $this->sendVerification($this->user->producer->iugu_user_token, $this->user->producer->iugu_account_id, $data);

                if (isset($res['errors'])) {
                    return Redirect::route('producer.create')->withError('Ocorreu algum erro ao solicitar esta verificação, favor tente mais tarde.')->withErrors($res['errors']);
                }

                $producer = Producer::find($this->user->producer->id);
                $producer->status = '1';
                $producer->save();

                return Redirect::route('user.account.producer')->withSuccess('Seus dados foram enviados para moderação. Lembre-se que você só poderá receber pelos seus projetos quando estiver com sua conta verificada, portanto envie seus dados de forma correta.');
            }
        }
        
        $response = $this->createMarketPlace([
            'name' => 'Bepartus - Produtor: ' . $this->user->name, 
            'commission_percent' => '5'
        ]);
        
        // Criando produtor para o IUGU
        $producer = Producer::create([
            'iugu_account_id' => $response['account_id'],
            'iugu_name' => $response['name'],
            'iugu_user_token' => $response['user_token'],
            'iugu_test_api_token' => $response['test_api_token'],
            'iugu_live_api_token' => $response['live_api_token']
            ] + $data['data']
        );
        $producer->user()->associate($this->user);
        $producer->save();

        $res = $this->sendVerification($producer->iugu_user_token, $producer->iugu_account_id, $data);

        if (isset($res['errors'])) {
            return Redirect::route('producer.create')->withError('Ocorreu algum erro ao solicitar esta verificação, favor tente mais tarde.')->withErrors($res['errors']);
        }

        $producer->status = '1';
        $producer->save();

        return Redirect::route('user.account.producer')->withSuccess('Seus dados foram enviados para moderação. Lembre-se que você só poderá receber pelos seus projetos quando estiver com sua conta verificada,portanto envie seus dados corretamente.');
    }
    
    private function getProducerRoles() {
        return [
            'data.business_type' => 'required',
            'data.name' => 'required',
            'data.email' => 'required|email|unique:users,email,' . $this->user->id,
            'data.cpf' => 'required|unique:userdatas,cpf,' . $this->user->id . ',user_id',
            'data.address' => 'required',
            'state' => 'required',
            'city' => 'required',
            'data.cep' => 'required',
            'data.telephone' => 'required',
            'data.bank_ag' => 'required',
            'data.bank_cc' => 'required',
        ];
    }
    
    private function getProducerAttributeNames() {
        return [
            'data.business_type' => 'Descrição do Projeto',
            'data.name' => 'Nome',
            'data.email' => 'E-mail',
            'data.cpf' => 'CPF',
            'data.address' => 'Endereço',
            'state' => 'Estado',
            'city' => 'Cidade',
            'data.cep' => 'CEP',
            'data.telephone' => 'Telefone',
            'data.bank_ag' => 'Agência',
            'data.bank_cc' => 'Conta',
        ];
    }
    
    private function updateUser() 
    {
        $this->user->name = Input::get('data.name');
        $this->user->email = Input::get('data.email');
        $this->user->userdata->cpf = Input::get('data.cpf');
        $this->user->userdata->address = Input::get('data.address');
        $this->user->userdata->city_id = Input::get('city');
        $this->user->producer->telephone = Input::get('data.telephone');
        $this->user->producer->cep = Input::get('data.cep');
        $this->user->producer->business_type = Input::get('data.business_type');
        $this->user->producer->physical_products = Input::get('data.physical_products');
        $this->user->push();
    }
    
    private function handleProducerFiles() 
    {
        if (isset($this->user->producer->id)) 
        {
            $rg_path = $this->user->producer->doc_rg != '' ? asset('uploads/archives/users/' . $this->user->producer->doc_rg) : null;
            $cpf_path = $this->user->producer->doc_cpf != '' ? asset('uploads/archives/users/' . $this->user->producer->doc_cpf) : null;
            $activity_path = $this->user->producer->doc_activity != '' ? asset('uploads/archives/users/' . $this->user->producer->doc_activity) : null;
        }

        if (Input::hasFile('files[id]')) 
        {
            $file = Input::file('files[id]');
            $file->move('uploads/archives/users', 'arq_' . $this->user->id . '_rg.' . $file->getClientOriginalExtension());

            $rg_path = asset('uploads/archives/users', 'arq_' . $this->user->id . '_rg.' . $file->getClientOriginalExtension());

            if (isset($this->user->producer->id)) {
                if ($this->user->producer->doc_rg != '') {
                    File::delete('uploads/archives/users/' . $this->user->producer->doc_rg);
                }
            }
        }

        if (Input::hasFile('files[cpf]')) 
        {
            $file = Input::file('files[cpf]');
            $file->move('uploads/archives/users', 'arq_' . $this->user->id . '_cpf.' . $file->getClientOriginalExtension());

            $cpf_path = asset('uploads/archives/users', 'arq_' . $this->user->id . '_cpf.' . $file->getClientOriginalExtension());

            if (isset($this->user->producer->id)) {
                if ($this->user->producer->doc_cpf != '') {
                    File::delete('uploads/archives/users/' . $this->user->producer->doc_cpf);
                }
            }
        }

        if (Input::hasFile('files[activity]')) 
        {
            $file = Input::file('files[activity]');
            $file->move('uploads/archives/users', 'arq_' . $this->user->id . '_activity.' . $file->getClientOriginalExtension());

            $activity_path = asset('uploads/archives/users', 'arq_' . $this->user->id . '_activity.' . $file->getClientOriginalExtension());


            if (isset($this->user->producer->id)) {
                if ($this->user->producer->doc_activity != '') {
                    File::delete('uploads/archives/users/' . $this->user->producer->doc_activity);
                }
            }
        }
    }

    public function rewardStore($slug) {
        $data = Input::all();
        $project = Project::whereSlug($slug)->first();

        $validator = Validator::make($data, []);
        $validator->each('value', ['required|greaterthan:0']);
        $validator->each('title', ['required|max:255']);
        $validator->each('description', ['required']);

        $attrNames = [];
        foreach (range(0, count($data['title'])) as $i) {
            $attrNames['value.' . $i] = 'Valor';
            $attrNames['title.' . $i] = 'Título';
            $attrNames['description.' . $i] = 'Descrição';
        }

        $validator->setAttributeNames($attrNames);

        if ($validator->fails()) {
            return Redirect::route('project.reward.create', $project->slug)
                            ->withErrors($validator)
                            ->withInput()
            ;
        }

        $edit = false;

        for ($i = 0; $i < sizeof($data['title']); $i++) {
            if (isset($data['ids'][$i])) {
                $reward = Reward::find($data['ids'][$i]);
                $reward->update(['title' => $data['title'][$i], 'value' => Helper::toCents($data['value'][$i]), 'description' => $data['description'][$i]]);
                $edit = true;
            } else {
                if ($data['title'][$i] != '' and $data['value'][$i] != '' and $data['description'][$i] != '') 
                {
                    // Criando um plano no IUGU                    
                    $res = $this->createPlan($project, [
                        'name' => $data['title'][$i] . ' - ' . $project->name,
                        'identifier' => 'bepartus_' . uniqid(),
                        'interval' => '1',
                        'interval_type' => 'months',
                        'currency' => 'BRL',
                        'value_cents' => $data['value'][$i] * 100
                    ]);

                    if (isset($res['identifier'])) {
                        $reward = Reward::create(['title' => $data['title'][$i], 'value' => Helper::toCents($data['value'][$i]), 'description' => $data['description'][$i], 'iugu_plan_id' => $res['id'], 'iugu_plan_identifier' => $res['identifier']]);
                        $reward->project()->associate($project);
                        $reward->save();
                    }
                }
            }
        }

        $user = $this->getUser();

        if (isset($user->producer->id)) 
        {

            if ($user->producer->status != '2') {
                return Redirect::route('projects.account.show', $project->slug)->withSuccess(Lang::get('project.created'))->withWarning('Você possui uma verificação pendente, portante seu projeto ainda pode nnão aparecer no site, enquanto isto aguarde. Assim verificada sua conta, seus projetos são liberados, aproveite.');
            }
            return Redirect::route('projects.account.show', $project->slug)->withSuccess(Lang::get('project.created'));
        }

        return Redirect::route('producer.create');
    }

    public function cancel($subscription_id) 
    {
        $user = $this->getUser();
        $subscription = Subscription::where('id', $subscription_id)->where('user_id', $user->id)->first();
        if (!$subscription) {
            App::abort(403, 'Unauthorized action.');
        }

        if (App::environment('local', 'staging')) {
            Iugu::setApiKey($subscription->project->user->producer->iugu_test_api_token);
        } else {
            Iugu::setApiKey($subscription->project->user->producer->iugu_live_api_token);
        }
        
        try 
        {
            if(empty($subscription->iugu_subscription_id)){
                throw new IuguObjectNotFound();
            }
            $sub = Iugu_Subscription::fetch($subscription->iugu_subscription_id);
            $sub->delete();
            $subscription->delete();
            Event::fire(UserEventHandler::ON_PROJECT_SUBSCRIPTION_CANCEL, array($subscription, $user));
            return Redirect::route('user.account.sponsor')->withSuccess('Patrocínio cancelado com sucesso');
        } catch (IuguObjectNotFound $e) 
        {
            $subscription->delete();
            Event::fire(UserEventHandler::ON_PROJECT_SUBSCRIPTION_CANCEL, array($subscription, $user));
            return Redirect::route('user.account.sponsor')->withSuccess('Patrocínio cancelado com sucesso');
        } catch (IuguException $e) 
        {
            return Redirect::route('user.account.sponsor')
                ->withError('Erro ao suspender patrocinio, tente novamente mais tarde ou entre em contato conosco.'.$e->getMessage())
            ;
        }
    }

    public function profileProducer() {
        $user = $this->getUser();


        // verifica pendencias da conta do IUGU
        $verify = $this->verifyAccount($user->producer->iugu_account_id, $user->producer->iugu_user_token);

        if ($verify['is_verified?'] == '1' and $verify['can_receive?'] == '1') {
            $producer = Producer::whereUser_id($user->id)->first();
            $producer->status = '2';
            $producer->save();

            // Deixando todos os projetos com doc_status ok
            $projects = Project::whereIn('id', $user->projects->lists('id'))->update(['status_docs' => '1']);
        }

        if (isset($user->userdata->city_id)) {
            $cities = $user->userdata->city_id != '0' ? City::whereState_id($user->userdata->city->state->id)->orderBy('name')->lists('name', 'id') : array();
        } elseif (Input::has('state_id')) {
            $cities = City::whereState_id(Input::get('state_id'))->orderBy('name')->lists('name', 'id');
        } else {
            $cities = array();
        }

        $states = State::orderBy('name')->lists('name', 'id');

        $this->layout->content = View::make('accounts.producer.show', compact('user', 'states', 'cities'));
    }
    
    /**
     * Subscrição
     * - @getSubscribe
     * - Página de ccriação da assinatura onde mostro todas as recompensas
     *
     * - @paymentSubscribe
     * - Levo a recompensa via post e redireciono para o checkou com dados de cartão
     *
     * - @getSubscribeCheckout
     * - Página de checkou para usuario preencher dados de cartão
     *
     * - @postSubscribe
     * -  Ação onde é gerado o pagamento
     * */
    public function getSubscribe($slug) {
        $project = Project::whereSlug($slug)->first();

        if ($project->rewards->count() < '1') {
            //return Redirect::back()->withError('Não possui recompensa por enquanto');
        }

        $this->layout->content = View::make('subscriptions.create', compact('project'));
    }

    public function paymentSubscribe($slug) {
        $data = Input::all();

        $project = Project::whereSlug($slug)->first();



        if (Input::has('amount')) {
            Session::set('checkout_amount', Helper::toCents($data['amount']));
        } else {
            Session::set('checkout_reward', $data['reward_id']);
        }


        return Redirect::route('project.subscribe.checkout', $slug);
    }

    public function getSubscribeCheckout($slug) {

        $project = Project::whereSlug($slug)->first();

        $user = $this->getUser();


        /* Session::forget('checkout_amount');
          Session::forget('checkout_reward'); */

        // Verifica se ja existe uma subscrição para esta recompensa e projeto
        /* $subscription = Subscription::whereUser_id($user->id)->whereProject_id($project->id)->whereReward_id($reward->id)->first();

          if(isset($subscription->id)){
          return Redirect::action('project.show', $project->slug)->withWarning('Você já apoia esse projeto e já escolheu esta recompensa. Tente patrocinar com outra recompensa. :)');
          } */

        $this->layout->content = View::make('subscriptions.payment', compact('project', 'user'));
    }
    
    /**
     * Refactored postSubscribe method
     * @param type $slug The Project Slug
     */
    public function postSubscribe($slug) 
    {
        $data = Input::all();
        
        if (!Input::has('has_registered')) {
            return Redirect::back()->withError('Selecione uma forma de pagamento');
        }
        
        $details = [];
        $project = Project::whereSlug($slug)->first();

        if (App::environment('local', 'staging')) {
            Iugu::setApiKey($project->user->producer->iugu_test_api_token);
        } else {
            Iugu::setApiKey($project->user->producer->iugu_live_api_token);
        }

        $user = $this->getUser();
        $user->load('customer');        

        if (Session::has('checkout_amount')) 
        {
            $reward = false;
            $amount = Session::get('checkout_amount');
            $pfr = '0';
            $details['description'] = sprintf('%s para o projeto %s', intval($amount) / 100, $project->name);
            $details['reward_id'] = null;
            $details['plan_identifier'] = '';
        } else {
            $reward = Reward::find(Session::get('checkout_reward'));
            $amount = $reward->value;
            $pfr = '1';
            $details['description'] = sprintf('%s para o projeto %s', $reward->title, $project->name);
            $details['reward_id'] = $reward->id;
            $details['plan_identifier'] = $reward->iugu_plan_identifier;
        }
        
        if (!isset($user->customer->id)) {
            $user = $this->customerFactory($user);
        }
        
        $data_buy = $this->buyerDataFactory($user, $project);
        $payment_method = Input::get('has_registered') == '2' ? 'boleto' : (Input::get('has_registered') == '0' ? 'cartao' : false);
        
        # 1 - cria-se a assinatura do plano
        $iugu_sub = Iugu_Subscription::create(['plan_identifier' => $details['plan_identifier'], 'customer_id' => $user->customer->iugu_customer_id, 'expires_at' => Helper::AdicionarMeses(date('Y-m-d'), 1), 'subitems' => [[
            'description' => $details['description'],
            'quantity' => '1',
            'price_cents' => ($amount),
            'recurrent' => true
        ]]]);

        # 2 - cria-se a fatura
        $invoice = Iugu_Invoice::create([ 'email' => $user->email, 'due_date' => date('d/m/Y'), 'items' => [[
            'description' => $details['description'],
            'quantity' => '1',
            'price_cents' => ($amount),
        ]], 'subscription_id' => $iugu_sub->id ]);
                    
        switch ($payment_method) {
            case 'cartao':
                $payment = Iugu_Charge::create(['token' => $data['iugu_api_token'], 'invoice_id' => $invoice->id, $data_buy]);
                break;
            case 'boleto':
                $payment = Iugu_Charge::create([ 'method' => 'bank_slip', 'invoice_id' => $invoice->id, $data_buy ]);
                break;
            default:
                $payment = new stdClass();
                $payment->errors = ['Método de pagamento desconhecido'];    
        }

        if ($payment->errors) {
            return Redirect::route('project.subscribe.checkout', $project->slug)->withErrors($payment->errors);
        }

        if ($payment->success == 'true') {
            $subscription = Subscription::create([
                'project_id' => $project->id, 
                'user_id' => $user->id, 
                'reward_id' => $details['reward_id'], 
                'statusText' => $payment->message, 
                'iugu_subscription_id' => $iugu_sub->id, 
                'amount' => $amount, 
                'paid_for_reward' => $pfr, 
                'type' => $payment_method == 'boleto' ? '2' : '1', 
                'status' => $payment_method == 'cartao' ? '1' : '0'
            ]);

            Session::forget('checkout_amount');
            Session::forget('checkout_reward');

            // Envia email pela compra            
            Event::fire(UserEventHandler::ON_PROJECT_SUBSCRIPTION, array($subscription, $reward, $payment, $user, $payment_method));
            return $payment_method == 'cartao' ? Redirect::route('user.account.sponsor')->withSuccess('Pagamento realizado com sucesso.') : Redirect::away($payment['url']);
        } else {
            return Redirect::route('project.subscribe.checkout', $project->slug)->withErrors([$payment->message]);
        }
    }
    
    private function customerFactory($user) 
    {
        $new_customer = Iugu_Customer::create([
            'email' => $user->email, 
            'name' => $user->full_name, 
            'notes' => $user->id
        ]);
        
        $customer = Customer::create([
            'iugu_customer_id' => $new_customer->id,
            'iugu_item_type' => '',
            'iugu_data_token' => '',
        ]);

        $user->customer()->save($customer);        
        $user->save();
        return User::with('customer')->find(Sentry::getUser()->id);
    }
    
    private function buyerDataFactory($user, $project) 
    {
        $p_city = isset($project->user->userdata->city->name) ? $project->user->userdata->city->name : 'Cidade não fornecida';
        $p_state = isset($project->user->userdata->city->state->uf) ? $project->user->userdata->city->state->uf : 'Estado não fornecida';
        
        return [
            "customer_id" => $user->customer->iugu_customer_id,
            "email" => $user->email,
            "payer" => [
                "name" => $user->name,
                "phone_prefix" => "1",
                "phone" => "1000",
                "email" => $user->email,
                "address" => [
                    "street" => isset($user->userdata->address) ? $user->userdata->address : 'Endereco nao fornecido',
                    "number" => "001",
                    "city" => isset($user->userdata->city->name) ? $user->userdata->city->name : $p_city,
                    "state" => isset($user->userdata->city->state->uf) ? $user->userdata->city->state->uf : $p_state,
                    "country" => "Brasil",
                    "zip_code" => "12122-00"
                ]
            ]
        ];
    }
    
    #IUGU METHODS
    #@TODO: Move to a service
    private function createMarketPlace($post) 
    {
        Iugu::setApiKey($this->getIuguApiToken(true));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.iugu.com/v1/marketplace/create_account');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->_defaultHeaders());
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

        $json_response = curl_exec($ch);
        $response = json_decode($json_response, true);
        curl_close($ch);
        return $response;
    }

    private function verifyAccount($account_id, $iugu_account_token) 
    {
        Iugu::setApiKey($iugu_account_token);
        
        $ch_two = curl_init();
        curl_setopt($ch_two, CURLOPT_URL, 'https://api.iugu.com/v1/accounts/' . $account_id);
        curl_setopt($ch_two, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_two, CURLOPT_HTTPHEADER, $this->_defaultHeaders());

        $json_response = curl_exec($ch_two);

        $response = json_decode($json_response, true);

        return $response;
    }
    
    private function sendVerification($iugu_user_token, $iugu_account_id, $verification_post) 
    {
        Iugu::setApiKey($iugu_user_token);

        $ch_two = curl_init();
        curl_setopt($ch_two, CURLOPT_URL, 'https://api.iugu.com/v1/accounts/' . $iugu_account_id . '/request_verification');
        curl_setopt($ch_two, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_two, CURLOPT_HTTPHEADER, $this->_defaultHeaders());
        curl_setopt($ch_two, CURLOPT_POSTFIELDS, http_build_query($verification_post));

        $json_response = curl_exec($ch_two);

        $response = json_decode($json_response, true);

        return $response;
    }

    private function sendBankVerification($producer, $data) 
    {        
        if (App::environment('local', 'staging')) {
            Iugu::setApiKey($producer->iugu_test_api_token);
        } else {
            Iugu::setApiKey($producer->iugu_live_api_token);
        }

        $ch_two = curl_init();
        curl_setopt($ch_two, CURLOPT_URL, 'https://api.iugu.com/v1/bank_verification');
        curl_setopt($ch_two, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_two, CURLOPT_HTTPHEADER, $this->_defaultHeaders());
        curl_setopt($ch_two, CURLOPT_POSTFIELDS, http_build_query($data));

        $json_response = curl_exec($ch_two);
        $response = json_decode($json_response, true);

        return $response;
    }
    
    private function createPlan($project, $post) 
    {
        if (App::environment('local', 'staging')) {
            Iugu::setApiKey($project->user->producer->iugu_test_api_token);
        } else {
            Iugu::setApiKey($project->user->producer->iugu_live_api_token);
        }
                
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.iugu.com/v1/plans');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->_defaultHeaders());
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, true);    
    }
    
    /**
     * 
     *  OLD PAYMENT METHOD - WAITING FOR BURIAL
        public function postSubscribeOld($slug) {
        $data = Input::all();

        if (!Input::has('has_registered')) {
            return Redirect::back()->withError('Selecione uma forma de pagamento');
        }

        $project = Project::whereSlug($slug)->first();

        $p_city = isset($project->user->userdata->city->name) ? $project->user->userdata->city->name : 'Cidade nÃ£o fornecida';
        $p_state = isset($project->user->userdata->city->state->uf) ? $project->user->userdata->city->state->uf : 'Estado nÃ£o fornecida';

        if (App::environment('local', 'staging')) {
            Iugu::setApiKey($project->user->producer->iugu_test_api_token);
        } else {
            Iugu::setApiKey($project->user->producer->iugu_live_api_token);
        }

        $user = $this->getUser();
        $user->load('customer');
        
        $details = [];

        if (Session::has('checkout_amount')) {
            //$reward = Reward::whereProject_id($project->id)->orderBy('id','asc')->first();
            $reward = false;
            $amount = Session::get('checkout_amount');
            $pfr = '0';
            $details['description'] = sprintf('%s para o projeto %s', intval($amount) / 100, $project->name);
            $details['reward_id'] = null;
            $details['plan_identifier'] = '';
        } else {
            $reward = Reward::find(Session::get('checkout_reward'));
            $amount = $reward->value;
            $pfr = '1';
            $details['description'] = sprintf('%s para o projeto %s', $reward->title, $project->name);
            $details['reward_id'] = $reward->id;
            $details['plan_identifier'] = $reward->iugu_plan_identifier;
        }

        // Se for boleto bancÃ¡rio
        if (Input::get('has_registered') == '2') {
            $payment = Iugu_Charge::create([
                        "method" => "bank_slip",
                        "email" => $user->email,
                        "items" => [
                            [
                                "description" => $details['description'],
                                "quantity" => "1",
                                "price_cents" => ($amount),
                            ]
                        ],
                        "payer" => [
                            //  "cpf_cnpj" = > "12312312312",
                            "name" => $user->name,
                            "phone_prefix" => "1",
                            "phone" => "1000",
                            "email" => $user->email,
                            "address" => [
                                "street" => isset($user->userdata->address) ? $user->userdata->address : 'Endereco nao fornecido',
                                "number" => "001",
                                "city" => isset($user->userdata->city->name) ? $user->userdata->city->name : $p_city,
                                "state" => isset($user->userdata->city->state->uf) ? $user->userdata->city->state->uf : $p_state,
                                "country" => "Brasil",
                                "zip_code" => "12122-00"
                            ]
                        ]
            ]);

            if (isset($payment['success'])) {
                $subscription = Subscription::create(['project_id' => $project->id, 'user_id' => $user->id, 'reward_id' => $details['reward_id'], 'statusText' => 'Pendente', 'amount' => $amount, 'paid_for_reward' => $pfr, 'type' => '2', 'status' => '0']);
                //echo $payment['url'];
                Event::fire(UserEventHandler::ON_PROJECT_SUBSCRIPTION, array($subscription, $reward, $payment, $user, 'boleto'));
                return Redirect::away($payment['url']);
            }

            return Redirect::route('project.subscribe.checkout', $slug)
                ->withError('Erro ao gerar boleto tente novamente mais tarde')
            ;
        }


        // Se jÃ¡ houver cadastrado o cliente
        if (isset($user->customer->id)) 
        {
            // Se solicitar atravÃ©s do metodo de pagamento ja existente
            $data_buy = [
                "customer_id" => $user->customer->iugu_customer_id,
                "email" => $user->email,
                "payer" => [
                    "name" => $user->name,
                    "phone_prefix" => "1",
                    "phone" => "1000",
                    "email" => $user->email,
                    "address" => [
                        "street" => isset($user->userdata->address) ? $user->userdata->address : 'Endereco nao fornecido',
                        "number" => "001",
                        "city" => isset($user->userdata->city->name) ? $user->userdata->city->name : $p_city,
                        "state" => isset($user->userdata->city->state->uf) ? $user->userdata->city->state->uf : $p_state,
                        "country" => "Brasil",
                        "zip_code" => "12122-00"
                    ]
            ]];

            // Se ja for cadastrado e for cartÃ£o de crÃ©dito novo
            if (Input::get('has_registered') == '0') 
            {
                # 1 - cria-se a assinatura do plano
                $iugu_sub = Iugu_Subscription::create([
                    "plan_identifier" => $details['plan_identifier'],
                    "customer_id" => isset($user->customer->iugu_customer_id) ? $user->customer->iugu_customer_id : $system_customer->iugu_customer_id,
                    "expires_at" => Helper::AdicionarMeses(date('Y-m-d'), 1),
                    "subitems" => [[
                        "description" => $details['description'],
                        "quantity" => "1",
                        "price_cents" => ($amount),
                        "recurrent" => true
                    ]]
                ]);

                # 2 - cria-se a fatura
                $invoice = Iugu_Invoice::create([
                    "email" => $user->email,
                    "due_date" => date('d/m/Y'),
                    "items" => [[
                        "description" => $details['description'],
                        "quantity" => "1",
                        "price_cents" => ($amount),
                    ]],
                    "subscription_id" => $iugu_sub->id
                ]);

                $payment = Iugu_Charge::create(Array(
                    "token" => $data['iugu_api_token'],
                    "invoice_id" => $invoice->id,
                    $data_buy
                ));

                if ($payment->errors) {
                    return Redirect::route('project.subscribe.checkout', $project->slug)
                        ->withErrors($payment->errors)
                    ;
                }

                if ($payment->success == 'true') {
                    $subscription = Subscription::create(['project_id' => $project->id, 'user_id' => $user->id, 'reward_id' => $details['reward_id'], 'statusText' => $payment->message, 'iugu_subscription_id' => $iugu_sub->id, 'amount' => $amount, 'paid_for_reward' => $pfr, 'type' => '0', 'status' => '1']);

                    Session::forget('checkout_amount');
                    Session::forget('checkout_reward');

                    // Envia email pela compra
                    Event::fire(UserEventHandler::ON_PROJECT_SUBSCRIPTION, array($subscription, $reward, $payment, $user, 'cartao'));
                    return Redirect::route('user.account.sponsor')->withSuccess('Pagamento realizado com sucesso.');
                }
            }
            //print_r($payment);
        } else {
            $new_customer = Iugu_Customer::create(["email" => $user->email, "name" => $user->full_name, "notes" => $user->id]);
            $customer = Iugu_Customer::fetch($new_customer->id);

            $d = explode('/', $data['expiration']);
            list($firstname, $lastname) = explode(' ', $data['full_name'], 2);

            $payment_method = $customer->payment_methods()->create([
                "description" => "CartÃ£o de crÃ©dito do cliente " . $user->name,
                "item_type" => "credit_card",
                "data" => [
                    "number" => $data['number'],
                    "verification_value" => $data['verification_value'],
                    "first_name" => $firstname,
                    "last_name" => $lastname,
                    "month" => $d[0],
                    "year" => $d[1]
                ]
            ]);
            
            if (!isset($payment_method->id)) {
                return Redirect::back()->withErrors($payment_method->errors['data']);
            }

            $system_customer = Customer::create([
                'iugu_customer_id' => $new_customer->id,
                'iugu_item_type' => $payment_method->item_type,
                'iugu_data_token' => $payment_method->id,
            ]);

            $system_customer->user()->associate($user);
            $system_customer->save();
        }

        $iugu_sub = Iugu_Subscription::create(Array(
            "plan_identifier" => $details['plan_identifier'],
            "customer_id" => isset($user->customer->iugu_customer_id) ? $user->customer->iugu_customer_id : $system_customer->iugu_customer_id,
            "only_on_charge_success" => true,
            "expires_at" => Helper::AdicionarMeses(date('Y-m-d'), 1),
            "subitems" => [[
                "description" => $details['description'],
                "quantity" => "1",
                "price_cents" => ($amount),
                "recurrent" => true
            ]]
        ));

        if (isset($iugu_sub->errors)) {
            return Redirect::route('project.subscribe.checkout', $slug)
                ->withErrors($iugu_sub->errors)
            ;
        }

        $subscription = Subscription::create(['project_id' => $project->id, 'user_id' => $user->id, 'reward_id' => $details['reward_id'], 'statusText' => 'Autorizado', 'iugu_subscription_id' => $iugu_sub->id, 'amount' => $amount, 'paid_for_reward' => $pfr, 'type' => '1', 'status' => '1']);

        if (isset($iugu_sub->id)) {
            Session::forget('checkout_amount');
            Session::forget('checkout_reward');

            Event::fire(UserEventHandler::ON_PROJECT_SUBSCRIPTION, array($subscription, $reward, $payment, $user, 'cartao'));
            return Redirect::route('user.account.sponsor')->withSuccess('Pagamento realizado com sucesso.');
        }
    }
     */

}
