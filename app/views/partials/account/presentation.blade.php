<div class="bgContainer">
  	<div class="container">
    @include('partials.site.notifications')
        <div class="BoxPatrocinador">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-lg-4">
                <div class="fotoPratoc">
                    @include('partials.site.loader')
                    <img src="{{ $user->userdata->image != '' ? asset('uploads/normal/'.$user->userdata->image) : 'http://placehold.it/307X307' }}" alt="{{ $user->name }}">
                    {{ Form::open(['route'=>'user.upload.photo', 'files'=>true]) }}
                        <div class="fakeInpt">
                            <input name="user_image" class="trocarFoto" type="file" />
                        </div>
                        
                    {{ Form::close() }}
                </div>
            </div>
        	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-lg-8">
                <div class="DadosPatroc">
                    <h2>{{ $user->name }}</h2>
                    <p>Apoia {{ $user->subscriptions->count() }} projetos</p>
                    <p>Criador de {{ $user->projects->count() }} projetos</p>
                    @if($route =='user.account.producer')
                        <a href="{{ route('user.account.producer') }}">Conta de Produtor</a>
                    @else
                        <a href="{{ route('producer.create') }}">Trocar informações</a>
                    @endif
                </div>   
            </div>
        </div>
    </div>
</div>