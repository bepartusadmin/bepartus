@if($user->subscriptions->count() > 0)       
<h3>Projetos que sou Patrocinador</h3>  
<div class="boxPatrocinador2">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12">
        <ul>
            @foreach($user->subscriptions as $s)
            <li>
                <img src="{{ $s->project->getImageSrc() }}" alt="" />
                <span class="info">
                    <p class="nomeProjeto">{{ $s->project->name }}</p>
                    <p class="valorProjeto">R${{ Helper::Monetize($s->amount/100) }} por mês</p>
                </span>
                <a class="btVisualizar" href="{{ route('project.show', $s->project->slug) }}">Visualizar</a>
                <a class="btCanelar" name="modal" href="#modalcancelar{{$s->id}}">Cancelar</a>
               <!--  <a class="btFormaP" href="#">Alterar forma de pagamento</a> -->
                 <div id="modalcancelar{{$s->id}}" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 window modalcancelar">
                    <a href="#" class="close"><img src="{{ asset('img/xfechar.jpg') }}"></a><br />
                    {{ Form::open(['route'=>array('subscription.cancel', $s->id)]) }}
                        <p>Você tem certeza que deseja deixar de patrocinar esse projeto?</p>
                        <input type="submit" value="Sim" />
                        <a href="javaScript:;" class="fechar close">Não</a>
                    {{ Form::close() }}
                </div>
            </li>
            @endforeach        
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12"></div>
</div>
@endif