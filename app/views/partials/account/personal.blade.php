<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-lg-6">
    <div class="row">
        <div class="col-md-12">
            <h3>Informações da Conta</h3>
            <div class="dadosConta">
                {{ Form::open(['route'=>'user.update.userdata']) }}
                    <div>
                        <label>Nome</label>
                        {{ Form::text('name', $user->name, ["placeholder"=>"Nome", 'required']) }}
                        <label>E-mail</label>
                        {{ Form::email('email', $user->email, ["placeholder"=>"E-mail", 'required']) }}
                        <label>CPF</label>
                       
                        {{ Form::text('cpf', $user->userdata->cpf, ["placeholder"=>"000.000.000-00", 'required', 'id'=>'cpf']) }}
                        
                        <label>Endereço</label>
                        {{ Form::text('address', isset($user->userdata->address) ? $user->userdata->address : null, ["placeholder"=>"Rua xxx, 292 - Apartamento 201", 'required']) }}
                        <label>Estado</label>
                        {{ Form::select('state_id', [''=>'Selecione Estado']+$states, isset($user->userdata->city->state->id) ? $user->userdata->city->state->id : null) }}

                        <label>Cidade</label>
                        {{ Form::select('city_id', [''=>'Selecione Cidade']+$cities, isset($user->userdata->city->id) ? $user->userdata->city->id : null) }}
                    </div>
                    <input class="btEnviar" type="submit" value="Salvar" required />
                {{ Form::close() }}
            </div> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
             <h3>Nova senha?</h3>
            <div class="dadosConta">
                {{ Form::open(['route'=>'user.update.password']) }}
                    <div>
                        <label for="password">Senha</label>
                        {{ Form::password('password') }}
                        <label for="password">Confirmar senha</label>
                        {{ Form::password('password_confirmation') }}
                    </div>
                    {{ Form::submit('Alterar',['class'=>'btEnviar']) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
   
</div>
