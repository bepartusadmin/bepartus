{{-- Inluindo metas novas --}}

<?php 
    $stores_objectives = count($objectives);
    $begins_on = $stores_objectives;
    $total = is_null(Input::old('value')) ? ($stores_objectives == 0 ? 1 : 0) : count(Input::old('value')); 
?>
@for ($i = $begins_on; $i < $total; $i++)
<div class="new_add">
    <a class="btn btn-danger {{ ($i == $begins_on && $stores_objectives == 0) ? 'deleteAddedDinamic' : 'deleteAdded' }}" href="javaScript:;"><i class="fa fa-trash"></i>Excluir</a>
    
    <label for="name">Meta</label>
    {{ Form::text('value['.$i.']', null, ['placeholder'=>'R$ Valor','class'=>'inptMeta valor moneyInp '.($errors->has('value.'.$i) ? 'objective_field_error' : '')]) }}
    <span>Valor que você quer alcançar para oferecer o que você descreve nesta meta.</span><br>
    
    <label for="nomemeta">Nome da Meta</label>
    {{ Form::text('name['.$i.']', null, ['placeholder'=>'Nome da Meta/Pequeno Título até 37 caracteres', 'class' => ($errors->has('name.'.$i) ? 'objective_field_error' : '')]) }}<br>
   
    <label for="resultado">Resultado <img title="Diga o que irá mudar no seu conteúdo caso você atinja este valor de meta." src="{{ asset('img/duvidab.jpg') }}" /></label><!-- <input required type="text" name="resultado" placeholder="Descrição da meta 1 até 600 caracteres" /> -->
    {{ Form::textarea('result['.$i.']', null, ['placeholder'=>'Descrição da meta 1 até 600 caracteres','ckeditor', 'class' => ($errors->has('result.'.$i) ? 'objective_field_error' : '')]) }}
</div>
@endfor
