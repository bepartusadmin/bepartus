<div class="alert alert-1 alert-success">{{ Session::has('success') ? '<i class="icon icon-arrows-circle-check"></i>' . Session::get('success') : null }}</div>
<div class="alert alert-1 alert-warning">{{ Session::has('warning') ? '<i class="icon icon-arrows-circle-check"></i>' . Session::get('warning') : null }}</div>
<div class="alert alert-2 alert-danger">{{ Session::has('error') ? '</i><i class="icon icon-arrows-exclamation"></i>' . Session::get('error') : null }}</div>
 <div class="alert alert-3 alert-danger">@foreach($errors->all() as $k => $error) <p>{{ $error }}</p> @endforeach</div>
       
    
