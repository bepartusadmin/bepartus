    <header>
  	<div class="container">
        <h1 class="col-xs-12 col-sm-2 col-md-2 col-lg-2 logo1"><a href="{{ route('home') }}"><img src="{{ asset('img/bepartus.jpg') }}" alt="bepatur" class="img-responsive"></a></h1>

        <nav class="col-xs-12 col-sm-10 col-md-10 col-lg-10 menudesktop">
        	<ul class="fl">
          	  <li><a href="{{ route('home') }}">Home</a></li>
              <li><a href="{{ route('projects.index') }}">Projetos</a></li>
              <li>
                <div class="search">
                      {{ Form::open(['route'=>'main.search', 'method'=>'get']) }}
                        <div class="alvo act">
                            <img src="{{ asset('img/busca.jpg') }}" alt="busca" />
                            <input type="text" name="s" value="{{ Input::get('s') }}" placeholder="O que está Procurando?" />
                        </div>
                    {{ Form::close() }}
                </div>                  
              </li>
          </ul>
          <ul class="fr">
              <!--<li class="end"><a href="http://bepartus.com/blog">Blog</a></li>-->
              @if(Sentry::check())
                <li><a href="{{ route('user.account.sponsor') }}">Olá, {{ $user->first_name }}</a></li>
                <li><a href="{{ route('user.logout') }}">Sair</a></li>
              @else
                <li><a href="{{ route('auth.register') }}" class="buttonl">Cadastrar</a></li>
                <li><a href="{{ route('faq') }}">Perguntas Frequentes</a></li>
                <li><a href="{{ route('auth.login') }}">Login</a></li>
              @endif
          </ul>
        </nav>
        
        <nav class="navbar navbar-default navmob" role="navigation">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" 
               data-target="#example-navbar-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
            </button>
            <h2 class="logo2"><img src="{{ asset('img/bepartus2.jpg') }}" alt="bepatur" class="img-responsive"></h2>
         </div>
         <div class="collapse navbar-collapse" id="example-navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="end"><a href="{{ route('home') }}">Home</a></li>
                <li class="end"><a href="{{ route('projects.index') }}">Projetos</a></li>
                <li class="end"><a href="{{ route('about') }}">Como Funciona</a></li>
                <!--<li class="end"><a href="http://bepartus.com/blog">Blog</a></li>-->
                @if(Sentry::check())
                  <li>Olá, {{ Sentry::getUser()->name }}. <a href="{{ route('user.logout') }}">Sair</a></li>
                @else
                  <li class="end"><a href="{{ route('auth.register') }}">Cadastrar</a></li>
                  <li class="end"><a href="{{ route('auth.login') }}">Login</a></li>
                @endif
            </ul>
         </div>
        </nav>
        
    </div>
    <!-- Modal Login -->
    <div id="login" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 window">
        <a href="#" class="close"><img src="{{ asset('img/xfechar.jpg') }}"></a><br />
        <h2>Log in</h2>
         <form action="{{ route('auth.post.login') }}" method="post">
            <input type="text" value="{{ Input::old('email') }}" name="email" placeholder="E-mail" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 imgemail" />
            <input type="password" name="password" value="" placeholder="Senha" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 imgsenha" />            
            <input type="submit" value="Entrar" />
            <a class="novaSenha" href="{{ route('password.remind') }}">Esqueci minha senha</a>
            <span>Ainda não é cadastrado? <a href="{{ route('auth.register') }}">Cadastre-se</a></span>
        </form>
    </div>
    <!-- Máscara para cobrir a tela -->
  	<div id="mask"></div>
  </header>