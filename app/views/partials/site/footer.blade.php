      <footer>
        <div class="container">
            
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"></div>
            
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                <form data-modal="#newsletter_confirmation_modal" class="newsletterform" data-toggle="validator" role="form" action="{{ route('newsletter.store') }}" method="post"><input required type="email" name="email" value="{{ Input::old('email') }}" placeholder="Digite seu email" class="email"><button type="submit" class="cadastro">Cadastrar <i class="fa fa-spin fa-circle-o-notch" style="display:none"></i></button></form>
                <div id="newsletter_confirmation_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="NewsletterModal">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <h4 class="modal-title" id="mySmallModalLabel">Newsletter</h4> 
                            </div>
                            <div class="modal-body">
                                <span class="text">Cadastro efetuado com sucesso, aguarde em breve mais novidades no seu e-mail!</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"></div>

            <div class="clear"></div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <p><img src="{{ asset('img/bepartus-branco.jpg') }}" alt="Bepartus"></p>
                <p class="txt2">Todos os direitos reservados<br> Bepartus 2015</p>
            </div>
            
            
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">

                <div class="row">
                    <ul class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li><a href="{{ route('projects.index') }}">Explorar</a></li>
                        <li><a href="{{ route('projects.create') }}">Iniciar Projeto</a></li>
                        <!--<li><a href="#">Blog</a></li>-->
                    </ul>
                    
                    <ul class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <li><a href="{{ route('terms') }}">Termos de uso</a></li>
                        <li><a href="{{ route('about') }}">Como Funciona</a></li>
                        <li><a href="{{ route('faq') }}">Perguntas Frequentes</a></li>
                        <li><a href="{{ route('get.contact') }}">Contato</a></li>
                    </ul>
                    
                    <div class="col-xs-12 col-sm-3 col-sm-offset-1 col-md-3 col-md-offset-1 col-lg-3 col-lg-offset-1 redessociais">
                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"><a href="https://www.facebook.com/bepartus"><img src="{{ asset('img/facebook_icon.png') }}" alt="facebook" class="img-responsive"></a></div>
                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"><a href="http://twitter.com/bepartus"><img src="{{ asset('img/icoTwitter.png') }}" alt="twitter" class="img-responsive"></a></div>
                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"><a href="https://www.youtube.com/channel/UCMy4dJtdUYzGQ57bA1qZHwA"><img src="{{ asset('img/icoYT.png') }}" alt="youtube" class="img-responsive"></a></div>
                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"><a href="http://www.instagram.com/bepartus"><img src="{{ asset('img/instagram_icon.png') }}" alt="instagram" class="img-responsive"></a></div>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </footer>

   
    <script src="{{ asset('js/jquery-maskmoney.js') }}"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script src="{{ asset('js/jquery.maskedinput.js') }}"></script>
    

   