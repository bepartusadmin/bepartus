<h2>Etapas de criação do projeto</h2>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 imgestapas">
	
	@if(!isset($project->id))
		<a href="{{ route('projects.create') }}" class="etapa1 active"></a>
		<a class="etapa2"></a>
		<a class="etapa3"></a>
	@else  
		<a href="{{ route('projects.edit', $project->slug) }}" class="etapa1 {{ Helper::activeMenu('projects.edit') }}"></a>
		<a href="{{ route('project.objective.create', $project->slug) }}" class="etapa2 {{ Helper::activeMenu('project.objective.create') }}"></a>
		<a href="{{ route('project.reward.create', $project->slug) }}" class="etapa3 {{ Helper::activeMenu('project.reward.create') }}"></a>
	@endif
        	
</div>