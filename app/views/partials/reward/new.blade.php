{{-- Inluindo metas novas --}}

<?php 
    $stores_objectives = count($rewards);
    $begins_on = $stores_objectives;
    $total = is_null(Input::old('value')) ? ($stores_objectives == 0 ? 1 : 0) : count(Input::old('value')); 
?>
@for ($i = $begins_on; $i < $total; $i++)
<div class="new_add">
    <a class="btn btn-danger {{ ($i == $begins_on && $stores_objectives == 0) ? 'deleteAddedDinamic' : 'deleteAdded' }}" href="javaScript:;"><i class="fa fa-trash"></i>Excluir</a>
    <label for="value">Valor</label>
    {{ Form::text('value['.$i.']', null, ['placeholder'=>'R$ 2,00','class'=>'inptMeta valor moneyInp '.($errors->has('value.'.$i) ? 'objective_field_error' : '')]) }}
    <span>Valor mínimo que o fã precisa patrocinar para receber esta recompensa</span><br>
    <label for="title">Título</label>
    {{ Form::text('title['.$i.']', null, ['placeholder'=>'Título da Recompensa','class' => ($errors->has('title.'.$i) ? 'objective_field_error' : '')]) }}<br>
    <label for="descricao">Descrição</label><!-- <input type="text" name="descricao" placeholder="Descrição da Recompensa" /> -->

    {{ Form::textarea('description['.$i.']', null, ['placeholder'=>'"Descrição da Recompensa','ckeditor', 'class' => ($errors->has('description.'.$i) ? 'objective_field_error' : '')]) }}
</div> 
@endfor
