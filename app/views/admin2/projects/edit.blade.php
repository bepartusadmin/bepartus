@extends('layouts.admin2')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title">
            <h1><i class="fa fa-desktop"></i> Projetos <small></small></h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        @include('admin2.partials.notifications')
    </div>
</div>

@foreach($errors->all() as $k => $error) <p>{{ $error }}</p> @endforeach

<div class="row">
    <div class="col-md-12">
    {{ BootForm::open()->multipart()->action( route('admin.projetos.update',$project->id) )->put() }}
    {{ BootForm::bind($project) }}
    <div class="tabs-container">

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true">Projeto</a></li>
            <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false">Categorias</a></li>
            <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false">Metas</a></li>
            <li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="false">Recompensas</a></li>
            <li class=""><a data-toggle="tab" href="#tab-5" aria-expanded="false">Redes Sociais</a></li>
        </ul>
        <div class="tab-content">
            <div id="tab-1" class="tab-pane active">
                <div class="panel-body">
                    {{ BootForm::text('Nome', 'name'); }}
                    {{ BootForm::file('Imagem', 'image'); }}
                    <img src="{{ $project->getImageSrc('uploads/banner/') }}" alt="{{ $project->name }}" class="img-responsive bordered_img">

                    {{ BootForm::text('Site', 'site'); }}
                    {{ BootForm::textarea('Resumo', 'excerpt')->rows(3); }}
                    {{ BootForm::text('Vídeo de Apresentação', 'video_src')->value($project->video_link); }}
                    {{ BootForm::textarea('Descrição', 'description'); }}


                    {{ BootForm::checkbox('Destaque', 'featured'); }}
                    {{ BootForm::select('Status', 'status')->options([0 => 'Inativo', 1 => 'Ativo', 2 => 'Cancelado']); }}
                </div>
            </div>
            <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                    {{ BootForm::select('Categoria','category_id[]')->options($categories)->select(count($project_categories) > 0 ? $project_categories[0] : null) }}
                    {{ BootForm::select('Categoria','category_id[]')->options($categories)->select(count($project_categories) > 1 ? $project_categories[1] : null) }}
                    {{ BootForm::select('Categoria','category_id[]')->options($categories)->select(count($project_categories) > 2 ? $project_categories[2] : null) }}
                </div>
            </div>
            
            <div id="tab-3" class="tab-pane">
                <div class="panel-body">
                     @foreach($project->objectives as $index => $objective)
                     <div class="panel panel-default panel-meta">
                        <div class="panel-body">
                        {{ BootForm::hidden("objectives[$index][id]")->value($objective->id); }}
                        {{ BootForm::text('Valor', "objectives[$index][value]")->defaultValue(\Helper::Monetize($objective->value/100))->addClass('moneyInp'); }}
                        {{ BootForm::text('Nome', "objectives[$index][name]")->defaultValue($objective->name); }}
                        {{ BootForm::textarea('Resultado', "objectives[$index][result]")->defaultValue($objective->result)->rows(3); }}
                        {{ BootForm::checkbox('Remover',"objectives[$index][delete]"); }}
                        </div>
                    </div>
                    @endforeach
                    <div class="newMetaPlaceHolder"></div>
                    <button type="button" class="btn btn-success addObjective pull-right"><i class="fa fa-plus-circle"></i> Adicionar nova meta</button>
                </div>
            </div>
            
            <div id="tab-4" class="tab-pane">
                <div class="panel-body">
                    @foreach($project->rewards as $index => $reward)
                     <div class="panel panel-default panel-recompensa">
                        <div class="panel-body">
                        {{ BootForm::hidden("rewards[$index][id]")->value($reward->id); }}
                        {{ BootForm::text('Valor', "rewards[$index][value]")->defaultValue(\Helper::Monetize($reward->value/100))->addClass('moneyInp'); }}
                        {{ BootForm::text('Titulo', "rewards[$index][title]")->defaultValue($reward->title); }}
                        {{ BootForm::textarea('Descricao', "rewards[$index][description]")->defaultValue($reward->description)->rows(3); }}
                        {{ BootForm::checkbox('Remover',"rewards[$index][delete]"); }}
                        </div>
                    </div>
                    @endforeach
                    <div class="newRecompensaPlaceHolder"></div>
                    <button type="button" class="btn btn-success addReward pull-right"><i class="fa fa-plus-circle"></i> Adicionar nova recompensa</button>
                </div>
            </div>
            
            <div id="tab-5" class="tab-pane">
                <div class="panel-body">
                    <div class="row">
                    @foreach($networks->chunk(3) as $chunk)
                        <div class="area1 col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            @foreach($chunk as $network)
                            {{ BootForm::text($network->name, "networks[".$network->id."]")->defaultValue( (!empty($n) ? $n[$network->id] : '') )  }}
                            @endforeach
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>

    </div>
    {{ BootForm::submit('Editar Projeto')->addClass('btn btn-primary') }}
    
    {{  BootForm::close() }}
    </div>
</div>
<br>
@stop

@section('footer')
    @parent
    {{ HTML::script('admin/js/summernote/summernote.min.js') }}
    {{ HTML::script('admin/js/projects.js') }}
    {{ HTML::script('js/jquery-maskmoney.js') }}
@stop

@section('header')
    @parent
    {{ HTML::style('admin/css/summernote-bs3.css') }}
@stop