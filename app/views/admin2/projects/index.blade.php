@extends('layouts.admin2')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title">
            <h1><i class="fa fa-desktop"></i> Projetos <small></small></h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        @include('admin2.partials.notifications')
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            {{ Form::open(['route'=>'admin.projetos.index', 'method'=>'get', 'class'=>'form-inline']) }}
            {{ Form::text('n', Input::get('n'), ['class'=>'form-control', 'placeholder'=>'Digite nome do projeto']) }}
            {{ Form::button('Buscar <i class="fa fa-search"></i>', ['class'=>'btn btn-info', 'type'=>'submit']) }}
            {{ Form::close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">	
        @if($projects->count() > 0)
        <div class="table-responsive">
        <table class="table table-striped table-condensed table-bordered">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Produtor</th>
                    <th>Data de criação</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
            @foreach($projects as $project)
            <tr>
                <td>{{ $project->name }}</td>
		<td>{{ $project->user->name }}</td>
		<td>{{ Helper::ConverterBR($project->created_at) }} às {{ Helper::Hora($project->created_at) }}</td>
                <td>
                    @if($project->status == 0) <span class="label label-warning"><i class="fa fa-hourglass-1"></i> Inativo</span> @endif
                    @if($project->status == 1) <span class="label label-success"><i class="fa fa-check"></i> Ativo</span> @endif
                    @if($project->status == 2) <span class="label label-danger"><i class="fa fa-remove"></i> Cancelado</span> @endif
                </td>
                <td>
                    <a class="pull-left btn btn-default btn-xs" title="Editar" href="{{ route('admin.projetos.edit', $project->id) }}"><i class="fa fa-pencil"></i></a> 
                    {{ Form::open(['route'=>array('admin.projetos.destroy', 'id'=>$project->id), 'method'=>'delete', 'onsubmit' => "return confirm('Deseja remover esse projeto?')"]) }}
                    <button class="btn btn-default btn-xs pull-left" type="submit" title="Deletar"><i class="fa fa-trash"></i></button>			
                    {{ Form::close() }}
		</td>
            </tr>
            @endforeach
            </tbody>
	</table>	
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12"><div class="dataTables_info">Exibindo de {{ $projects->getFrom() }} até {{ $projects->getTo() }} de {{ $projects->getTotal() }} registros</div></div>
            <div class="col-md-8 col-xs-12"><div class="dataTables_paginate paging_bootstrap">{{ $projects->appends($_GET)->links() }}</div></div>
        </div>
        @else
        <div class="alert alert-warning" style="margin-top: 25px;">
            <p><i class="fa fa-exclamation-circle"></i> Ainda não temos conteúdo para esta área</p>
        </div>
        @endif
    </div>
</div>

@stop