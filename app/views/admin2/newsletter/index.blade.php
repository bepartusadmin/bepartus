@extends('layouts.admin2')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title">
            <h1><i class="fa fa-envelope"></i> Newsletter <small></small></h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        @include('admin2.partials.notifications')
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            {{ Form::open(['route'=>'admin.newsletters.download']) }}
            {{ Form::button('Download <i class="fa fa-files-o"></i>', ['class'=>'btn btn-success', 'type'=>'submit']) }}
            {{ Form::close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">	
        @if($news->count() > 0)
        <div class="table-responsive">
        <table class="table table-striped table-condensed table-bordered">
            <thead>
                <tr>
                    <th>Email</th>
                    <th>Cadastro em</th>
		</tr>
            </thead>
            <tbody>
            @foreach($news as $n)
            <tr>
                <td>{{ $n->email }}</td>
                <td><small>{{ Helper::ConverterBR($n->created_at) }} {{ Helper::Hora($n->created_at) }}</small></td>
            </tr>
            @endforeach
            </tbody>
	</table>	
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12"><div class="dataTables_info">Exibindo de {{ $news->getFrom() }} até {{ $news->getTo() }} de {{ $news->getTotal() }} registros</div></div>
            <div class="col-md-8 col-xs-12"><div class="dataTables_paginate paging_bootstrap">{{ $news->appends($_GET)->links() }}</div></div>
        </div>
        @else
        <div class="alert alert-warning" style="margin-top: 25px;">
            <p><i class="fa fa-exclamation-circle"></i> Ainda não temos conteúdo para esta área</p>
        </div>
        @endif
    </div>
</div>

@stop