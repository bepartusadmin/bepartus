@if(Session::has('success'))
    <div class="alert alert-success">
        <i class="icon icon-arrows-circle-check"></i> {{ Session::get('success')  }}
    </div> 
@endif

@if(Session::has('warning'))
    <div class="alert alert-warning">
        <i class="icon icon-arrows-circle-check"></i> {{ Session::get('warning')  }}
    </div> 
@endif

@if(Session::has('error'))
    <div class="alert alert-danger">
        <i class="fa fa-exclamation-circle"></i> {{ Session::get('error')  }}
    </div> 
@endif
