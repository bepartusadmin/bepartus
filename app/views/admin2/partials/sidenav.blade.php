<nav class="navbar-aside navbar-static-side" role="navigation">
    <div class="sidebar-collapse nano">
        <div class="nano-content">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown side-profile text-left"> 
                        <span style="display: block;">
                            <img src="{{ Helper::getUserThumbnail(Sentry::getUser()->id) }}" class="img-circle" width="40">
                        </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear" style="display: block;">
                                <span class="block m-t-xs">
                                    <strong class="font-bold">{{ Sentry::getUser()->name }}  
                                        <b class="caret"></b></strong>
                                </span>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="{{ route('admin.usuarios.edit', Sentry::getUser()->id) }}"><i class="fa fa-user"></i>Meu Perfil</a></li>
                            <li class="divider"></li>
                            <li><a href="{{route('admin.logout')}}"><i class="fa fa-key"></i>Sair</a></li>
                        </ul>
                    </div>
                </li>
                <li class="{{ Helper::activeMenu('admin.dashboard.index') }}">
                    <a href="{{ route('admin.dashboard.index') }}"><i class="fa fa-th-large"></i>Dashboard</a>
                </li>
                <li class="{{ Helper::activeMenu('admin.projetos.index') }}">
                    <a href="{{ route('admin.projetos.index') }}"><i class="fa fa-laptop"></i>Projetos</a>
                </li>
                <li class="{{ Helper::activeMenu('admin.usuarios.index') }}">
                    <a href="{{ route('admin.usuarios.index') }}"><i class="fa fa-users"></i>Usuários</a>
                </li>
                <li class="{{ Helper::activeMenu('admin.newsletters.index') }}">
                    <a href="{{ route('admin.newsletters.index') }}"><i class="fa fa-envelope"></i> Newsletters</a>
                </li>
                <li class="{{ Helper::activeMenu('admin.patrocinios.index') }}">
                    <a href="{{ route('admin.patrocinios.index') }}"><i class="fa fa-money"></i> Patrocínios</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
