@extends('layouts.login2')
@section('content')

<div class="container">
    <div class="row">
        <div class="account-col text-center">
            <img style="margin: 0 auto 30px;display: block;" src="{{ asset('img/bepartus.png') }}" alt="Administração">
            
            @include('admin2.partials.notifications')
            
            {{ Form::open(['route'=>'admin.post.login', 'class' => 'm-t']) }}
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    {{ Form::email('email', null, ['placeholder'=>'E-mail', 'class'=>'form-control']) }}
                    {{ $errors->first('email', '<p class="help-block">:message</p>') }} 
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    {{ Form::password('password', ['placeholder'=>'Senha', 'class'=>'form-control']) }}
                    {{ $errors->first('password', '<p class="help-block">:message</p>') }} 
                </div>
                <div class="checkbox">
                    <label><input name="remember-me" type="checkbox" value="Remember Me">Me lembrar</label>
                </div>
                {{ Form::submit('Login', ['class'=>'btn btn-primary btn-block']) }}
                <a href="{{ route('password.remind') }}"><small>Esqueci minha senha</small></a>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
