@extends('layouts.admin2')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title">
            <h1><i class="fa fa-money"></i> Patrocínios <small></small></h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        @include('admin2.partials.notifications')
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            {{ Form::open(['route'=>'admin.patrocinios.index', 'method'=>'get', 'class'=>'form-inline']) }}
            {{ Form::text('project', Input::get('project'), ['class'=>'form-control', 'placeholder'=>'Digite nome do projeto']) }}
            {{ Form::button('Buscar <i class="fa fa-search"></i>', ['class'=>'btn btn-info', 'type'=>'submit']) }}
            {{ Form::close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">	
        @if($subscriptions->count() > 0)
        <div class="table-responsive">
        <table class="table table-striped table-condensed table-bordered">
            <thead>
                <tr>
                    <th>Projeto</th>
                    <th>Usuário</th>
                    <th>Status</th>
                    <th>Tipo</th>
                    <th>Valor</th>
                    <th>Criado em</th>
                    <th>Ações</th>
		</tr>
            </thead>
            <tbody>
            @foreach($subscriptions as $s)
            <tr>
                <td>{{ $s->project->name }}</td>
                <td>{{ $s->user->name}}<br><small>{{ $s->user->email}}</small></td>
                <td>{{ $s->status == '2' ? 'Cancelado' : ($s->status == '0' ? 'Pendente' : 'Pago' ) }}</td>
                <td>{{ $s->type == '2' ? 'Boleto' : 'Cartão de Crédito' }}</td>
                <td>R${{ Helper::Monetize($s->amount/100) }}</td>
                <td>{{ $s->created_at }}</td>
                <td>
                    {{ Form::open(['route'=>array('admin.patrocinios.destroy', 'id'=>$s->id), 'method'=>'delete', 'onsubmit' => "return confirm('Deseja cancelar esse patrocinio?')"]) }}
                    <button class="btn btn-danger btn-xs pull-left" type="submit" title="Cancelar Patrocínio"><i class="fa fa-ban"></i></button>			
                    {{ Form::close() }}
                </td>
            </tr>
            @endforeach
            </tbody>
	</table>	
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12"><div class="dataTables_info">Exibindo de {{ $subscriptions->getFrom() }} até {{ $subscriptions->getTo() }} de {{ $subscriptions->getTotal() }} registros</div></div>
            <div class="col-md-8 col-xs-12"><div class="dataTables_paginate paging_bootstrap">{{ $subscriptions->appends($_GET)->links() }}</div></div>
        </div>
        @else
        <div class="alert alert-warning" style="margin-top: 25px;">
            <p><i class="fa fa-exclamation-circle"></i> Ainda não temos conteúdo para esta área</p>
        </div>
        @endif
    </div>
</div>

@stop