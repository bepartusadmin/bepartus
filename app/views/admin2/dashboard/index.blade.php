@extends('layouts.admin2')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title">
            <div class="pull-left">
                <h1>Dashboard</h1>
            </div>
            
            <div class="pull-right">
                <form class="form-inline">
                    <input type="date" name="from" class="form-control" value="{{ $from }}">
                    <input type="date" name="to" class="form-control" value="{{ $to }}">
                    <button type="submit" class="btn btn-default">Filtrar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div style="margin-bottom: 25px;"></div>

<div class="row">
    <div class="col-md-12">
        @include('admin2.partials.notifications')
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <div class="widget-box clearfix">
            <div class="pull-left">
                <h4>Novos Usuários</h4>
                <h2>{{ $users['count'] }}</h2>
            </div>
            <div class="text-right">
                <span id="sparkline8"></span>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="row">
            <div class="col-md-4">
                <div class="widget-box clearfix">
                    <div>
                        <h4>Novos patrocinios</h4>
                        <h2>{{ $subscriptions['count'] }} <i class="fa fa-plus pull-right"></i></h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="widget-box clearfix">
                    <div>
                        <h4>Total de vendas</h4>
                        <h2>R${{ Helper::Monetize($subscriptions['sum']/100) }} <i class="fa fa-shopping-cart pull-right"></i></h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="widget-box clearfix">
                    <div>
                        <h4>Assinaturas Pendentes</h4>
                        <h2>{{ $subscriptions['pending'] }} <i class="fa fa-tasks pull-right"></i></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-card recent-activites">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title">Divisão de Assinaturas por Projeto</h4>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th width="10">%</th>
                            <th width="10"></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $total_projects = count($subscriptions['per_project']) ?>
                            @foreach($subscriptions['per_project'] as $s)
                            <tr>
                                <td>{{ $s->project_name }}</td>
                                <td><div class="sparkline" data-total="{{ $total_projects }}" data-share="{{ $s->count }}"></div></td>
                                <td class="text-center"><a class="pull-left btn btn-default btn-xs" title="Editar" href="{{ route('admin.projetos.edit', $s->project_id) }}"><i class="fa fa-pencil"></i></a> </td>
                            </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        </div><!-- End .panel --> 
    </div>
    <div class="col-sm-6">
        <div class="panel panel-card recent-activites">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title">Meios de Pagamento</h4>
            </div>
            <div class="panel-body">
                <div>
                    <canvas id="doughnutChart" height="140"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>



@stop
@section('footer')
    @parent
    {{ HTML::script('admin/js/jquery.sparkline.min.js') }}
    {{ HTML::script('admin/js/morris_chart/raphael-2.1.0.min.js') }}
    {{ HTML::script('admin/js/morris_chart/morris.js') }}
    {{ HTML::script('admin/js/chartjs/Chart.min.js') }}
    <script>
        $("#sparkline8").sparkline([{{ join(',',array_pluck($users['progression'],'user_count')) }}], {
            type: 'bar',
            barWidth: 4,
            height: '40px',
            barColor: '#01a8fe',
            negBarColor: '#c6c6c6'
        });
        
        $(".sparkline").each(function(){
            var el = $(this);
            el.sparkline([el.data('share'),el.data('total')], {
                type: 'pie',
                sliceColors: ['#01a8fe', '#ddd']
            });
        });
        
    var doughnutOptions = {
        segmentShowStroke: true,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        percentageInnerCutout: 45, // This is 0 for Pie charts
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false,
        responsive: true
    };
        
        var ctx = document.getElementById("doughnutChart").getContext("2d");
        var myNewChart = new Chart(ctx).Doughnut({{json_encode($subscriptions['type']) }}, doughnutOptions);
    </script>
@stop