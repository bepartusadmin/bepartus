@extends('layouts.admin2')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title">
            <h1><i class="fa fa-users"></i> Usuários <small></small></h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        @include('admin2.partials.notifications')
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            {{ Form::open(['route'=>'admin.usuarios.index', 'method'=>'get', 'class'=>'form-inline']) }}
            {{ Form::text('n', Input::get('n'), ['class'=>'form-control', 'placeholder'=>'Digite nome do usuário']) }}
            {{ Form::button('Buscar <i class="fa fa-search"></i>', ['class'=>'btn btn-info', 'type'=>'submit']) }}
            {{ Form::close() }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">	
        @if($users->count() > 0)
        <div class="table-responsive">
        <table class="table table-striped table-condensed table-bordered">
            <thead>
                <tr>
                    <th>Thumb</th>
                    <th>Dados Pessoais</th>
                    <th>Cliente</th>
                    <th>Produtor</th>
                    <th>Cadastro em</th>
                    <th>Último login</th>
                    <th>Admin</th>
                    <th style="width:80px">Ações</th>
		</tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->getImageSrc('113') }}</td>
		<td>
                    <p>
                        <small>
                        <strong>Nome:</strong>{{ $user->name }}<br>
                        <strong>Email:</strong>{{ $user->email }}<br>
                        <strong>CPF:</strong>{{ $user->userdata->cpf }}<br>
                        <strong>Tels:</strong>{{ $user->userdata->tel or '--' }}<br>
                        <strong>Endereço:</strong>{{ $user->userdata->address or null }} {{ $user->userdata->city->name or null }} - {{ $user->userdata->city->state->uf or null }}
                        </small>
                    </p>
		</td>
		<td>
                    @if(isset($user->customer->id))
                        <a  data-toggle="modal" data-target="#customer_{{ $user->customer->id }}" href=""><i class="fa fa-user"></i></a>
			@include('admin.users.modal.customer')
                    @else
                        N/A
                    @endif
		</td>
		<td>
                    @if(isset($user->producer->id))
                        <a data-target="#producer_{{ $user->producer->id }}" data-toggle="modal" href=""><i class="fa fa-briefcase"></i></a>
                        @include('admin.users.modal.producer')
                    @else
                        N/A
                    @endif
		</td>
                <td><small>{{ Helper::ConverterBR($user->created_at) }} {{ Helper::Hora($user->created_at) }}</small></td>
		<td><small>{{ Helper::ConverterBR($user->last_login) }} {{ Helper::Hora($user->last_login) }}</small></td>
		<td>
                    @if(Sentry::findUserById($user->id)->inGroup($admin_group))
                        <i class="fa fa-check"></i>
                    @else
                        <i class="fa fa-times"></i>
                    @endif
                </td>
                <td>
                    <a class="pull-left btn btn-default btn-xs" title="Editar" href="{{ route('admin.usuarios.edit', $user->id) }}"><i class="fa fa-pencil"></i></a> 
                    {{ Form::open(['route'=>array('admin.user.deactivate',$user->id)]) }}
                    @if($user->activated == '1')
			{{ Form::button('<i class="fa fa-ban"></i>', ['class'=>'btn btn-danger btn-xs pull-left', 'type'=>'submit', 'title' => 'Desativar']) }}
                    @else
                        {{ Form::button('<i class="fa fa-check"></i>', ['class'=>'btn btn-success btn-xs pull-left', 'type'=>'submit', 'title' => 'Ativar']) }}
                    @endif
                    {{ Form::close() }}
                    
		</td>
            </tr>
            @endforeach
            </tbody>
	</table>	
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12"><div class="dataTables_info">Exibindo de {{ $users->getFrom() }} até {{ $users->getTo() }} de {{ $users->getTotal() }} registros</div></div>
            <div class="col-md-8 col-xs-12"><div class="dataTables_paginate paging_bootstrap">{{ $users->appends($_GET)->links() }}</div></div>
        </div>
        @else
        <div class="alert alert-warning" style="margin-top: 25px;">
            <p><i class="fa fa-exclamation-circle"></i> Ainda não temos conteúdo para esta área</p>
        </div>
        @endif
    </div>
</div>

@stop