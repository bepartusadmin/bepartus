@extends('layouts.admin2')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title">
            <h1><i class="fa fa-users"></i> Usuário <small></small></h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        @include('admin2.partials.notifications')
    </div>
</div>

@foreach($errors->all() as $k => $error) <p>{{ $error }}</p> @endforeach

<div class="row">
    <div class="col-md-12">
    {{ BootForm::open()->multipart()->action( route('admin.usuarios.update',$user->id) )->put() }}
    {{ BootForm::bind($user) }}
    
    <div class="row">
        <div class="col-md-1">
            <img src="{{ $user->userdata->image != '' ? asset('uploads/normal/'.$user->userdata->image) : 'http://placehold.it/307X307' }}" alt="{{ $user->name }}" class="img-responsive img-circle">
        </div>
        <div class="col-md-11">
            {{ BootForm::file('Imagem', 'userdata[image]'); }}
        </div>
    </div>
    
    <hr>
    
    {{ BootForm::text('Nome', 'name'); }}
    {{ BootForm::text('E-mail', 'email'); }}
    {{ BootForm::text('CPF', 'userdata[cpf]'); }}
    {{ BootForm::text('Telefone', 'userdata[tel]'); }}
    {{ BootForm::text('Endereço', 'userdata[address]'); }}
    
    {{ BootForm::select('Estado','state')->options($states)->select( $user->userdata->city_id ? $user->userdata->city->state->id : '') }}
    {{ BootForm::select('Cidade', 'userdata[city_id]')->options($cities)->select($user->userdata->city_id); }}
    
    <hr>
    
    {{ BootForm::password('Senha', 'password'); }}
    {{ BootForm::password('Confirmação de Senha', 'password_confirmation'); }}
    
    
    {{ BootForm::submit('Editar Usuário')->addClass('btn btn-primary') }}
    
    {{  BootForm::close() }}
    </div>
</div>
<br>
@stop

@section('footer')
    @parent

    <script>
    $(document).ready(function() {          
        $('select[name="state"]').change(function(){
            var state = $(this).val();
        
            $.ajax({
                url:'/city/search',
                type:'post',
                data:{state:state}
            }).done(function(data){
            
                $('select[name*="city_id"]').html(data.response);
            });
        });  
    });  
    </script>
@stop