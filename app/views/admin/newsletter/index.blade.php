@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-md-10">
        <h1 class="page-header">
            Newsletters <small>({{ $news->getTotal() }})</small>
        </h1>
    </div>
    <div class="col-md-2">
    	{{ Form::open(['route'=>'admin.newsletters.download']) }}
    	{{ Form::button('Download <i class="fa fa-files-o"></i>', ['class'=>'btn btn-success', 'type'=>'submit']) }}
    	{{ Form::close() }}
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	 @include('partials.site.notifications')
	@if($news->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Email</th>
			</tr>
			@foreach($news as $n)
				<tr>
				<td>{{ $n->email }}</td>
				

				</tr>
			@endforeach
		</table>
		{{ $news->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop