@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Projeto <small>({{ $project->name }})</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="row">
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-12">
				<h2>Produtor</h2>
				<p><strong>Nome: </strong>{{ $project->user->name }}</p>
				<p><strong>Email: </strong>{{ $project->user->email }}</p>
				<p><strong>Localização: </strong>{{ $project->user->userdata->city->name or null }} - {{ $project->user->userdata->city->state->uf or null }}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2>Informações</h2>
				<p><strong>Descrição: </strong>{{ $project->description }}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2>Metas</h2>
				@foreach($project->objectives as $k => $objective)
					<p>{{ $k+1 }}. {{ $objective->name }}</p>
				@endforeach
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2>Recompensas</h2>
				@foreach($project->rewards as $k => $reward)
					<p>{{ $k+1 }}. {{ $reward->title }}</p>
				@endforeach
			</div>
		</div>
		
	</div>
	<div class="col-md-4">
		<img src="{{ $project->getImageSrc() }}" alt="">
	</div>
</div>

@stop