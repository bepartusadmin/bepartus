@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Projetos <small>({{ $projects->getTotal() }})</small>
        </h1>
    </div>
</div>
<div class="row">
	<div class="col-md-12">
		{{ Form::open(['route'=>'admin.projetos.index', 'method'=>'get', 'class'=>'form-inline']) }}
		{{ Form::text('n', Input::get('n'), ['class'=>'form-control', 'placeholder'=>'Digite nome do projeto']) }}
		{{ Form::button('Buscar <i class="fa fa-search"></i>', ['class'=>'btn btn-info', 'type'=>'submit']) }}
		{{ Form::close() }}
	</div>
</div>
<!-- /.row -->
<div class="col-md-12">
	
	 @include('partials.site.notifications')
	@if($projects->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Nome</th>
				<th>Produtor</th>
				<th>Data de criação</th>
				<th>Visualizar</th>
				<th>Aprovar</th>
				<!-- <th>Apagar</th> -->
				
			</tr>
			@foreach($projects as $project)
				<tr>
					
					<td>{{ $project->name }}</td>
					<td>{{ $project->user->name }}</td>
					<td>{{ Helper::ConverterBR($project->created_at) }} às {{ Helper::Hora($project->created_at) }}</td>
				
					
					<td><a class="btn btn-primary" href="{{ route('admin.projetos.show', ['id'=>$project->id]) }}"><i class="fa fa-search"></i></a></td>
					<td>
						{{ Form::open(['route'=>array('admin.projetos.aprovar',$project->id), 'class'=>'form-inline']) }}
						{{ Form::checkbox('status', '1', in_array('1', [$project->status]), ['class'=>'status_in','data-id'=>$project->id]) }}
						{{ Form::hidden('pid', $project->id) }}
						<a style="{{ $project->status == '0' ? 'display: inline;' : 'display: none' }}" id="modalLinkComment_{{ $project->id }}" data-toggle="modal" data-target="#modalComment_{{ $project->id }}" class="btn {{ $project->approval_comment != '' ? 'btn-success' : 'btn-info' }} comment_modal_link"><i class="fa fa-comment"></i></a>
						<span style="display: none;" class="loader_admin"><svg width='40px' height='40px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-ripple"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><g> <animate attributeName="opacity" dur="2s" repeatCount="indefinite" begin="0s" keyTimes="0;0.33;1" values="1;1;0"></animate><circle cx="50" cy="50" r="40" stroke="#ed4700" fill="none" stroke-width="6" stroke-linecap="round"><animate attributeName="r" dur="2s" repeatCount="indefinite" begin="0s" keyTimes="0;0.33;1" values="0;22;44"></animate></circle></g><g><animate attributeName="opacity" dur="2s" repeatCount="indefinite" begin="1s" keyTimes="0;0.33;1" values="1;1;0"></animate><circle cx="50" cy="50" r="40" stroke="#bfbfbf" fill="none" stroke-width="6" stroke-linecap="round"><animate attributeName="r" dur="2s" repeatCount="indefinite" begin="1s" keyTimes="0;0.33;1" values="0;22;44"></animate></circle></g></svg></span>
						{{ Form::close() }}
						
						<!-- Modal -->
						<div id="modalComment_{{ $project->id }}" class="modal fade" role="dialog">
						  <div class="modal-dialog">

						    <!-- Modal content-->
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal">&times;</button>
						        <h4 class="modal-title">Comente sobre ão aprovação deste projeto</h4>
						        <p>{{ $project->name }}</p>
						      </div>
						      <div class="modal-body">
						        {{ Form::open(['route'=>array('admin.projetos.justifica', $project->id)]) }}
						        <div class="form-group">
						        	{{ Form::textarea('approval_comment', $project->approval_comment, ['class'=>'form-control']) }}

						        </div>
						        <div class="forr-group">
						        	{{ Form::button('Enviar <i class="fa fa-arrow-right"></i>', ['class'=>'btn btn-primary', 'type'=>'submit']) }}
						        </div>
						        {{ Form::close() }}
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">fechar</button>
						      </div>
						    </div>

						  </div>
						</div>

					</td>
					<!-- <td>
						{{ Form::open(['route'=>array('admin.projetos.destroy', 'id'=>$project->id), 'method'=>'delete']) }}
						<button class="btn btn-danger" type="submit">
								                <i class="fa fa-trash"></i>
								            </button>
						
						{{ Form::close() }}
					</td> -->
				</tr>
			@endforeach
		</table>
		{{ $projects->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop