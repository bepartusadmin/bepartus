@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Usuários <small>{{ $users->getTotal() }}</small>
        </h1>
    </div>
</div>
<div class="row">
	<div class="col-md-12">
		{{ Form::open(['route'=>'admin.usuarios.index', 'method'=>'get', 'class'=>'form-inline']) }}
		{{ Form::text('n', Input::get('n'), ['class'=>'form-control', 'placeholder'=>'Digite nome do usuário']) }}
		{{ Form::button('Buscar <i class="fa fa-search"></i>', ['class'=>'btn btn-info', 'type'=>'submit']) }}
		{{ Form::close() }}
	</div>
</div>
<!-- /.row -->
<div class="col-md-12">
	 @include('partials.site.notifications')
	@if($users->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Thumb</th>
				<th>Dados Pessoais</th>
				<th>Cliente</th>
				<th>Produtor</th>
				<th>Cadastro em</th>
				<th>Último login</th>
				<th>Ações</th>
			</tr>
			@foreach($users as $user)
				<tr>
				<td>{{ $user->getImageSrc('113') }}</td>
					<td>
						<p><strong>Nome:</strong>{{ $user->name }}</p>
						<p><strong>Email:</strong>{{ $user->email }}</p>
						<p><strong>CPF:</strong>{{ $user->userdata->cpf }}</p>
						<p><strong>Tels:</strong>{{ $user->userdata->tel or '--' }}</p>
						<p><strong>Endereço:</strong>{{ $user->userdata->address or null }} {{ $user->userdata->city->name or null }} - {{ $user->userdata->city->state->uf or null }}</p>
					</td>
					<td>
						@if(isset($user->customer->id))
							<a  data-toggle="modal" data-target="#customer_{{ $user->customer->id }}" href=""><i class="fa fa-user"></i></a>

							@include('admin.users.modal.customer')

						@else
							N/A
						@endif
					</td>
					<td>
						@if(isset($user->producer->id))
							<a data-target="#producer_{{ $user->producer->id }}" data-toggle="modal" href=""><i class="fa fa-briefcase"></i></a>

							@include('admin.users.modal.producer')
						@else
							N/A
						@endif
					</td>
					<td>{{ Helper::ConverterBR($user->created_at) }} {{ Helper::Hora($user->created_at) }}</td>
					<td>{{ Helper::ConverterBR($user->last_login) }} {{ Helper::Hora($user->last_login) }}</td>
					<td>
						{{ Form::open(['route'=>array('admin.user.deactivate',$user->id)]) }}
						@if($user->activated == '1')
							{{ Form::button('Desativar', ['class'=>'btn btn-danger', 'type'=>'submit']) }}
						@else
							{{ Form::button('Ativar', ['class'=>'btn btn-info', 'type'=>'submit']) }}
						@endif
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</table>
		{{ $users->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop