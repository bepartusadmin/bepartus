<!-- Modal -->
<div id="producer_{{ $user->producer->id }}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Produtor {{ $user->name }}</h4>
      </div>
      <div class="modal-body">
        <h4>Projetos</h4>
        <table class="table table-bordered">
        	<tr>
        		<th>Nome</th>
        		<td>Resumo</td>
        		<th>URL</th>
        	</tr>
        	@foreach($user->projects as $p)
        	<tr>
        		<td>{{ $p->name }}</td>
        		<td>{{ $p->excerpt }}</td>
        		<td>{{ $p->status == '1' ? '<a href="'. route('project.show', $p->slug) .'" target="_blank"><i class="fa fa-share-square"></i></a>':'--'}}</td>
        	</tr>
        	@endforeach
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">fechar</button>
      </div>
    </div>

  </div>
</div>