<!-- Modal -->
<div id="customer_{{ $user->customer->id }}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cliente</h4>
      </div>
      <div class="modal-body">
        <p><strong>Tipo de pagamento cadastrado:</strong>{{ $user->customer->iugu_item_type }} </p>
        @if($user->subscriptions->count() > 0)
	        <p><strong>Assinaturas</strong></p>
	        <table class="table table-bordered">
	        	<tr>
	        		<th>Projeto</th>
	        		<th>Recompensa</th>
	        		<th>Valor pago</th>
	        		<th>Status</th>
	        	</tr>
	        	@foreach($user->subscriptions as $s)
		        	<tr>
						<td>{{ $s->project->name }}</td>
						<td>
							{{ $s->paid_for_reward == '1' ? $s->reward->title.' - R$'.Helper::Monetize($s->reward->value/100) : '--' }}
						</td>
						<td>R${{ Helper::Monetize($s->getValue()) }}</td>
						<td>{{ $s->statusText }}</td>
		        	</tr>
	        	@endforeach
	        </table> 	
        @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">fechar</button>
      </div>
    </div>

  </div>
</div>