@extends('layouts.login')
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <img style="margin: 0 auto 30px;display: block;" src="{{ asset('img/bepartus.png') }}" alt="Administração">

            <p>Administração</p>

            @include('partials.site.notifications') 

            {{ Form::open(['route'=>'admin.post.login']) }}
           
            <fieldset>
                <div class="form-group">
                    {{ Form::email('email', null, ['placeholder'=>'E-mail', 'class'=>'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::password('password', ['placeholder'=>'Senha', 'class'=>'form-control']) }}
                </div>
                <div class="checkbox">
                    <label>
                        <input name="remember-me" type="checkbox" value="Remember Me">Me lembrar
                    </label>
                    <a class="pull-right" href="{{ route('password.remind') }}">Esqueci minha senha</a>
                </div>
                {{ Form::submit('Login', ['class'=>'btn btn-lg btn-success btn-block']) }}
            </fieldset>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
