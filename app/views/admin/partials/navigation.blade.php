        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('admin.projetos.index') }}"><img height="30" src="{{asset('img/bepartus.png')}}" alt=""></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
         
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Sentry::getUser()->name }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                       
                        <li class="divider"></li>
                        <li>
                            <a href="{{route('admin.logout')}}"><i class="fa fa-fw fa-power-off"></i> Sair</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
               
                <ul class="nav navbar-nav side-nav">
                    <li {{ Route::current()->getName() == 'admin.projetos.index' ? 'class="active"' : null }}>
                        <a href="{{ route('admin.projetos.index') }}"><i class="fa fa-fw fa-file-text"></i> Projetos</a>
                    </li>
                    <li {{ Route::current()->getName() == 'admin.usuarios.index' ? 'class="active"' : null }}>
                        <a href="{{ route('admin.usuarios.index') }}"><i class="fa fa-users"></i> Usuários</a>
                    </li>
                     <li {{ Route::current()->getName() == 'admin.newsletters.index' ? 'class="active"' : null }}>
                        <a href="{{ route('admin.newsletters.index') }}"><i class="fa fa-envelope"></i> Newsletters</a>
                    </li>  
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>