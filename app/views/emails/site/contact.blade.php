<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<h2>Novo contato site Bepartus</h2>
	<body>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td><b>Nome:</b> {{ $data['name'] }}</td>
			</tr>
			<tr>
				<td><b>Email:</b> {{ $data['email'] }}</td>
			</tr>
			<tr>
				<td><b>Mensagem:</b> {{ $data['msg'] }}</td>
			</tr>
			<tr>
				<td><b>Data:</b> {{ date('d/m/Y') }} às {{ date('H:i') }}h</td>
			</tr>
		</table>
	</body>
</html>
