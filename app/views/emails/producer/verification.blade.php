<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
                            <td><p>Olá, {{ $producer->name }}</p></td>
			</tr>
                        @if ($data['status'] == 'accepted')
                        <tr>
                            <td>
                                <p>Seus dados foram aprovados pelo nosso parceiro de pagamento e agora você está pronto para postar um projeto na Bepartus!</p>
                                <p>Tem alguma dúvida sobre como começar? Dê uma olhada em nosso <a href="http://bepartus.com/perguntas-frequentes">FAQ</a> e se ainda tiver mais dúvidas não hesite em entrar em contato pelo email contato@bepartus.com, redes sociais ou pelo próprio site, ficaremos felizes em ajudá-lo!</p>
                            </td>
                        </tr>
                        @else
			<tr>
				<td>
                                    <p>Seus dados bancários foram reprovados: </p>
                                    <p><b>Motivo</b>: {{ $data['feedback'] }}
                                </td>
			</tr>
                        @endif
		</table>
	</body>
</html>
