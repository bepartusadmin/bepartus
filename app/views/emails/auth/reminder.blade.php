<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Gerar nova senha</h2>
		<table>
			<tr>
				<td>Para gerar sua nova senha complete este fomulário clicando no link a seguir: <a href="{{url('password/reset/'.$resetCode)}}">{{url('password/reset/'.$resetCode)}}</a></td>
			</tr>
			<tr>
				<td>Este link se expira em {{ Config::get('auth.reminder.expire', 60) }} minutos.</td>
			</tr>
			<tr>
				<td>Se você não solicitou uma nova senha, desconsidere este e-mail.</td>
			</tr>
		</table>
	</body>
</html>
