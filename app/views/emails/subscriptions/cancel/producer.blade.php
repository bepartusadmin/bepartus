<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>Olá, {{ $subscription->project->user->name }}</td>
			</tr>
			<tr>
				<td>Um patrocinio para o seu projeto {{ $subscription->project->name }}  foi cancelado</td>
			</tr>
			<tr>
                                <?php $now = date('Y-m-d H:i:s') ?>
				<table cellpadding="0" cellspacing="0" border="1">
					<tr>
						<th>Usuário</th>
						<th>Valor</th>
						<td>Data Cancelamento</td>
					</tr>
					<tr>
						<td>{{ $subscription->user->name }}</td>
						<td>R${{ Helper::Monetize($subscription->amount/100) }}</td>
						<td>{{ Helper::ConverterBR($now) }} às {{ Helper::Hora($now) }}</td>
					</tr>
				</table>
			</tr>
		</table>
	</body>
</html>
