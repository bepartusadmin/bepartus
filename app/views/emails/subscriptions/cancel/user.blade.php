<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>Olá, {{ $subscription->user->name }}</td>
			</tr>
			<tr>
                             <?php $now = date('Y-m-d H:i:s') ?>
				<td>O seu patrocínio para o seu projeto {{ $subscription->project->name }} no valor de R${{ Helper::Monetize($subscription->amount/100) }} foi cancelado com sucesso em {{ Helper::ConverterBR($now) }} às {{ Helper::Hora($now) }} </td>
			</tr>
		</table>
	</body>
</html>
