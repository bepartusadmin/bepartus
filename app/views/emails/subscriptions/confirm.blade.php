<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>Olá, {{ $subscription->user->name }}</td>
			</tr>
			<tr>
				<td>Sua compra foi efetuada</td>
			</tr>
			<tr>
				<table cellpadding="0" cellspacing="0" border="1">
					<tr>
						<th>Produtor</th>
						<th>Projeto</th>
						<th>Recompensa</th>
						<th>Valor</th>
						<td>Status</td>
						<th>Data início da assinatura</th>
					</tr>
					<tr>
						<td>{{ $subscription->project->user->name }}</td>
						<td>{{ $subscription->project->name }}</td>
						<td>{{ $subscription->direct() ? $reward->title : 'Não escolheu recompensa' }}</td>
						<td>R${{ Helper::Monetize($subscription->amount/100) }}</td>
						<td>{{ $subscription->statusText }}</td>
						<td>{{ Helper::ConverterBR($subscription->created_at) }} às {{ Helper::Hora($subscription->created_at) }}</td>
					</tr>
				</table>
			</tr>
		</table>
	</body>
</html>
