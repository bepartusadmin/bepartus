<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<h2>Seu projeto {{ $project->name }} não foi aprovado.</h2>
	<body>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>A equipe Bepartus não aprovou;</td>
			</tr>
			<tr>
				<td><b>Justificatva:</b> {{ $project->approval_comment }}</td>
			</tr>
			
			<tr>
				<td>Qualquer dúvida entre em contato</td>
			</tr>
		</table>
	</body>
</html>
