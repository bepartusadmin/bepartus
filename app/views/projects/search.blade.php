@section('content')
 <section class="novosprodutores categorias">
    	<div class="container">
        <h2>Resultados para: {{ Input::get('s') }} <small>({{ $projects->getTotal() }})</small></h2>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                
                <div id="">
                  @foreach($projects->chunk(4) as $chunk)
                  <div id="tabs-1">
					@foreach($chunk as $project)
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 produtorinfo">
                        <a href="{{ route('project.show', $project->slug) }}"><img src="{{ $project->getImageSrc() }}" alt="{{ $project->name }}" class="img-responsive center-block"></a>
                        <div class="infos">
                            <p class="nomeprojeto">{{ $project->name }}</p>
                            <p class="resumoprojeto">{{ $project->excerpt }}</p>
                            <p class="npatrocindores">{{ $project->subscriptions->count() }} Patrocinadores</p>
                            <p class="valordoado"><span>R$ {{ Helper::Monetize($project->getAllCash()) }}</span> por mês</p>
                        </div>
                    </div>
                    @endforeach
                  </div>
                  
				 @endforeach
                  

                  </div>        
                {{ $projects->appends($_GET)->links() }}
               <!--  <ul class="paginacao">
                   <li><a href="#"><</a></li>
                   <li class="ativo"><a href="#">1</a></li>
                   <li><a href="#">2</a></li>
                   <li><a href="#">3</a></li>
                   <li><a style="postion:relative;right:2px;" href="#">...</a></li>
                   <li><a href="#">></a></li>
               </ul>
                            -->
            </div>	
            
        	</div>
                  
    </section>
@stop