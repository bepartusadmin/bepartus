@section('content')
     <section class="formscadastro">
    <div class="container">
        
            @include('partials.project.steps')

            <div class="box">
              @include('partials.site.notifications')
              {{ Form::open(['route'=>array('projects.update', $project->slug), 'files'=>true]) }}
                   <p> 
                   @if($project->image != '')
                            <img src="{{ $project->getImageSrc() }}" alt="">
                    @endif
                    </p>
                    <p class="inputfile"><label for="imagem">Imagem&nbsp;&nbsp;&nbsp;<img title="Imagem de capa do seu projeto, tamanho ideal: 1280x400px" src="{{ asset('img/duvidab.jpg') }}"></label>

                   
                   
                        <input type="file" name="image" id="imagem" />
                   </p>
                    <p class="clearinput"></p>
                    <p>
                        <label for="name">Nome</label>
                        {{ Form::text('name', $project->name, ["id"=>"nome", "placeholder"=>"Nome do projeto"]) }}
                    </p>
                    <p>
                        <label for="site">Site</label>
                        {{ Form::text('site', $project->site, ["id"=>"site", "placeholder"=>"Site do projeto"]) }}
                        
                    </p>
                    <p>
                        <label for="excerpt">
                            Resumo <img title="Pequena descrição do seu projeto, resumo para o usuário ver no card antes de entrar na página." src="{{ asset('img/duvidab.jpg') }}" />
                        </label>
                        {{ Form::textarea('excerpt', $project->excerpt, ["id"=>"excerpt", "placeholder"=>"Resumo do projeto"]) }}
                        
                    </p>
                    <p><label for="slug"><span class="url1">URL</span> <img title="Link direto para o seu projeto. Evite espaõs, acentos e caracteres especiais" src="{{ asset('img/duvidab.jpg') }}" /> <span class="Furl">http://bepartus.com/projeto/</span></label>
                        
                        {{ Form::text('slug', $project->slug, ["id"=>"slug"]) }}
                    </p>

                    <p><label for="category_id">Categoria(s)</label>
                        {{ Form::select('category_id[]', [''=>'Escolher categoria']+$categories,$project->categories()->count() > 0 ? $project->categories()->lists('category_id')[0] : null) }}
                        {{ Form::select('category_id[]', [''=>'Escolher categoria']+$categories,$project->categories()->count() > 1 ? $project->categories()->lists('category_id')[1] : null) }}
                        {{ Form::select('category_id[]', [''=>'Escolher categoria']+$categories,$project->categories()->count() > 2 ? $project->categories()->lists('category_id')[2] : null) }}
                    </p>
                    <!--p><label for="video">video ou imagem</label><input type="text" name="video" placeholder="ex youtube.com" id="video" placeholder="Site do projeto"></p-->
                    <p>
                        <label for="video_src">Vídeo de apresentação</label>
                        {{ Form::text('video_src', $project->video_link, ["id"=>"video_src", "placeholder"=>"Video de apresentação do projeto. Copie e cole link de vídeo do Youtube ou Vimeo. Não é obrigatório."]) }}
                    </p>
                    <p>
                        <label for="description">Descrição do Projeto</label>
                        {{ Form::textarea('description', $project->description, ["id"=>"description", "placeholder"=>"Descrição do projeto",'ckeditor']) }}
                    </p>
                    
            </div>
            
            <h3>Redes Sociais</h3>
            <div class="box">
                    @foreach($networks->chunk(3) as $chunk)
                        <div class="area1 col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            @foreach($chunk as $network)
                            <label for="youtube">{{ $network->name }}:</label><input value="  @if(!empty($n)) {{ $n[$network->id] }} @endif" type="text" name="networks[{{ $network->id }}]"><br>
                          
                            @endforeach
                        </div>
                    @endforeach
                
            </div>
            <input class="btEnviar" type="submit" value="Salvar e continuar"><br><br>
            </form>
    </div>
  </section>
    <style>#cke_description{width: 79.5%; float: right;}</style>
   <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
<script>
$(document).ready(function(){
     CKEDITOR.replace( 'description' );
});
    
</script>
@stop