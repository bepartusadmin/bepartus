@section('content')
<section class="formscadastro metas">
  	<div class="container">
            @include('partials.project.steps')
		
                {{ Form::open(['route'=>array('project.objective.store',$project->slug)]) }}
                  <div class="box">
                  @include('partials.site.notifications')
                    <p class="title">Metas são valores específicos que você quer chegar para poder oferecer melhorias no seu conteúdo</p>
                    <p><b>As metas são completamente opcionais</b>, mas podem incluir melhorias como vídeo extra por semana, efeitos especiais, câmera nova, novos convidados, entre outras. Elas servem para incentivar os seus fãs e mostrar aonde você quer chegar com aquele valor.</p>
                    <div class="boxinfoMetas">
                      @foreach($project->objectives as $index => $objetive)
                       <div class="new_add">
                       <!--<a class="btn btn-danger deleteAddedDinamic" href="javaScript:;"><i class="fa fa-trash"></i>Excluirs</a>-->
                       {{ Form::hidden('ids['.$index.']', $objetive->id) }}
                       <a class="btn deleteAdded btn-danger ajax-delete-link" data-confirm="Você deseja realmente excluir essa meta?" href="{{ route('project.objective.delete',[$project->slug, $objetive->id])}}"><i class="fa fa-trash"></i>Excluir</a>
                    	<label for="name">Meta</label>
                        {{ Form::text('value['.$index.']', Helper::Monetize($objetive->value/100), ['placeholder'=>'R$ Valor','class'=>'inptMeta valor moneyInp '.($errors->has('value.'.$index) ? 'objective_field_error' : '')]) }}
                        <span>Valor que você quer alcançar para oferecer o que você descreve nesta meta.</span><br>
                        <label for="nomemeta">Nome da Meta</label>
                      
                         {{ Form::text('name['.$index.']', $objetive->name, ['placeholder'=>'Nome da Meta/Pequeno Título até 37 caracteres', 'class' => ($errors->has('name.'.$index) ? 'objective_field_error' : '')]) }}<br>
                        <label for="resultado">Resultado <img title="Diga o que irá mudar no seu conteúdo caso você atinja este valor de meta." src="{{ asset('img/duvidab.jpg') }}" /></label><!-- <input required type="text" name="resultado" placeholder="Descrição da meta 1 até 600 caracteres" /> -->
                      
                           {{ Form::textarea('result['.$index.']', $objetive->result, ['placeholder'=>'Descrição da meta 1 até 600 caracteres','ckeditor','class' => ($errors->has('result.'.$index) ? 'objective_field_error' : '')]) }}
            
                        <br> 
                        </div>
                      @endforeach
                      
                      <div class="block_to_add">@include('partials.objective.new', array('objectives' => $project->objectives))</div>
                        <br>
                        
                        <input class="btAdd btAddMeta" type="button" value="Adicionar Meta" />
                       
                    </div>
                
                
                  </div>                  
                  <input class="btEnviar" type="submit" value="Salvar e continuar" /><br/><br />
            {{ Form::close() }}

       </div>
  </section>
@stop