@section('content')

    <section class="bannerinterno">
		<div class="container">
          @include('partials.site.notifications')
        	<img src="{{ $project->getImageSrc('uploads/banner/') }}" alt="{{ $project->name }}" class="img-responsive bordered_img">
        </div>
    </section>
  
  
  	<section class="produtor">
    	<div class="container">
        	
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 sidebar">
            
            	<div id="CompraTop" class="patrocinar">
                    <!-- <div class="sejapatrocinador">
                        <img src="{{ asset('img/sejapatrocinador.png') }}" alt="" class="col-xs-3 col-sm-3 col-md-3 col-lg-3 img-responsive">
                        <p class="col-xs-9 col-sm-9 col-md-9 col-lg-9">Seja um patrocinador</p>
                    </div> -->
                    
                    <div class="patrocinadores">
                        <p class="txtpreto">{{ $project->subscriptions->count() }}</p>
                        <p class="txtlaranja">Pessoas</p>
                         @if($project->getAllCash()>'0')
                        <span class="clear"></span>
                        <p class="txtpreto">R${{ Helper::Monetize($project->getAllCash()) }}</p>
                        <p class="txtlaranja">por mês <img title="Este é um valor estimado do que o produtor ganha por mês. Existe uma margem de erro por conta de finalizações de pagamento." src="{{ asset('img/duvidab.jpg') }}"></p>
                         @endif
                        <span class="clear"></span>
                    	<div class="btpatrocinar">
                        	<a href="{{ route('project.subscribe', $project->slug) }}">Patrocinar</a>
                        </div>
                    </div>
                </div>

                <div class="Boxrecompensas">   
                    <h4 class="titrecompensa">Recompensas <img title="As Recompensas são itens ou benefícios que os produtores oferecem aos seus patrocinadores como forma de agradecimento por ajudar." src="{{ asset('img/duvidac.jpg') }}"></h4>
                    @foreach($project->rewards as $reward)
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 recompensas">
                            <h2 class="col-xs-12 col-sm-12 col-md-12 col-lg-12">{{ $reward->title }}</h2>
                            <div class="valor">R${{ Helper::Monetize($reward->value/100) }} ou mais por mês</div>
                            <div class="countpatrocinadores"><img src="{{ asset('img/group.jpg') }}">{{ $reward->subscriptions->count() }} Patrocinadores</div>
                            <p>
                              {{ nl2br(strip_tags($reward->description)) }}
                            </p>
                            <a href="#CompraTop"><img class="hoverAfter" src="{{ asset('img/btNextRecomp.jpg') }}" /><img class="hoverBt" src="{{ asset('img/btNextRecomp2.png') }}" /> <span>QUERO ESTA RECOMPENSA</span></a>
                    </div>
                    @endforeach           
                </div>     
                            

<!--                  <div class="imgprodutor">
                     <img class="img-responsive" src="{{ isset($project->user->userdata->image) ? asset('uploads/normal/'.$project->user->userdata->image) : 'http://placehold.it/307X307' }}" alt="{{ $project->user->name or null }}">
                 </div>

                 <div class="redesociaisside">
                   @foreach($networks as $network)
                        @if(!empty($n))
                   		 @if($n[$network->id] != '')
                         <a target="_blank" href="{{ $n[$network->id] }}"><img src="{{ asset('img/'.$network->flag) }}" /></a>   
                         @endif  
                        @endif
                   @endforeach
                   
                 </div> -->

                 <div class="sobre">

                    <h4>Sobre</h4>
                    <p>{{ $project->excerpt }}</p>

                    <!-- <h4>Localização</h4> -->
                    <p><b>{{ $project->user->userdata->city->name or null }} - {{ $project->user->userdata->city->state->uf or null }}</b></p>

                 </div>
                
            </div>
            
            
            <div class="infoProdutorTop">

                 <div class="imgprodutor">
                     <img class="img-responsive" src="{{ isset($project->user->userdata->image) ? asset('uploads/normal/'.$project->user->userdata->image) : 'http://placehold.it/307X307' }}" alt="{{ $project->user->name or null }}">
                 </div>
                
                 <div class="producerData">
                     <p>
                         {{ $project->user->name }}
                         <span>{{ join(',',$categories) }}</span>
                     </p>
                 </div>

                 <div class="redesociaisside">
                   @foreach($networks as $network)
                        @if(!empty($n))
                         @if($n[$network->id] != '')
                         <a target="_blank" href="{{ $n[$network->id] }}"><img src="{{ asset('img/'.$network->flag) }}" /></a>   
                         @endif  
                        @endif
                   @endforeach
                   
                 </div>                

            </div>
            
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 oprojeto">
                    <h2>{{ $project->name }}</h2><br/><br/>
                
                   {{ $project->getVideo() }} <br><br>
                   {{ $project->description }}

                 <div class="compartilhar">
                    <p><a href="javaScript:;" id="bt_share"><img src="{{ asset('img/compartilhar.jpg') }}" alt="">Compartilhar</a></p>
                    <!-- AddToAny BEGIN -->
                    <div id="shareDiv" style="display: none;" class="a2a_kit a2a_kit_size_32 a2a_default_style">
                        <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                        <a class="a2a_button_facebook"></a>
                        <a class="a2a_button_twitter"></a>
                        <a class="a2a_button_google_plus"></a>
                        <a class="a2a_button_whatsapp"></a>
                        <a class="a2a_button_email"></a>
                    </div>
                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                    <!-- AddToAny END -->
                 </div>
                   
            </div>

            <!--<h4 class="titrecompensa">Recompensas<img title="As Recompensas são itens ou benefícios que os produtores oferecem aos seus patrocinadores como forma de agradecimento por ajudar." src="{{ asset('img/duvidac.jpg') }}"></h4>
            @foreach($project->rewards as $reward)
              <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 recompensas">
                    <h2 class="col-xs-12 col-sm-6 col-md-6 col-lg-6">{{ $reward->title }}</h2>
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 valor">R${{ Helper::Monetize($reward->value/100) }} ou mais por mês</div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 countpatrocinadores"><img src="{{ asset('img/group.jpg') }}">{{ $reward->subscriptions->count() }} Patrocinadores</div>
                    <p>
                      {{ $reward->description }}
                    </p>
            </div>
            @endforeach -->

            <div class="BoxMeta col-xs-12 col-sm-9 col-md-9 col-lg-9">

                 <!-- <h3>Metas <img title="As metas são os valores que o produtor quer alcançar para poder oferecer algo em troca ao seu público." src="{{ asset('img/duvidac.jpg')}}"></h3> -->
                <section class="metasSlide slider">
                @foreach($project->objectives as $objective)
                 <div class="metas">
                     <div class="row">
                         <div class="col-sm-6">
                            <p class="txtpreto">{{ $objective->name }}</p>
                            <p class="txtlaranja">R${{ Helper::Monetize($objective->value/100) }} por mês</p>
                         </div>
                         <div class="col-sm-6">
                            <?php 
                                $pct = ceil( ($project->getAllCash() * 100) / ($objective->value/100) );
                            ?>
                            R$ {{ Helper::Monetize($project->getAllCash()) }} de R${{ Helper::Monetize($objective->value/100) }}
                            <div class="progress" style="height: 10px;">
                                <div class="progress-bar {{ $pct >= 100 ? 'progress-bar-success' : 'progress-bar-warning'}}" role="progressbar" aria-valuenow="{{ $pct }}" aria-valuemin="0" aria-valuemax="100" style="width: {{$pct}}%;">
                                    <span class="sr-only">{{ $pct }}% Complete</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <p class="descricaometa">{{ nl2br(strip_tags($objective->result)) }}</p>
                        </div>
                     </div>
                </div>
                 @endforeach            
                 </section>

            </div>
            
        </div>
    </section>
    
@stop