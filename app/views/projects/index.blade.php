@section('content')
  <section class="novosprodutores categorias">
    	<div class="container">

            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                  
                  <ul class="list col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <li><b>Categorias</b></li>
                    <li><a href="{{ route('projects.index') }}"><span>Todas</span></a></li>
                    @foreach($categories as $cat)
                        <li class="{{ $cat->slug == Route::current()->getParameter('slug_cat') ? 'active' : null }}"><a href="{{ route('category.show', $cat->slug) }}"><span>{{ $cat->name }}</span></a></li>
                    @endforeach
                  </ul>


                  <ul class="list col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <li><b>Ordenar Por</b></li>
                    <?php 
                        $slugcat = Route::current()->getParameter('slug_cat',false);
                        $base_url = $slugcat ? route('category.show', $slugcat)  : route('projects.index');
                    ?>
                    <li class="{{ Input::get('order') == 1 ? 'active' : null }}"><a href="{{ $base_url }}?order=1"><span>Mais Patrocinadores</span></a></li>
                    <li class="{{ Input::get('order') == 2 ? 'active' : null }}"><a href="{{ $base_url }}?order=2"><span>Maior Arrecadação</span></a></li>
                    <li class="{{ Input::get('order') == 3 ? 'active' : null }}"><a href="{{ $base_url }}?order=3"><span>Recém Adicionados</span></a></li>
                    <li class="{{ Input::get('order') == 4 ? 'active' : null }}"><a href="{{ $base_url }}?order=4"><span>Mais Antigos</span></a></li>
                  </ul>

            </div>


            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                <h2>Projetos {{ isset($category->id) ? ' em '.$category->name : null }}</h2>
                @forelse($projects as $project)
                    <div class="produtorinfo col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <a href="{{ route('project.show', $project->slug) }}"><img src="{{ $project->getImageSrc() }}" alt="" class="img-responsive center-block bordered_img"></a>
                        <div class="infos">
                            <p class="nomeprojeto">{{ $project->name }}</p>
                            <p class="resumoprojeto">{{ $project->excerpt }}</p>
                            <p class="npatrocindores">{{ $project->subscriptions->count() }} Patrocinadores</p>
                            @if($project->getAllCash()>'0')
                            <p class="valordoado"><span>R$ {{ Helper::Monetize($project->getAllCash()) }}</span> por mês</p>
                            @endif
                        </div>
                    </div>
                @empty
                 <div style="width: 100%;" class="produtorinfo">
                    <p>Ainda não temos projetos ativos nesta seção</p>
                 </div>
                @endforelse
                
                <div class="clearfix"></div>
                {{ $projects->appends($_GET)->links() }}

                <!-- <ul class="paginacao">
                    <li><a href="#"><</a></li>
                    <li class="ativo"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a style="postion:relative;right:2px;" href="#">...</a></li>
                    <li><a href="#">></a></li>
                </ul> -->
            </div>
                                                           

        </div>
                  
    </section>
@stop