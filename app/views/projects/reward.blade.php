@section('content')
 <section class="formscadastro recompensas">
    <div class="container">
             @include('partials.project.steps')

               {{ Form::open(['route'=>array('project.reward.store', $project->slug)]) }}
                    <div class="box">
                    @include('partials.site.notifications')
                    <p class="title">Recompensas são os benefícios que você irá oferecer para seus fãs que se tornarem patrocinadores.</p>
                    <p><b>Seja criativo</b> e ofereça benefícios como por exemplo acesso a grupos exclusivos, agradecimentos em vídeo, sorteios, etc. Recompensas são a sua melhor forma de agradecimento.</p>
                    <div class="boxinfoMetas">
                    @foreach($project->rewards as $index => $reward)
                    <div class="new_add">

                       <a class="btn btn-danger deleteAddedDinamic ajax-delete-link" data-confirm="Você deseja realmente excluir essa recompensa?" href="{{ route('project.reward.delete',[$project->slug, $reward->id])}}"><i class="fa fa-trash"></i>Excluir</a>
                    {{ Form::hidden('ids['.$index.']', $reward->id) }}
                    
                        <label for="value">Valor</label>
                            {{ Form::text('value['.$index.']', Helper::Monetize($reward->value/100), ['placeholder'=>'R$ 2,00','class'=>'inptMeta valor moneyInp']) }}
                        <span>Valor mínimo que o fã precisa patrocinar para receber esta recompensa</span><br>
                        <label for="title">Título</label>
                        {{ Form::text('title['.$index.']', $reward->title, ['placeholder'=>'Título da Recompensa']) }}<br>
                        <label for="descricao">Descrição</label><!-- <input type="text" name="descricao" placeholder="Descrição da Recompensa" /> -->
                      
                         {{ Form::textarea('description['.$index.']', $reward->description, ['placeholder'=>'"Descrição da Recompensa','ckeditor']) }}

                    </div>

                    @endforeach
                    
                    <div class="block_to_add">@include('partials.reward.new', array('rewards' => $project->rewards))</div>

                    
                     <div class="boxinfoMetas">
                      <input class="btAdd btAddRecompensa" type="button" value="Adicionar Recompensa" />
                      </div>
                    </div>

                </div>
                 <input class="btEnviar" type="submit" value="Salvar e continuar" /><br/><br />
            {{ Form::close() }}
    </div>
  </section>
@stop