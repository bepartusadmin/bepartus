@section('pagetitle')
 - Contato
@stop
@section('content')
  <section class="contato">
  	<div class="container">
    	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-lg-6 center">
            <h2>Fale com a gente!</h2>
            <span class="emailContato">contato@bepartus.com</span>
            @include('partials.site.notifications')
            <form action="{{ route('post.contact') }}" method="post">
                <input class="imgname col-xs-12 col-sm-5 col-md-5 col-lg-5 col-lg-5" type="text" value="{{ Input::old('name') }}" name="name" placeholder="Nome" required />

                <input class="icoEmail col-xs-12 col-sm-5 col-md-5 col-lg-5 col-lg-5 fr" type="email" value="{{ Input::old('email') }}" name="email" placeholder="E-mail" required />

                <textarea name="msg" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12" placeholder="Mensagem" required>{{ Input::old('msg') }}</textarea>

                <input class="btLimpar col-xs-12 col-sm-5 col-md-5 col-lg-5 col-lg-5" type="reset" value="Limpar">
                <input class="btEnviar col-xs-12 col-sm-5 col-md-5 col-lg-5 col-lg-5 fr" type="submit" value="Enviar">
            </form>
        </div>
    </div>
  </section>
@stop