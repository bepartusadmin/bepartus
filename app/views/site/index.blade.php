@section('content')
<section id="myCarousel" class="carousel slide" data-ride="carousel">
  <!--<ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
  </ol>-->

  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <span class="banner1">
          <p class="chamadabanner">Financiamento coletivo recorrente para produtores de conteúdo <!-- <br/><span style="font-size:26px;">Fãs patrocinam seus produtores de conteúdo preferidos com qualquer valor acima de um real por mês</span class="textomenorbanner"> --></p>
         
          <a href="{{ route('auth.register') }}">Comece Hoje</a>
      </span>
    </div>
    <!--div class="item ">
      <a href="#" class="banner2">
          <p>Patrocínio recorrente para produtores de conteúdo</p>
          <span>cadastre-se</span>
      </a>
    </div-->        
  </div>

<!--   <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>-->
</section>



	<!--<section class="boxhome">
    <div class="container">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 itembox">
            <img src="img/cadastrese.jpg" alt="cadastre-se" class="img-responsive center-block">
            <p class="txttitulo">Cadastre-se</p>
            <p class="txt">Faça seu cadastro e preencha todos os campos como forem necessários.</p>
        </div>
        
         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 itembox">
            <img src="img/crie.jpg" alt="cadastre-se" class="img-responsive center-block">
            <p class="txttitulo">crie sua página</p>
            <p class="txt">Crie sua página de produtor! Com ela pronta você poderá começar a receber patrocínio dos seus fãs</p>
        </div>
        
         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 itembox">
            <img src="img/avise.jpg" alt="cadastre-se" class="img-responsive center-block">
            <p class="txttitulo">Avise seus fãs</p>
            <p class="txt">Divulgue a sua página de produtor e avise aos seus fãs que você está na Bepartus, mostre como eles podem ajudar!</p>
        </div>
        
         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 itembox">
            <img src="img/patrocinador.jpg" alt="cadastre-se" class="img-responsive center-block">
            <p class="txttitulo">seja patrocinado</p>
            <p class="txt">Pense em recompensas, metas e estimule os seus fãs, dessa forma conseguirá crescer cada vez mais!</p>
        </div>
    </div>   
</section>-->


<section class="Home novosprodutores">
	<div class="container">
    <h2>Conheça nossos mais novos produtores</h2>
    	@foreach($projects as $project)
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 produtorinfo">
            <a href="{{ route('project.show', $project->slug) }}"><img src="{{ $project->getImageSrc() }}" alt="{{ $project->name }}" class="img-responsive center-block bordered_img"></a>
            <div class="infos">
                <p class="nomeprojeto">{{ $project->name }}</p>
                <p class="resumoprojeto">{{ $project->excerpt }}</p>
                <p class="npatrocindores">{{ $project->subscriptions->count() }} Patrocinadores</p>
               @if($project->getAllCash()>'0')
                <p class="valordoado"><span>R$ {{ Helper::Monetize($project->getAllCash()) }}</span> por mês</p>
                @endif
              
            </div>
        </div>
        @endforeach
        <div class="vejamais"><a href="{{ route('projects.index') }}">Veja mais</a></div>
        
    </div>
</section>


<section class="Home novosprodutores">
	<div class="container">
    <h2>Veja quem está se destacando</h2>
    	@foreach($projects_highlights as $project)
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 produtorinfo">
            <a href="{{ route('project.show', $project->slug) }}"><img src="{{ $project->getImageSrc() }}" alt="{{ $project->name }}" class="img-responsive center-block bordered_img"></a>
            <div class="infos">
                <p class="nomeprojeto">{{ $project->name }}</p>
                <p class="resumoprojeto">{{ $project->excerpt }}</p>
                <p class="npatrocindores">{{ $project->subscriptions->count() }} Patrocinadores</p>
               @if($project->getAllCash()>'0')
                <p class="valordoado"><span>R$ {{ Helper::Monetize($project->getAllCash()) }}</span> por mês</p>
                @endif
              
            </div>
        </div>
        @endforeach
        
        
        <div class="vejamais"><a href="{{ route('projects.index') }}">Veja mais</a></div>
    </div>
    
    <div class="espaco"></div>
</section>
    
@stop