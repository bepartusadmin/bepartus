@section('content')
<section class="comofunciona">
	<div class="container">
		<h2>Como Funciona</h2>
		
        <div class="box">
        
        	<img src="{{ asset('img/cadastre.png') }}" class="img-responsive center-block">
            <h3>1 cadastre-se</h3>
            <p>Após se cadastrar na Bepartus, você tem a opção de se tornar um produtor de conteúdo, basta acessar a sua conta e clicar em "Conta de Produtor".</p>
            <div class="linha center-block"></div>
            
            
            <img src="{{ asset('img/crie2.png') }}" class="img-responsive center-block">
            <h3>2 Crie sua página</h3>
            <p>Após escolher tornar-se um produtor e ser aprovado pela Bepartus, você poderá iniciar um projeto e criar sua página de produtor com suas informações, metas e recompensas.</p>
            <div class="linha center-block"></div>
            
            
            <img src="{{ asset('img/avise2.png') }}" class="img-responsive center-block">
            <h3>3 Avise seus fãs</h3>
            <p>Agora sua página já está no ar e você está pronto para ser patrocinado pelos seus fãs. Basta compartilhar o link que você escolheu na hora de cadastrar seu projeto e divulgá-lo!</p>
            <div class="linha center-block"></div>
            
            
            <img src="{{ asset('img/patrocine.png') }}" class="img-responsive center-block">
            <h3>4 Seja patrocinado</h3>
            <p>Com a divulgação e o trabalho duro, seus fãs vão reconhecer o seu trabalho e irão se tornar patrocinadores do seu conteúdo!</p>
            
        </div>
        
        
</div>
</section>
@stop