@section('pagetitle')
	- Perguntas frequentes
@stop
@section('content')
<section class="perguntas">
	<div class="container">
		<h2>Perguntas Frequentes</h2>
		
        <div class="box">
        <div class="accordion">
            <dl>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">Geral</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p><b>O que é a Bepartus?</b></p>
                <p>Somos uma comunidade de produtores de conteúdo e fãs, funcionamos como financiamento coletivo recorrente com foco na melhora do trabalho do produtor de conteúdo. Nosso objetivo é tornar possível o crescimento de produtores independentes com a ajuda de seus fãs (patrocinadores), sendo apoiados com pequenas quantias de forma mensal e oferecendo benefícios em troca, por mínimos que sejam, muito valiosos para os patrocinadores.</p>

                <p><b>A Bepartus é um site de crowdfunding?</b></p>
                <p>Não. Nós somos uma comunidade de produtores de conteúdo e fãs. Em nossa plataforma, os doadores patrocinam os seus produtores preferidos com um valor mensal escolhido, podendo ser cancelado a qualquer momento sem nenhuma taxa ou burocracia.</p>
              </dd>
              <dt>
                <a href="#accordion2" aria-expanded="false" aria-controls="accordion2" class="accordion-title accordionTitle js-accordionTrigger">Produtores de Conteúdo</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion2" aria-hidden="true">
                <p><b>Quero ser um produtor, o que preciso fazer?</b></p>
                <p>Primeiro você deve se cadastrar na Bepartus. O segundo passo é acessar a sua conta e clicar em "Conta de Produtor". Depois disso, basta informar os dados necessários, preencher os campos de metas, recompensas, explicar o seu projeto e enviar para nós. Temos um prazo de até 48h para aprovar seu projeto e colocá-lo no ar. Após a aprovação, você já poderá começar a ser patrocinado pelos seus fãs!</p>

                <p><b>Quem pode ser um produtor de conteúdo?</b></p>
                <p>Youtubers, produtores de vídeo em geral, músicos, podcasters, fotógrafos, artistas, animadores, ilustradores, designers, escritores, blogueiros, vlogueiros, dançarinos, atores, atrizes, pintores, humoristas, professores, artesãos, e todo tipo de pessoa que produz conteúdo e impacta pessoas fazendo aquilo que gosta. Faça parte da Bepartus!</p>    

                <p><b>Quais as vantagens de ser um produtor de conteúdo na Bepartus?</b></p>
                <p>Fazendo parte da comunidade da Bepartus você vai receber constantemente conteúdos exclusivos e de grande valor para quem está no meio criativo, dicas e artigos sobre como melhorar seus resultados e o seu trabalho, passará a receber uma quantia financeira de forma mensal das pessoas que realmente gostam do seu trabalho, construirá uma relação mais próxima com os seus fãs mais engajados, retribuirá seus patrocinadores com recompensas, crescerá cada vez mais.</p>

                <p><b>A Bepartus cobrará algum valor dos produtores de conteúdo?</b></p>
                <p>Não, a única taxa que cobraremos será 5% em cima de cada doação feita, o produtor já receberá o valor líquido em sua conta. (Após serem cobradas as taxas do sistema de pagamento)</p>

                <p><b>O que são metas? Como elas podem me ajudar?</b></p>
                <p>As metas são estipuladas pela quantia que o produtor consegue receber de doação. Cada meta deve prometer aos usuários certos benefícios, como por exemplo: “Se o canal chegar a R$500 em doação, farei três vídeo por semana ao invés de dois”. O próprio produtor estipula quantas metas, os valores de cada uma e quais benefícios ele irá oferecer. Elas estimulam os usuários a doarem cada vez mais para obter os benefícios prometidos.</p>

                <p><b>Sou produtor de conteúdo, posso ser doador de outros produtores?</b></p>
                <p>Sim. Na Bepartus os produtores de conteúdo também podem se ajudar. Nós somos uma comunidade e o nosso objetivo e ajudar todos os produtores a alcaçarem seus objetivos e crescerem cada vez mais.</p>

                <p><b>O que são recompensas? Como posso criá-las?</b></p>
                <p>As recompensas são benefícios que o produtor de conteúdo oferecerá aos seus doadores, há a possibilidade de se criar vários pacotes de recompensas de acordo com a quantia doada pelo usuário. Você poderá criá-las diretamente no seu painel de controle.</p>                    

              </dd>
              <dt>
                <a href="#accordion3" aria-expanded="false" aria-controls="accordion3" class="accordion-title accordionTitle js-accordionTrigger">Patrocinadores</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion3" aria-hidden="true">
              
              <p><b>Quais as formas de pagamentos disponíveis na Bepartus?</b></p>
              <p>Hoje a Bepartus aceita cartão de crédito e boleto bancário.</p>

              <p><b>Qual o valor mínimo para apoiar um produtor?</b></p>
              <p>O valor mínimo para patrocinar seu produtor com o boleto bancário é de R$5,00 (devido às taxas do sistema de pagamento), e de R$1,00 no cartão de crédito.</p>

              <p><b>Quais as vantagens de ser um patrocinador na Bepartus?</b></p>
              <p>Você estará apoiando o trabalho dos produtores que realmente gosta, com o mínimo que seja, será extremamente importante e fará parte da vida dos produtores de conteúdo. Receberá recompensas exclusivas dos produtores que apoiar, terá uma relação mais próxima com os mesmos além de receber material exclusivo da Bepartus com assuntos relacionados a produção de conteúdo e criatividade.</p>

              <p><b>Existe um tempo mínimo de doação para que eu possa cancelar o pagamento?</b></p>
              <p>Não. Se você definir não patrocinar mais um produtor de conteúdo, basta cancelar a doação e a partir da próxima fatura não será cobrado nenhum valor em seu cartão de crédito.</p>

              <p><b>Não posso ajudar financeiramente agora, existe outra forma de apoiar meu produtor preferido?</b></p>
              <p>Claro! Se no momento você não pode se tornar um patrocinador, ajude o seu produtor preferido compartilhando a página dele. Dessa forma você vai estar o ajudando a melhorar cada vez mais o trabalho e tornar-se conhecido. Seu produtor preferido ainda não está na Bepartus? Convide ele!</p>                                                                        

              </dd>
            </dl>
          </div> 

        </div>
        
        
</div>
</section>

@stop
@section('add_scripts')
<script>
  $(document).ready(function(){

(function() {
  var d = document,
    accordionToggles = d.querySelectorAll('.js-accordionTrigger'),
    setAria,
    setAccordionAria,
    switchAccordion,
    touchSupported = ('ontouchstart' in window),
    pointerSupported = ('pointerdown' in window);

  skipClickDelay = function(e) {
    e.preventDefault();
    e.target.click();
  }

  setAriaAttr = function(el, ariaType, newProperty) {
    el.setAttribute(ariaType, newProperty);
  };
  setAccordionAria = function(el1, el2, expanded) {
    switch (expanded) {
      case "true":
        setAriaAttr(el1, 'aria-expanded', 'true');
        setAriaAttr(el2, 'aria-hidden', 'false');
        break;
      case "false":
        setAriaAttr(el1, 'aria-expanded', 'false');
        setAriaAttr(el2, 'aria-hidden', 'true');
        break;
      default:
        break;
    }
  };
  //function
  switchAccordion = function(e) {
    console.log("triggered");
    e.preventDefault();
    var thisAnswer = e.target.parentNode.nextElementSibling;
    var thisQuestion = e.target;
    if (thisAnswer.classList.contains('is-collapsed')) {
      setAccordionAria(thisQuestion, thisAnswer, 'true');
    } else {
      setAccordionAria(thisQuestion, thisAnswer, 'false');
    }
    thisQuestion.classList.toggle('is-collapsed');
    thisQuestion.classList.toggle('is-expanded');
    thisAnswer.classList.toggle('is-collapsed');
    thisAnswer.classList.toggle('is-expanded');

    thisAnswer.classList.toggle('animateIn');
  };
  for (var i = 0, len = accordionToggles.length; i < len; i++) {
    if (touchSupported) {
      accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
    }
    if (pointerSupported) {
      accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
    }
    accordionToggles[i].addEventListener('click', switchAccordion, false);
  }
})();
  });
</script>
@stop