@section('content')

 <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-sitemap"></i></li>
                        <li class="base"><a href="{{ route('home') }}">Home</a></li>
                        <li class="base"><a class="active">Login</a></li>
                    </ul>
                </div>
            </div>
           @include('partials.site.notifications')
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="size-32">Cadastrar nova senha</h2>
                    <p class="size-18 gray-5 text-light">Digite sua nova senha nos campos abaixo.</p>
                </div>
            </div>
            <div style="margin-top:25px;"></div>
            <div class="row">
                <div class="col-md-4 col-md-push-4">
                     {{ Form::open(['route'=>array('password.reset', $token)]) }}
                        <div class="form-group">
                            <label for="password">SENHA</label>
                            <input type="password" class="form-control input-lg" id="password" name="password">
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">CONFIRMAR SENHA</label>
                            <input type="password" class="form-control input-lg" id="password_confirmation" name="password_confirmation">
                        </div>
                        <button type="submit" class="btn btn-success btn-green-2 btn-block btn-lg btn-top text-uppercase text-extra-bold size-16">salvar nova senha</button>
                    {{ Form::close() }}
                </div>
            </div>
            <div style="margin-top:40px;"></div>
        </div>
    </section>

@stop