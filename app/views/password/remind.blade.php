@section('content')
 
<section class="esqueci-senha">
<div class="container">
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-8 col-lg-offset-2">
        <h2>Recuperar senha</h2>
        <p>Por favor digite abaixo o email que você cadastrou na Bepartus para que possamos enviar
um email e você possa redefinir sua senha.</p>    
        @include('partials.site.notifications')   
        {{ Form::open(['route'=>'password.request', "data-toggle"=>"validator", "role"=>"form"]) }}
            <input  value="{{ Input::old('email') }}" name="email" required class="icoEmail" type="email" value="" placeholder="Email">
                   
            <input type="submit" value="Enviar">
        {{ Form::close() }}
    </div>
</div>
</section>

@stop