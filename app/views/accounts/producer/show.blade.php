@section('content')
 <section class="admPatrocinador">
    {{-- Aqui o usuário é produtor e leva ao perfil de patrocinador?! --}}
    @include('partials.account.presentation', ['route'=>'user.account.sponsor'])
    <div class="bdr"></div>
    <div class="container"> 

       @include('partials.account.sponsor')

        <h3>Meus Projetos</h3>  
        <div class="boxPatrocinador2 admProdutor">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12">
                <ul>
                    @foreach($user->projects as $project)
                        <li>
                            <img src="{{ $project->getImageSrc() }}" height="105" alt="" />
                            <span class="info">
                                <p class="nomeProjeto">{{ $project->name }}</p>
                                <p class="Npatrocinadores fl"><b>{{ $project->subscriptions->count() }}</b> patrocinadores</p>
                                <p class="ValorM"><b>R${{ Helper::Monetize($project->getAllCash()) }}</b> por mês</p>
                            </span>
                            <a class="btEditar" href="{{ route('projects.edit', $project->slug) }}">editar</a>
                            <a class="btDetalhes" href="{{ route('projects.account.show', $project->slug) }}">detalhes</a>
                            <a class="btExcluir" name="modal" href="#modalexcluir">excluir</a>
                        </li>              
                    @endforeach      
                </ul>
                <a href="{{ route('projects.create') }}"><img src="{{ asset('img/btAdd.png') }}"  alt="" /> Adicionar novo projeto</a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12"></div>
        </div>            
     
        @include('partials.account.personal')
        @include('partials.account.payment')
        
        </div>
    </div>
  </section>

@include('partials.account.modals')
  
@stop