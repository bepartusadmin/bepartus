@section('content')
<?php $message_template = '<span class="help-block">:message</span>' ?>
<section class="admPatrocinador ContaProdutor">

    <div class="container">                
             @include('partials.site.notifications')

           {{ Form::open(['route'=>'producer.store', 'files'=>true]) }}
            @if(isset($user->producer->id))
              @if($user->producer->status == '1')
              <div class="row"><div class="col-md-12 alert alert-warning"><i class="fa fa-alert"></i> Você possui uma verificação pendente, aguarde.</div></div>
              @endif
            @endif
           {{ Form::hidden('data[price_range]','Até R$ 100,00') }}
           {{ Form::hidden('data[person_type]','Pessoa Física') }}
                <div class="dadosConta col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12"> 
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-lg-8 center">
                        <h3>Conta de Produtor</h3>
                        <p>As informações abaixo são exigidas pelo nosso parceiro de pagamento para efetuar o cadastro como produtor e são enviadas diretamente para eles, não sendo armazenadas pela Bepartus.</p>
                        
                        <span style="padding:0;" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-lg-6">
                            <label style="position:relative;top:12px;">Produtos<br /> Físicos?</label>
                            {{ Form::select('data[physical_products]', ['false'=>'Não','true'=>'Sim'], null, ['required']) }}
                           
                  
                            <label style="position:relative;top:12px;margin-top:-15px;">Transferência<br /> Automática</label>
                             {{ Form::select('data[automatic_transfer]', ['true'=>'Sim','false'=>'Não'], null, ['required']) }}                                                             
                        </span>
                        <div class="infoBox col-xs-12 col-sm-6 col-md-6 col-lg-6 col-lg-6 form-group {{ $errors->has('data.business_type') ? 'has-error' : '' }}">
                            <span>Descrição do Projeto</span><img src="{{ asset('img/duvidab.jpg') }}" title="Este projeto será um canal, blog, podcast? Fale resumidamente sobre ele." />
                            {{ Form::textarea('data[business_type]', isset($user->producer->business_type)? $user->producer->business_type : null, []) }}
                            {{ $errors->first('data.business_type', $message_template); }} 
                        </div>
                        <span class="clear block"></span>
                        
                        <div class="plain_div {{ $errors->has('data.business_type') ? 'has-error' : '' }}">
                            <label>Nome</label>
                            <input  type="text" name="data[name]" value="{{ $user->name }}" placeholder="Nome" />
                            {{ $errors->first('data.name', $message_template); }}
                        </div>
                        
                        <div class="plain_div {{ $errors->has('data.email') ? 'has-error' : '' }}">
                            <label>E-mail</label>
                            <input  type="email" name="data[email]" value="{{ $user->email }}" placeholder="E-mail" />
                            {{ $errors->first('data.email', $message_template); }}
                        </div>
                        
                        <div class="plain_div {{ $errors->has('data.cpf') ? 'has-error' : '' }}">
                            <label>CPF</label>
                            <input  name="data[cpf]"  value="{{ $user->userdata->cpf or null }}" type="text" id="cpf" placeholder="000.000.000-00" />
                            {{ $errors->first('data.cpf', $message_template); }}
                        </div>
                        
                        <div class="plain_div {{ $errors->has('data.address') ? 'has-error' : '' }}">
                            <label>Endereço</label>
                            <input  type="text" name="data[address]"  value="{{ $user->userdata->address or null }}" placeholder="Rua xxx, 292 - Apartamento 201" />
                            {{ $errors->first('data.address', $message_template); }}
                        </div>
                        
                        <div class="plain_div {{ $errors->has('state') ? 'has-error' : '' }}">
                            <label>Estado</label>
                            {{ Form::select('state', [''=>'selecione estado']+$states, isset($user->userdata->city->state->name) ? $user->userdata->city->state->name : null, ['style'=>'width:80%;']) }}
                            {{ Form::hidden('data[state]', isset($user->userdata->city->state->name) ? $user->userdata->city->state->name : null, ['id'=>'data_state_id']) }}
                            {{ $errors->first('state', $message_template); }}
                        </div>

                        <div class="plain_div {{ $errors->has('city') ? 'has-error' : '' }}">
                            <label>Cidade</label>
                            {{ Form::hidden('data[city]', isset($user->userdata->city->name) ? $user->userdata->city->name : null, ['id'=>'data_city_id']) }}
                            {{ Form::select('city', [''=>'selecione cidade']+$cities, isset($user->userdata->city->id) ? $user->userdata->city->id : null, ['style'=>'width:80%;']) }}
                            {{ $errors->first('city', $message_template); }}
                        </div>
                        
                        <div class="plain_div {{ $errors->has('data.cep') ? 'has-error' : '' }}">
                            <label>Cep</label>
                            <input  type="text" name="data[cep]" id="cep" value="{{ $user->producer->cep or null }}" placeholder="00000-000" />
                            {{ $errors->first('data.cep', $message_template); }}
                        </div>
                         
                        <div class="plain_div {{ $errors->has('data.telephone') ? 'has-error' : '' }}">
                            <label>Telefone</label>
                            <input  type="text" name="data[telephone]" value="{{ $user->producer->telephone or null }}" id="telefone" placeholder="xx 99999 9999" />                        
                            {{ $errors->first('data.telephone', $message_template); }}
                        </div>
                    </div>
                </div>  

                <div class="dadosConta col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12"> 
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-lg-8 center">
                        
                        <span style="padding:0;" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-lg-6">
                            <label style="position:relative;top:12px;">Banco</label>
                             {{ Form::select('data[bank]', ['Itaú'=>'Itaú','Banco do Brasil'=>'Banco do Brasil','Santander'=>'Santander','Caixa Econômica'=>'Caixa Econômica','Bradesco'=>'Bradesco'], isset($user->producer->bank) ? $user->producer->bank :null, []) }}
                            
                            <div class="plain_div {{ $errors->has('data.bank_ag') ? 'has-error' : '' }}">
                                <label>Agencia</label>
                                <input id="edit1" name="data[bank_ag]" value="{{ isset($user->producer->bank_ag) ? $user->producer->bank_ag :null }}" class="agencia" style="width:60%;"type="text" placeholder="9999" />
                                {{ $errors->first('data.bank_ag', $message_template); }}
                            </div>
                        </span>
                        <span style="padding:0;margin-bottom:30px;" class="infoBox col-xs-12 col-sm-6 col-md-6 col-lg-6 col-lg-6">
                            <label>Tipo de conta</label>
                            {{ Form::select('data[account_type]', ['Corrente'=>'Corrente','Poupança'=>'Poupança'], isset($user->producer->account_type) ? $user->producer->account_type :null, []) }}
                            
                            <div class="plain_div {{ $errors->has('data.bank_cc') ? 'has-error' : '' }}">
                                <label>Conta</label>
                                <input class="conta" name="data[bank_cc]" value="{{ isset($user->producer->bank_cc) ? $user->producer->bank_cc :null }}" style="width:60%;"type="text" placeholder="99999-9" />
                                {{ $errors->first('data.bank_cc', $message_template); }}
                            </div>
                        </span>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td colspan="3"><b>Formatos para envio de dados bancários:</b></td>
                            </tr>
                            <tr>
                              <td><b>Banco</b></td>
                              <td><b>Agência</b></td>
                              <td><b>Conta</b></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td>Itaú</td>
                              <td>999</td>
                              <td>99999-D</td>
                            </tr>
                            <tr>
                              <td>Banco do Brasil</td>
                              <td>9999-D</td>
                              <td>99999999-D</td>
                            </tr>
                            <tr>
                              <td>Santander</td>
                              <td>9999</td>
                              <td>99999999-D</td>
                            </tr>
                            <tr>
                              <td>Caixa Econômica</td>
                              <td>9999</td>
                              <td>XXX99999999-D (X: Operação)</td>
                            </tr>
                            <tr>
                              <td>Bradesco</td>
                              <td>9999-D</td>
                              <td>9999999-D</td>
                            </tr>
                          </tbody>
                        </table>                        
                    </div>
                </div> 

                <div class="dadosConta files col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12"> 
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-lg-8 center">
                        <label>RG/CNH</label>
                         {{ $doc_rg != null ? '<img height="150" src="'.asset('uploads/archives/users/'.$doc_rg).'">' : null }}
                        <div class="fakeInpt"><input type="file" name="files[id]" id="imagem" /></div>
                        <span  style="display:block;" class="clear"></span>
                        <label>CPF</label>
                         {{ $doc_cpf != null ? '<img height="150" src="'.asset('uploads/archives/users/'.$doc_cpf).'">' : null }}
                        <div class="fakeInpt"><input type="file" name="files[cpf]" id="imagem" /></div>
                        <span  style="display:block;" class="clear"></span>
                        <label>Atividade</label>
                         {{ $doc_activity != null ? '<img height="150" src="'.asset('uploads/archives/users/'.$doc_activity).'">' : null }}
                        <div class="fakeInpt"><input type="file" name="files[activity]" id="imagem" /></div>
                        <img style="margin:10px 0 0 20px;" src="{{ asset('img/duvidab.jpg') }}" title="Recomendamos o uso do Media Kit, mas podem ser utilizados quaisquer documentos que provem a produção de conteúdo por parte do usuário." />
                    </div>
                </div>                
                <input class="check fl" type="checkbox" /><span class="smalltext">Declaro que li e estou de acordo com o <a href="#">Contrato de Comerciante</a> do Iugu. </span>
                <input class="btEnviar fl" type="submit" value="Enviar Para Verificação" />
            {{ Form::close() }}
        

        </div>
    </div>
  </section>
  <script>
    $(document).ready(function(){
      $('select[name="state"]').change(function(){
        var nome = $(this).find("option:selected").text();
        $('#data_state_id').val(nome);
      });
      $('select[name="city"]').change(function(){
        var nome = $(this).find("option:selected").text();
        $('#data_city_id').val(nome);
      });
    });
  </script>
@stop