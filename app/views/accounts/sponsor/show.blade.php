@section('content')
	  
<section class="admPatrocinador">
{{-- Aqui o usuário épatrocinador e leva ao perfil de produtor --}}
@include('partials.account.presentation', ['route'=>'user.account.producer'])

<div class="bdr"></div>
    <div class="container">        
       
       @include('partials.account.sponsor')  
     
       @include('partials.account.personal')
       
    </div>
</div>
</section>



@stop