@section('add_scripts')
  <script src="{{ asset('js/jquery-pagination.js') }}"></script>
@stop
@section('content')
 <section class="admPatrocinador DetalheProjeto">
  @include('partials.site.notifications')
    <div class="bgContainer">
      	<div class="container">
            <div class="BoxPatrocinador">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-lg-4">
                    <div class="fotoPratoc">
                        <img src="{{ $project->getImageSrc() }}" alt="{{ $project->name }}" />
                    </div>
                </div>
            	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-lg-8">
                    <div class="DadosPatroc">
                        <h2>{{ $project->name }}</h2>
                        <p><b>{{ $project->subscriptions->count() }}</b>patrocinadores</p>
                        <p><b>R${{ Helper::Monetize($project->getAllCash()) }}</b>por mês</p>
                        <a href="{{ route('projects.edit',$project->slug) }}">ALTERAR INFORMAÇÔES</a>
                    </div>   
                </div>
            </div>
        </div>
    </div>
    <div class="bdr"></div>
    <div class="container">             
        <div class="Dados col-xs-12 col-sm-6 col-md-6 col-lg-6 col-lg-6">
            <h3><img src="{{ asset('img/icoInfo.png') }}" alt="" />Informações Básicas</h3>
            <div class="box col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td><b>Nome:</b></td>
                      <td>{{ $project->name }}</td>
                    </tr>
                    <tr>
                      <td><b>Categorias:</b></td>
                      <td>
                        @foreach($project->categories as $k => $cat)
                          {{ $cat->name }}{{ $project->categories->count() > $k+1  ? ',' : null }}
                        @endforeach
                      </td>
                    </tr>
                    <tr>
                      <td><b>Nº de Patrocinadores:</b></td>
                      <td>{{ $project->subscriptions->count() }} Patrocinador(es)</td>
                    </tr>
                    <tr>
                      <td><b>Valor Mensal:</b></td>
                      <td>R${{ Helper::Monetize($project->getAllCash()) }}<br></td>
                    </tr>
                    <tr>
                      <td><b>Local:</b></td>
                      <td>{{ $project->user->userdata->city->name or null }} - {{ $project->user->userdata->city->state->uf or null }} </td>
                    </tr>
                  </tbody>
                </table>
            </div>  
            <h3><img src="{{asset('img/icoStar.png') }}" alt="" />Top 5 Patrocinadores</h3>                        
            <div class="box col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td><b>Nome</b></td>
                      <td><b>Email</b></td>
                      <td><b>Valor</b></td>
                      <td><b>Data</b></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>

                   @foreach($project->subscriptions as $s)
                      <tr>
                        <td>{{ $s->user->name }}</td>
                        <td>{{ $s->user->email }}</td>
                        <td>R${{ Helper::Monetize($s->amount/100) }}</td>
                        <td>{{ Helper::ConverterBR($s->created_at) }}</td>
                      </tr>
                    @endforeach
            
                  </tbody>
                </table>
            </div>  
        </div>
        <div class="Dados col-xs-12 col-sm-6 col-md-6 col-lg-6 col-lg-6">
            <h3><img src="{{asset('img/icoPatro.png') }}" alt="" />Patrocinadores</h3>
            <div class="box col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12">
                <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td><b>Nome</b></td>
                      <td><b>Email</b></td>
                      <td><b>Valor</b></td>
                      <td><b>Data</b></td>
                    </tr>
                    @foreach($project->subscriptions as $s)
                      <tr>
                        <td>{{ $s->user->name }}</td>
                        <td>{{ $s->user->email }}</td>
                        <td>R${{ Helper::Monetize($s->amount/100) }}</td>
                        <td>{{ Helper::ConverterBR($s->created_at) }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
            </div> 
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
           <!-- <ul class="paginacao fr">
                <li><a href="#">&lt;</a></li>
                <li class="ativo"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a style="postion:relative;right:2px;" href="#">...</a></li>
                <li><a href="#">&gt;</a></li>
            </ul>      
            <div class="paginacao fr"></div>     --> 
        </div> 

        </div>
    </div>
  </section>
  <script>
    $('.paginacao').pagination({
        items: 100,
        itemsOnPage: 10,
        onPageClick: function(page){
       
        }
    });
  </script>
@stop