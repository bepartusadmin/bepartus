@section('content')
<section class="cadastro">
	<div class="container">
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-lg-offset-4">
	        <h2>Cadastro</h2>
	        <h3>Ou cadastre-se com seus dados</h3>
	         @include('partials.site.notifications') 
	        {{ Form::open(['route'=>'auth.post.register','data-toggle'=>'validator', "class"=>"form", 'role'=>'form']) }}
	        	<div class="form__wrapper">
	        	{{ Form::text('name', null, ["placeholder"=>"", "class"=>"imgname form__input", "required"]) }}
	        	<i class="fa fa-user fa-2" aria-hidden="true"></i>	        	
	        	<label class="form__label" for="name">
					<span class="form__label-content">Nome</span>
				</label>	        	
	        	</div>
	        	<div class="form__wrapper">
	        	{{ Form::email('email', null, ["placeholder"=>"", "class"=>"imgemail form__input", "required"]) }}
	        	<i class="fa fa-envelope fa-2" aria-hidden="true"></i>
	        	<label class="form__label" for="email">
					<span class="form__label-content">Email</span>
				</label>
				</div>
				<div class="form__wrapper">
	         	{{ Form::password('password',["placeholder"=>"", "class"=>"imgsenha form__input", "required"]) }}
				<i class="fa fa-unlock-alt fa-2" aria-hidden="true"></i>
	         	<label class="form__label" for="password">
					<span class="form__label-content">Senha</span>
				</label>
				</div>
				<div class="form__wrapper">
	         	{{ Form::password('password_confirmation',["placeholder"=>"", "class"=>"imgsenha form__input", "required"]) }}
				<i class="fa fa-unlock-alt fa-2" aria-hidden="true"></i>
	         	<label class="form__label" for="password">
					<span class="form__label-content">Confirmar Senha</span>
				</label>
				</div>
	           	<div class="form__wrapper">
	           	{{ Form::text('cpf', null, ["placeholder"=>"", "class"=>"imgcpf form__input",  "id"=>"cpf", "required"]) }}
	           	<i class="fa fa-file-text fa-2" aria-hidden="true"></i>
	         	<label class="form__label" for="cpf">
					<span class="form__label-content">CPF</span>
				</label>	           	
	          	</div>
	            <input type="submit" value="Criar Conta">
	        {{ Form::close() }}
	        <p>Ao se cadastrar você declara estar de acordo com os <a target="_blank" href="{{ route('terms') }}">Termos de Uso e Políticas de Privacidade</a> da Bepartus e do <a href="https://iugu.com/juridico/contrato-do-consumidor" target="_blank">Contrato de Consumidor do Iugu</a>.</p>
	        <p>Já possui uma conta? <a href="{{ route('auth.login') }}">Faça Login</a></p>
	    </div>
	</div>
</section>
@stop