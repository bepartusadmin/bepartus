@section('content')
<section class="login">
  <div class="container">
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-lg-offset-4">
          <h2>Log in</h2>
           @include('partials.site.notifications') 
          <form class="form" data-toggle="validator" role="form" action="{{ route('auth.post.login') }}" method="post">
              <div class="form__wrapper">
                 <input required class="icoEmail form__input" type="email" value="{{ Input::old('email') }}" name="email" placeholder="">
                 <i class="fa fa-envelope fa-2" aria-hidden="true"></i>
                <label class="form__label" for="email">
                  <span class="form__label-content">Email</span>
                </label>                             
              </div>
              <div class="form__wrapper">
                <input required class="icoPass form__input" type="password" name="password" placeholder="">
                <i class="fa fa-unlock-alt fa-2" aria-hidden="true"></i>
                    <label class="form__label" for="password">
                  <span class="form__label-content">Senha</span>
                </label>                
              </div>
              <input type="submit" value="Log in">
          </form>
          <p><a href="{{ route('password.remind') }}">Esqueci minha senha</a></p>
          <p>Ainda não é cadastrado? <a href="{{ route('auth.register') }}">Cadastre-se</a></p>
      </div>
  </div>
</section>
@stop