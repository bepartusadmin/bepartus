@section('content')
 <section class="checkout metas">
  	<div class="container">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-8 col-lg-offset-2">
            @include('partials.site.notifications')
            <h2>Torne-se um Patrocinador</h2>
                <!--<form data-toggle="validator" role="form" action="#" method="#">
                <label for="meta">Valor do patrocínio</label><input required class="inptMeta valor imgpatrocinio" type="text" name="meta" placeholder="R$ Valor" /><span class="info1">por mês</span>
                </form>
                
                <form adata-toggle="validator" role="form" action="#" method="#">-->
                {{ Form::open(['route'=>array('project.payment.subscribe', $project->slug)]) }}
                 <label for="meta">Valor do patrocínio</label>
                 <input class="inptMeta valor imgpatrocinio moneyInp" type="text" name="amount" placeholder="R$ Valor" /><span class="info1">por mês</span>
                  <div class="closer">
                	@foreach($project->rewards as $r)
                    <div class="box2 rewardBox">                   
                        <input type="radio" name="reward_id" value="{{ $r->id }}"><strong>{{ $r->title }}</strong><br>
                        <span class="valores">R${{ Helper::Monetize($r->value/100) }} ou mais por mês</span>
                       {{ nl2br($r->description) }}
 
                    </div> <!--fim da div box--> 
                    @endforeach 
                    <div class="tap"></div>
                  </div>  
                   
                  <input class="btEnviar" type="submit" value="Continuar" /><br/><br />
                {{ Form::close() }}
				</div>
       </div>
  </section>

 
@stop