@section('content')
    <link rel="stylesheet" href="{{ asset('css/creditcard.css') }}">
    <section class="checkout metas">
        <div class="container">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-8 col-lg-offset-2">
                <h2>Forma de pagamento</h2>
                @include('partials.site.notifications')
                
                {{ Form::open(['route'=>array('project.post.subscribe', $project->slug), 'id'=>'payment-form']) }}
                    {{--
                    @if(isset($user->customer->id))
                    <div class="box2">                   
                        <input type="radio" id="hasCustomer" name="has_registered" checked="checked" value="1"><strong>Pagamento por Cartão de Crédito Cadastrado</strong><br>
                        <p>Plano para pagamento utilizando cartão de crédito. A fatura para verificação será enviada no email cadastrado na Bepartus. A cobrança é automática e pode ser cancelada a qualquer momento.</p>

                        <p>Nesta opção você pagará utilizando o cartão de crédito que cadastrou em sua página de administrador.</p>

                    </div> <!--fim da div box--> 
                    @endif
                    --}} 

                    <div class="box2">                   
                        <input type="radio" name="has_registered" value="0"><strong>Pagamento por Cartão de Crédito</strong><br>
                        <p>Plano para pagamento utilizando cartão de crédito. A fatura para verificação será enviada no email cadastrado na Bepartus. A cobrança é automática e pode ser cancelada a qualquer momento.</p>
                        <div class="dadosCartao">
                            {{ Form::hidden('iugu_api_token', null, ['id'=>'iugu_api_token']) }}
                            @include('partials.payment.credit-card')
                        </div>        
                    </div><!--fim da div box-->

                    <div class="box2">                   
                        <input type="radio" id="hasBoleto" name="has_registered" value="2"><strong>Pagamento por Boleto Bancário</strong><br>
                        <p>Plano para pagamento por meio de boleto bancário. Esse plano só passa a ser válido e cobrado para o total em apoios a partir de R$5,00 (essa restrição existe por causa de taxas cobradas pelo nosso parceiro de pagamento). O boleto será enviado no email cadastrado na Bepartus.</p>
                    </div>

                    <input class="btEnviar" type="submit" value="Finalizar" /><br/><br />
                </form>
            </div>
        </div>
    </section>
  
    <script src="{{ asset('js/creditcard.min.js') }}"></script>
    <script type="text/javascript" src="https://js.iugu.com/v2"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        Iugu.setAccountID("846171775E634BD9A19D0E0CC5AA2F45");
        <?php if (App::environment('local', 'staging')): ?>
        Iugu.setTestMode(true);
        <?php endif; ?>

        $('#payment-form').submit(function(evt) {
            var form = $(this);
            var tokenResponseHandler = function(data) {
                if (data.errors) {
                    //console.log(data.errors);
                    alert("Erro salvando cartão: " + JSON.stringify(data.errors));
                } else {
                    console.log(data);
                    $("#iugu_api_token").val( data.id );
                    form.get(0).submit();
                }
                //return false;
            };

            if($('input#hasCustomer').is(":checked") || $('input#hasBoleto').is(":checked")){
                form.get(0).submit();
            } else {
                Iugu.createPaymentToken(this, tokenResponseHandler);
            }
            return false;
        });

        $('.ccjs-month').change(function(){
            var ano =  $('.ccjs-year').val();
            var mes = $(this).val();
            $('input[name="expiration"]').val(mes+"/"+ano);
        });

        $('.ccjs-year').change(function(){
            var mes =  $('.ccjs-month').val();
            var ano = $(this).val();
            $('input[name="expiration"]').val(mes+"/"+ano);
        });
    });
  </script>
  @stop