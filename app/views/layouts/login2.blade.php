<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Bepartus - Administração</title>

        {{ HTML::style('admin/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('admin/css/waves.min.css') }}
        {{ HTML::style('admin/css/nanoscroller.css') }}
        {{ HTML::style('admin/css/style.css') }}
        {{ HTML::style('admin/font-awesome/css/font-awesome.min.css') }}
        
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="account">
        @yield('content')
    </body>
</html>