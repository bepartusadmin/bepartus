<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Bepartus - Administração</title>

        @section('header')
        {{ HTML::style('admin/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('admin/css/waves.min.css') }}
        {{ HTML::style('admin/css/nanoscroller.css') }}
        {{ HTML::style('admin/css/morris-0.4.3.min.css') }}
        {{ HTML::style('admin/css/menu-light.css') }}
        {{ HTML::style('admin/css/style.css') }}
        {{ HTML::style('admin/font-awesome/css/font-awesome.min.css') }}
        @show
        
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        @include('admin2.partials.topnav')
	<section class="page">
            @include('admin2.partials.sidenav')
            <div id="wrapper">
                <div class="content-wrapper container">
                    @yield('content')
                </div>
            </div>
        </section>	
        
    @section('footer')
    {{ HTML::script('admin/js/jquery.min.js') }}
    {{ HTML::script('admin/bootstrap/js/bootstrap.min.js') }}
    {{ HTML::script('admin/js/metisMenu.min.js') }}
    {{ HTML::script('admin/js/jquery.nanoscroller.min.js') }}
    {{ HTML::script('admin/js/pace.min.js') }}
    {{ HTML::script('admin/js/waves.min.js') }}
    @show
</body>
</html>