## Bepartus

Projeto de patrocínio recorrente para produtores de conteúdo
# 1 CADASTRE-SE
Após se cadastrar na Bepartus, você tem a opção de se tornar um produtor de conteúdo, basta acessar a sua conta e clicar em "Conta de Produtor".

# 2 CRIE SUA PÁGINA
Após escolher tornar-se um produtor e ser aprovado pela Bepartus, você poderá iniciar um projeto e criar sua página de produtor com suas informações, metas e recompensas.

# 3 AVISE SEUS FÃS
Agora sua página já está no ar e você está pronto para ser patrocinado pelos seus fãs. Basta compartilhar o link que você escolheu na hora de cadastrar seu projeto e divulgá-lo!

# 4 SEJA PATROCINADO
Com a divulgação e o trabalho duro, seus fãs vão reconhecer o seu trabalho e irão se tornar patrocinadores do seu conteúdo!