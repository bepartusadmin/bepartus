jQuery(document).ready(function ($) {
    $('#description').summernote();
    $('.moneyInp').maskMoney({ thousands:'.', decimal:','});
    
    $('.addObjective').click(function(){
       var index = $('.panel-meta').length;
       var template = $('<div class="panel panel-default panel-meta">' +
        '<div class="panel-body">' +
            '<input type="hidden" name="objectives['+index+'][id]" value="">' +
            '<div class="form-group"><label class="control-label">Valor</label><input type="text" name="objectives['+index+'][value]"  class="form-control moneyInp"></div>'+
            '<div class="form-group"><label class="control-label">Nome</label><input type="text" name="objectives['+index+'][name]" class="form-control"></div>'+
            '<div class="form-group"><label class="control-label">Resultado</label><textarea name="objectives['+index+'][result]" rows="3" cols="50" class="form-control"></textarea></div>'+
            '<div class="checkbox"><label class="control-label"><input type="checkbox" name="objectives['+index+'][delete]" value="1">Remover</label></div>' +
        '</div></div>');

        $('.newMetaPlaceHolder').append(template);
        template.find('input.moneyInp').maskMoney({thousands:'.',decimal:','});
    });
    
    $('.addReward').click(function(){
       var index = $('.panel-recompensa').length;
       var template = $('<div class="panel panel-default panel-recompensa">' +
        '<div class="panel-body">' +
            '<input type="hidden" name="rewards['+index+'][id]" value="">' +
            '<div class="form-group"><label class="control-label">Valor</label><input type="text" name="rewards['+index+'][value]"  class="form-control moneyInp"></div>'+
            '<div class="form-group"><label class="control-label">Titulo</label><input type="text" name="rewards['+index+'][title]" class="form-control"></div>'+
            '<div class="form-group"><label class="control-label">Descrição</label><textarea name="rewards['+index+'][description]" rows="3" cols="50" class="form-control"></textarea></div>'+
            '<div class="checkbox"><label class="control-label"><input type="checkbox" name="rewards['+index+'][delete]" value="1">Remover</label></div>' +
        '</div></div>');

        $('.newRecompensaPlaceHolder').append(template);
        template.find('input.moneyInp').maskMoney({thousands:'.',decimal:','});
    });
});