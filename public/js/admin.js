$(document).ready(function(){
	$('input[name="status"]').bootstrapSwitch({
		onText:"SIM",
		offText:"NÃO",
		size: "small",
		offColor: "danger",
		
	});
	$('input[name="status"]').on('switchChange.bootstrapSwitch', function(event, state) {
	 // console.log($(this).data('id')); // DOM element
	 $('#modalLinkComment_'+pid).next('.loader_admin').fadeIn();
	 var pid = $(this).data('id');
		$.ajax({
			url:'/admin/projeto/'+pid+'/aprovar',
			data:{state:state},
			type:'post'
		}).done(function(data){
			//console.log(data);
			if(data.status == '1'){
				$('#modalLinkComment_'+pid).fadeOut();
			}else{
				$('#modalLinkComment_'+pid).fadeIn();
			}
			alert(data.response);
			 $('#modalLinkComment_'+pid).next('.loader_admin').fadeOut();
		});
	});
});