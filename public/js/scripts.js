$(document).ready(function() {  
	 $('select[name="state_id"]').change(function(){
        var state = $(this).val();
        
        $.ajax({
            url:'/city/search',
            type:'post',
            data:{state:state}
        }).done(function(data){
            
            $('select[name="city_id"]').html(data.response);
        });
    });
      $('select[name="state"]').change(function(){
        var state = $(this).val();
        
        $.ajax({
            url:'/city/search/name',
            type:'post',
            data:{state:state}
        }).done(function(data){
            
            $('select[name="city"]').html(data.response);
        });
    });  

      $('#bt_share').click(function(){
          $('#shareDiv').fadeToggle(1000);
      });

    $('.moneyInp').maskMoney({
        thousands:'.',
        decimal:','
    });

    $('.moneyInp').focus(function(){
       $('.tap').fadeIn();
       $('input[name="reward_id"]').prop('checked',false);
    });
    
    $('.moneyInp').blur(function(){ 
        if($(this).val() == ''){
         $('.tap').fadeOut();
        $('input[name="reward_id"]').prop('checked',false);
        }
      
    });

    $('.block_to_add').on('click','a.deleteAdded', function(){
        $(this).parent('div').remove();
    });
    

    $('.btAddMeta').click(function(){
        var index = $('.boxinfoMetas .new_add').length;
        var m = $('<div class="new_add"><a class="btn btn-danger deleteAdded" href="javaScript:;"><i class="fa fa-trash"></i>Excluir</a><label for="name">Meta</label> <input placeholder="R$ Valor" class="inptMeta valor moneyInp" name="value[' + index + ']" type="text"> <span>Valor que você quer alcançar para oferecer o que você descreve nesta meta.</span><br> <label for="nomemeta">Nome da Meta</label> <input placeholder="Nome da Meta/Pequeno Título até 37 caracteres" name="name['+ index +']" type="text"><br> <label for="resultado">Resultado <img title="Diga o que irá mudar no seu conteúdo caso você atinja este valor de meta." src="http://bepartus.com/img/duvidab.jpg"></label><textarea placeholder="Descrição da meta 1 até 600 caracteres" name="result['+ index +']" cols="50" rows="10" ckeditor></textarea> <br></div>');

        $('.block_to_add').append(m);
         m.find('input.moneyInp').maskMoney({
            thousands:'.',
            decimal:','
        });
    });

    $('.btAddRecompensa').click(function(){
        var index = $('.boxinfoMetas .new_add').length;
        var m = $('<div class="new_add"> <a class="btn btn-danger deleteAdded" href="javaScript:;"><i class="fa fa-trash"></i>Excluir</a> <label for="value">Valor</label> <input placeholder="R$ 2,00" class="inptMeta valor moneyInp" name="value[' + index + ']" type="text"> <span>Valor mínimo que o fã precisa patrocinar para receber esta recompensa</span><br> <label for="title">Título</label> <input placeholder="Título da Recompensa" name="title[' + index + ']" type="text"><br> <label for="descricao">Descrição</label><!-- <input type="text" name="descricao" placeholder="Descrição da Recompensa" /> --> <textarea placeholder="&quot;Descrição da Recompensa" name="description[' + index + ']" cols="50" rows="10" ckeditor></textarea> </div>');

        $('.block_to_add').append(m);
         m.find('input.moneyInp').maskMoney({
            thousands:'.',
            decimal:','
        });
    });


    wow = new WOW();
    wow.init();

      /**
       * check if the input has any value (if we've typed into it)
       */
      $('.form__input').blur(function() {

        if ($(this).val()) {
          $(this).closest('.form__wrapper').addClass('form--filled');
        } else {
          $(this).closest('.form__wrapper').removeClass('form--filled');
        }
      });

      /**
       * Form validation
       */
      $('.form').validate({
        rules: {
          password: {
            minlength: 6
          }
        }
      });

      /**
       * Form2 validation
       */
      $('.form2').validate();

      /**
       * Simple Modal
       */
      $('.modal__toggle').on('click', function(e) {
        e.preventDefault();
        $('.modal').toggleClass('modal--open');
      });

  

    $("#cpf").mask("999.999.999-99");  
    $(".admPatrocinador .add").click(function(){
        $(".admPatrocinador .boxPatrocinador2.admProdutor ul li:last-child").clone().appendTo(".admPatrocinador .boxPatrocinador2.admProdutor ul");
    });  

    $('input[name="user_image"]').change(function(){
        $('.loader').fadeIn();
        $(this).parent().parent('form').submit();
    });

    $( "#tabs" ).tabs();
    $('a[name=modal]').click(function(e) {
        e.preventDefault();
        
        var id = $(this).attr('href');
    
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
    
        $('#mask').css({'width':maskWidth,'height':maskHeight});

        $('#mask').fadeIn(1000);    
        $('#mask').fadeTo("slow",0.8);  
    
        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();
              
        $(id).css('top',  winH/2-$(id).height()/2);
        $(id).css('left', winW/2-$(id).width()/2);
    
        $(id).fadeIn(2000); 
    
    });
    
    $('.window .close').click(function (e) {
        e.preventDefault();
        
        $('#mask').hide();
        $('.window').hide();
    });     
    
    $('#mask').click(function () {
        $(this).hide();
        $('.window').hide();
    }); 

/*    $( ".menudesktop .alvo" ).mouseover(function() {
      $( ".subM" ).show("fast");
      $( ".alvo" ).addClass("act");
    }); */

/*    $( ".menudesktop .alvo" ).mouseover(function() {
      $( ".subM" ).show("fast");
      $( ".alvo" ).addClass("act");
    }); 

    $( ".subM" ).mouseleave(function() {
      $( ".subM" ).hide("fast");
      $( ".alvo" ).removeClass("act");      
    }); 

    $( ".alvo img" ).click(function() {
      $( ".search input" ).addClass("go");
        setTimeout(function(){
          $( ".search input" ).removeClass("go");
        }, 15000);                     
    });*/   
     $("#cpf").mask("999.999.999-99");  
    $("#cep").mask("99999.999");  
   
    $("#telefone").mask("(99) 99999.9999");  
    $(".admPatrocinador .add").click(function(){
        $(".admPatrocinador .boxPatrocinador2.admProdutor ul li:last-child").clone().appendTo(".admPatrocinador .boxPatrocinador2.admProdutor ul");
    });  
    $( "#tabs" ).tabs();     
    $('a[name=modal]').click(function(e) {
        e.preventDefault();
        
        var id = $(this).attr('href');
    
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
    
        $('#mask').css({'width':maskWidth,'height':maskHeight});

        $('#mask').fadeIn(1000);    
        $('#mask').fadeTo("slow",0.8);  
    
        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();
              
        $(id).css('top',  winH/2-$(id).height()/2);
        $(id).css('left', winW/2-$(id).width()/2);
    
        $(id).fadeIn(2000); 
    
    });
    
    $('.window .close').click(function (e) {
        e.preventDefault();
        
        $('#mask').hide();
        $('.window').hide();
    });     
    
    $('#mask').click(function () {
        $(this).hide();
        $('.window').hide();
    }); 

      $('.metasSlide').slick({
        dots: false,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        adaptiveHeight: true
      });      

/*    $( ".menudesktop .alvo" ).mouseover(function() {
      $( ".subM" ).show("fast");
      $( ".alvo" ).addClass("act");
      
    }); 

    $( ".menudesktop .alvo" ).mouseover(function() {
      $( ".subM" ).show("fast");
      $( ".alvo" ).addClass("act");
    }); 

    $( ".subM" ).mouseleave(function() {
      $( ".subM" ).hide("fast");
      $( ".alvo" ).removeClass("act");      
    }); 

    $( ".alvo img" ).click(function() {
      $( ".search input" ).addClass("go");
        setTimeout(function(){
          $( ".search input" ).removeClass("go");
        }, 15000);                     
    }); */        
    
    $('[id^=edit]').keypress(validateNumber);

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;

    };    


    $('.ajax-delete-link').click(function(e){
        e.preventDefault();
        var button = $(this);
        
        if(confirm(button.data('confirm'))) 
        {
            $.ajax({
                url: button.attr('href'),
                type:'DELETE'
            }).done(function(data){
                if(data.success) {
                    window.location.reload();
                    //button.parent().fadeOut();
                }
            }) .fail(function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseJSON.error.message);
            });
        }
    });
      
    
    $('.newsletterform').submit(function(e){
        e.preventDefault();
        var form = $(this);
        var modal = $(form.data('modal'));
        var loading = form.find('.fa');
        loading.show();
        $.ajax({
            url: form.attr('action'),
            type:'POST',
            data: form.serialize()
        }).done(function(data){
            if(data.success) {
                modal.find('.text').html(data.message);
                modal.modal('show');
                form[0].reset();
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            modal.find('.text').html(jqXHR.responseJSON.error.message);
            modal.modal('show');
        }).always(function(){
            loading.hide();
        });
    });
});


  $(function() {
    $( document ).tooltip({
      position: {
        my: "center bottom-20",
        at: "center top",
        using: function( position, feedback ) {
          $( this ).css( position );
          $( "<div>" )
            .addClass( "arrow" )
            .addClass( feedback.vertical )
            .addClass( feedback.horizontal )
            .appendTo( this );
        }
      }
    });
  });
  

